package store_index_elasticsearch

import (
	"bitbucket.org/epicglue/api/connection/index"
	"bitbucket.org/epicglue/api/connection/index/elasticsearch"
	"bitbucket.org/epicglue/api/helpers"
)

var log = helpers.GetLogger("process_store")

type ElasticsearchStore struct {
	Connection index.Index
}

func NewElasticsearchIndexStore() *ElasticsearchStore {
	return &ElasticsearchStore{
		Connection: elasticsearch.NewElasticsearch(),
	}
}
