package worker_model

import "bitbucket.org/epicglue/api/model"

type Processable interface {
	GetItem() *model.Item
	ProcessTask(*Task) error
}
