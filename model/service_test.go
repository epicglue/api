package model_test

import "bitbucket.org/epicglue/api/model"

func GetTestService() *model.Service {
	return &model.Service{
		Id:        1,
		Category:  GetTestCategory(),
		ShortName: "test_service",
		Name:      "Test Service",
	}
}
