package model

import "github.com/yezooz/null"

type SubscriptionLog struct {
	Subscription *Subscription `json:"subscription"`
	URL          string        `json:"url"`
	Code         uint          `json:"code"`
	Msg          string        `json:"msg,omitempty"`
	CreatedAt    null.Time     `json:"-"`
}
