package model

import (
	"bitbucket.org/epicglue/api/connection/database/postgres"
	"database/sql"
	"github.com/Sirupsen/logrus"
	"github.com/lib/pq"
	"github.com/yezooz/null"
	"runtime/debug"
)

type UserToken struct {
	UserId    int64
	User      *User
	Token     string
	Expiry    pq.NullTime
	IsActive  bool
	CreatedAt null.Time
	UpdatedAt null.Time
}

func (ut *UserToken) GetUser() *User {
	if ut.User == nil {
		ut.User = LoadUser(ut.UserId)
	}

	return ut.User
}

func (ut *UserToken) ActiveTokenForUser(user *User) string {
	query := `
		SELECT
			user_id,
			token,
			expiry,
			is_active,
			created_at,
			updated_at
		FROM
			user_token
		WHERE
			user_id = $1 AND
			is_active = TRUE
	`

	row := postgres.NewPostgres().DB().QueryRow(query, user.Id)

	if row == nil {
		return ""
	}

	if err := ut.mapRow(row); err != nil {
		return ""
	} else {
		user.Token = ut.Token
		return ut.Token
	}
}

func (ut *UserToken) mapRow(row *sql.Row) error {
	err := row.Scan(
		&ut.UserId,
		&ut.Token,
		&ut.Expiry,
		&ut.IsActive,
		&ut.CreatedAt,
		&ut.UpdatedAt,
	)

	if err != nil && err != sql.ErrNoRows {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)
	}

	return err
}

type StoredUserToken struct {
}

type DummyUserToken struct {
}
