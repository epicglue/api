package model_test

import "bitbucket.org/epicglue/api/model"

func GetTestSubscription() *model.Subscription {
	return &model.Subscription{
		Id:     10,
		Source: GetTestSource(),
	}
}
