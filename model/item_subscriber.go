package model

type ItemSubscriber struct {
	Item             *Item             `json:"item"`
	Subscription     *Subscription     `json:"subscription"`
	UserSubscription *UserSubscription `json:"user_subscription"`
	Username         string            `json:"username,omitempty"`
}
