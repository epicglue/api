package model

import (
	"github.com/yezooz/null"
	"time"
)

type ElasticItem struct {
	Id             string   `json:"id"`
	ItemType       string   `json:"item_type,omitempty"`
	MediaType      string   `json:"media_type"`
	Service        string   `json:"service"`
	Title          string   `json:"title,omitempty"`
	Description    string   `json:"description,omitempty"`
	Author         string   `json:"author"`
	AuthorMedia    *Media   `json:"author_media,omitempty"`
	Media          []*Media `json:"media,omitempty"`
	Links          `json:"url"`
	Tags           []string   `json:"tags,omitempty"`
	Location       *Location  `json:"location,omitempty"`
	IsRead         bool       `json:"read,omitempty"`
	IsGlued        bool       `json:"glued,omitempty"`
	Points         int64      `json:"points,omitempty"`
	Comments       int64      `json:"comments,omitempty"`
	SubIdList      []int64    `json:"subs"`
	CreatedAt      *time.Time `json:"created_at"`
	UpdatedAt      *time.Time `json:"updated_at,omitempty"`
	IndexedAt      time.Time  `json:"indexed_at,omitempty"`
	ContentOrderBy *time.Time `json:"order_by"`
}

func (e *ElasticItem) ToItem() *Item {
	return &Item{
		Id:          e.Id,
		ItemType:    e.ItemType,
		MediaType:   e.MediaType,
		Service:     e.Service,
		Title:       null.StringFrom(e.Title),
		Description: null.StringFrom(e.Description),
		Author:      e.Author,
		AuthorMedia: e.AuthorMedia,
		ItemContent: ItemContent{
			CreatedAt: e.CreatedAt,
			UpdatedAt: null.TimeFrom(*e.UpdatedAt),
			OrderBy:   e.ContentOrderBy,
		},
		Media:     e.Media,
		Tags:      e.Tags,
		Links:     e.Links,
		Points:    null.IntFrom(int64(e.Points)),
		Comments:  null.IntFrom(int64(e.Comments)),
		SubIdList: e.SubIdList,
		IsRead:    e.IsRead,
		IsGlued:   e.IsGlued,
	}
}
