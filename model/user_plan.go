package model

import (
	"bitbucket.org/epicglue/api/connection/database/postgres"
	"errors"
	"fmt"
	"github.com/Sirupsen/logrus"
	"runtime/debug"
	"time"
)

type UserPlan struct {
	Id         int64
	User       *User
	Plan       *Plan
	Payment    *UserPayment
	ValidFrom  time.Time
	ValidUntil time.Time
	IsActive   bool
	CreatedAt  time.Time
}

func AddPlan(user *User, plan *Plan) error {
	up := &UserPlan{}

	lastActivePlanId, lastActivePlan := up.lastActivePlan(user)

	if lastActivePlan != nil && lastActivePlan.ProductId != plan.ProductId {
		// TODO: ???
		return errors.New("ProductIds are not matching")
	}

	if lastActivePlan == nil {
		log.Info("adding new plan")

		return up.addNewPlan(user, plan)
	} else {
		log.Info("extending active plan")

		return up.extendValidUntilPlan(lastActivePlanId, lastActivePlan)
	}
}

func (up *UserPlan) lastActivePlan(user *User) (int64, *Plan) {
	start := time.Now()

	query := `
		SELECT
			id, plan_id
		FROM
			user_plan
		WHERE
		    user_id = $1 AND
		    is_active = TRUE
		ORDER BY
			valid_until DESC
	`

	var userPlanId int64
	var planId int64
	row := postgres.NewPostgres().DB().QueryRow(query, user.Id).Scan(&userPlanId, &planId)

	if row != nil {
		log.WithFields(logrus.Fields{
			"stack":    string(debug.Stack()),
			"took":     time.Since(start),
			"took_str": time.Since(start).String(),
		}).Error("")

		return 0, nil
	}

	return userPlanId, LoadPlanById(planId)
}

func (up *UserPlan) TTLString() string {
	day := int64(3600 * 24)
	dayTTL := up.Plan.TTL / day

	ttlString := fmt.Sprintf("'%d DAYS'", dayTTL)
	if dayTTL%30 == 0 {
		ttlString = fmt.Sprintf("'%d MONTHS'", dayTTL/30)
	}

	return ttlString
}

func (up *UserPlan) addNewPlan(user *User, plan *Plan) error {
	start := time.Now()

	query := fmt.Sprintf(`
        INSERT INTO user_plan (user_id, plan_id, ttl, valid_from, valid_until, is_active, created_at) VALUES ($1, $2, $3, DATE(NOW()), DATE(NOW()) + INTERVAL %s, TRUE, NOW())
    `, up.TTLString())

	conn := postgres.NewPostgres()
	if _, err := conn.DB().Exec(query, user.Id, plan.Id, plan.TTL); err != nil {
		log.WithFields(logrus.Fields{
			"stack":    string(debug.Stack()),
			"query":    query,
			"ttl":      up.TTLString(),
			"took":     time.Since(start),
			"took_str": time.Since(start).String(),
		}).Error(err)

		return err
	}

	up.changePlan(plan)

	return nil
}

func (up *UserPlan) extendValidUntilPlan(entryId int64, plan *Plan) error {
	start := time.Now()

	// user_id is not necessary, added for security
	query := fmt.Sprintf(`
        UPDATE user_plan SET valid_until = valid_until + INTERVAL %s WHERE id = $1
    `, up.TTLString())

	conn := postgres.NewPostgres()
	if _, err := conn.DB().Exec(query, entryId); err != nil {
		log.WithFields(logrus.Fields{
			"stack":        string(debug.Stack()),
			"query":        query,
			"user_plan_id": entryId,
			"ttl":          up.TTLString(),
			"took":         time.Since(start),
			"took_str":     time.Since(start).String(),
		}).Error(err)

		return err
	}

	return nil
}

func (up *UserPlan) changePlan(newPlan *Plan) {
	if up.User.GetPlan().TTL > newPlan.TTL {
		up.downgradePlan(newPlan)
	} else if up.User.GetPlan().TTL < newPlan.TTL {
		up.upgradePlan(newPlan)
	}
}

func (up *UserPlan) upgradePlan(plan *Plan) {
	up.indexItemsNewerThen(time.Now().Add(time.Duration(plan.TTL) * time.Second))
}

func (up *UserPlan) downgradePlan(plan *Plan) {
	// TODO: delete items outside new plan's period
}

func (up *UserPlan) indexItemsNewerThen(t time.Time) error {
	query := `
		UPDATE
			item
		LEFT JOIN
			user_item ON (user_item.id = item.id)
		LEFT JOIN
			user_item_subscription ON (user_item_subscription.user_item_id = user_item.id)
		SET
			user_item_subscription.is_indexed = FALSE
		WHERE
			user_item.user_id = $1 AND
			item.content_created_at >= $2
	`

	conn := postgres.NewPostgres()
	r, err := conn.DB().Exec(query, up.User.Id, t.String())

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":   string(debug.Stack()),
			"query":   query,
			"user_id": up.User.Id,
			"time":    t.String(),
		}).Error(err)

		return err
	}

	n, _ := r.RowsAffected()

	log.WithFields(logrus.Fields{
		"user_id": up.User.Id,
		"time":    t.String(),
	}).Debug("Marked %d items to index", n)

	return nil
}
