package model

import (
	"database/sql"
	"github.com/lib/pq"
	"github.com/yezooz/null"
)

type UserServiceToken struct {
	Profile      *ServiceProfile
	Token        string
	TokenSecret  sql.NullString
	RefreshToken sql.NullString
	Expiry       pq.NullTime
	IsActive     bool
	CreatedAt    null.Time
	UpdatedAt    null.Time
}
