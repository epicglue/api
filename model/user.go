package model

import (
	"bitbucket.org/epicglue/api/connection/database/postgres"
	"database/sql"
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/yezooz/null"
	"runtime/debug"
	"strconv"
)

type User struct {
	Id        int64       `json:"id"`
	Username  string      `json:"username"`
	Email     string      `json:"email"`
	Password  null.String `json:"-"`
	Salt      null.String `json:"-"`
	Plan      *Plan       `json:"plan"`
	Devices   []*Device   `json:"-"`
	CreatedAt null.Time   `json:"-"`
	UpdatedAt null.Time   `json:"-"`
	Token     string      `json:"-"`
}

func loadBy(field string, value string) *User {
	query := fmt.Sprintf(`
		SELECT
			au.id,
			u.username,
			u.password,
			u.salt,
			au.email,
			u.created_at,
			u.updated_at
		FROM
			"user" AS u
		LEFT JOIN
			auth_user AS au ON (au.id = u.user_id)
		WHERE
			au.%s = $1
	`, field)

	u := User{}
	if err := u.mapRow(postgres.NewPostgres().DB().QueryRow(query, value)); err != nil {
		return nil
	}

	log.Debug("LoadUser")

	return &u
}

func LoadUser(id int64) *User {
	return loadBy("id", fmt.Sprintf("%d", id))
}

func LoadUserByUsername(username string) *User {
	return loadBy("username", username)
}

func (u *User) mapRow(row *sql.Row) error {
	err := row.Scan(
		&u.Id,
		&u.Username,
		&u.Password,
		&u.Salt,
		&u.Email,
		&u.CreatedAt,
		&u.UpdatedAt,
	)

	if err == sql.ErrNoRows {
		return err
	}

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return err
	}

	u.Plan = u.GetPlan()

	return nil
}

func (u *User) GetPlan() *Plan {
	if u.Plan == nil {
		u.Plan = ActivePlanByUser(u)
	}

	if u.Plan == nil {
		log.Panic("Plan not found")
	}

	return u.Plan
}

func (u *User) AddSubscriber(s *Subscription) bool {
	if u.HasSubscription(s) {
		return true
	}

	query := "INSERT INTO user_subscription (subscription_id, user_id, profile_id, created_at, updated_at) VALUES ($1, $2, $3, NOW(), NOW())"

	var pId sql.NullInt64
	if s.ServiceProfile == nil {
		pId = sql.NullInt64{Valid: false}
	} else {
		pId = sql.NullInt64{Int64: s.ServiceProfile.Id, Valid: true}
	}

	r, err := postgres.NewPostgres().DB().Exec(query, s.Id, u.Id, pId)
	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return false
	}

	if n, _ := r.RowsAffected(); n != 1 {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error("Expected 1 row affected")

		return false
	}

	return true
}

func (u *User) HasSubscription(s *Subscription) bool {
	query := "SELECT COUNT(*) as c FROM user_subscription WHERE subscription_id = $1 AND user_id = $2"
	if s.ServiceProfile != nil {
		query += " AND profile_id = " + strconv.FormatInt(s.ServiceProfile.Id, 10)
	} else {
		query += " AND profile_id is null"
	}

	var count int
	err := postgres.NewPostgres().DB().QueryRow(query, s.Id, u.Id).Scan(&count)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return false
	}

	return count > 0
}

func (u *User) IndexName() string {
	return fmt.Sprintf("user_%d", u.Id)
}

func (u *User) LogFields() map[string]interface{} {
	fields := map[string]interface{}{
		"user_id":   u.Id,
		"user_plan": u.GetPlan().ShortName,
	}
	return fields
}
