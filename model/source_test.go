package model_test

import "bitbucket.org/epicglue/api/model"

func GetTestSource() *model.Source {
	return &model.Source{
		Id:      5,
		Service: GetTestService(),
	}
}
