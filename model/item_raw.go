package model

import "github.com/yezooz/null"

type ItemRaw struct {
	Hash      string    `json:"hash"`
	JSON      []byte    `json:"json"`
	CreatedAt null.Time `json:"-"`
	UpdatedAt null.Time `json:"-"`
}
