package model

import (
	"bitbucket.org/epicglue/api/connection/database/postgres"
	"database/sql"
	"github.com/Sirupsen/logrus"
	"github.com/yezooz/null"
	"runtime/debug"
)

type Source struct {
	Id                int64       `json:"id"`
	Service           *Service    `json:"service"`
	ServiceId         int64       `json:"-"`
	ScriptName        string      `json:"-"`
	Name              string      `json:"name"`
	Description       string      `json:"description,omitempty"`
	URL               string      `json:"-"`
	AllowValue        bool        `json:"allow_value,omitempty"`
	Value             null.String `json:"value,omitempty"`
	ItemOrderingField null.String `json:"-"`
	MinRefresh        int         `json:"-"`
	IsPerUser         bool        `json:"is_per_user"`
	IsActive          bool        `json:"is_active"`
	IsPublic          bool        `json:"is_public"`
	CreatedAt         null.Time   `json:"-"`
	UpdatedAt         null.Time   `json:"-"`
}

func LoadSource(id int64) *Source {
	query := `
		SELECT
			id,
			service_id,
			script_name,
			name,
			description,
			allow_value,
			is_per_user,
			min_refresh,
			is_active,
			item_ordering_field,
			value,
			url,
			created_at,
			updated_at
		FROM
			source
		WHERE
			id = $1
	`

	s := Source{}
	if err := s.mapRow(postgres.NewPostgres().DB().QueryRow(query, id)); err != nil {
		return nil
	} else {
		return &s
	}
}

func (s *Source) mapRow(row *sql.Row) error {
	err := row.Scan(
		&s.Id,
		&s.ServiceId,
		&s.ScriptName,
		&s.Name,
		&s.Description,
		&s.AllowValue,
		&s.IsPerUser,
		&s.MinRefresh,
		&s.IsActive,
		&s.ItemOrderingField,
		&s.Value,
		&s.URL,
		&s.CreatedAt,
		&s.UpdatedAt,
	)

	if err == sql.ErrNoRows {
		return err
	}

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return err
	}

	s.Service = s.GetService()

	return nil
}

func (s *Source) GetService() *Service {
	if s.Service == nil {
		s.Service = LoadService(s.ServiceId)
	}

	return s.Service
}
