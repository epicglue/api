package model

import (
	"bitbucket.org/epicglue/api/connection/database/postgres"
	"database/sql"
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/yezooz/null"
	"runtime/debug"
	"time"
)

type Subscription struct {
	Id                          int64           `json:"id"`
	Source                      *Source         `json:"source"`
	SourceId                    int64           `json:"-"`
	User                        *User           `json:"user"`
	UserId                      null.Int        `json:"-"`
	ServiceProfile              *ServiceProfile `json:"profile,omitempty"`
	ServiceProfileId            null.Int        `json:"-"`
	Value                       null.String     `json:"value,omitempty"`
	LastRefreshAt               null.Time       `json:"last_refresh_at,omitempty"`
	DueRefresh                  null.Bool       `json:"-"`
	Status                      *Status         `json:"status,omitempty"`
	TokensUsed                  null.Int        `json:"-"`
	TokensLeft                  null.Int        `json:"-"`
	TokensResetAt               null.Time       `json:"-"`
	IsActive                    bool            `json:"is_active"`
	IsHidden                    bool            `json:"is_hidden"`
	OverwrittenBySubscriptionId null.Int        `json:"overwritten_by_subscription_id,omitempty"`
	CreatedAt                   null.Time       `json:"-"`
	UpdatedAt                   null.Time       `json:"-"`
}

func LoadSubscription(id int64) *Subscription {
	query := `
		SELECT
			id,
			source_id,
			user_id,
			profile_id,
			value,
			last_refresh_at,
			due_refresh,
			status,
			tokens_used,
			tokens_left,
			tokens_reset_at,
			is_active,
			is_hidden,
			overwritten_by_subscription_id,
			created_at,
			updated_at
		FROM
			subscription
		WHERE
			id = $1
	`
	s := Subscription{}
	if err := s.mapRow(postgres.NewPostgres().DB().QueryRow(query, id)); err != nil {
		return nil
	} else {
		return &s
	}
}

func (s *Subscription) GetSource() *Source {
	if s.Source == nil {
		s.Source = LoadSource(s.SourceId)
	}

	return s.Source
}

func (s *Subscription) GetServiceProfile() *ServiceProfile {
	if s.ServiceProfileId.Valid && s.ServiceProfile == nil {
		s.ServiceProfile = LoadServiceProfile(s.ServiceProfileId.Int64)
	}

	return s.ServiceProfile
}

func (s *Subscription) GetUser() *User {
	if s.User == nil {
		s.User = LoadUser(s.UserId.Int64)
	}

	return s.User
}

func (s *Subscription) mapRow(row *sql.Row) error {
	var status string

	err := row.Scan(
		&s.Id,
		&s.SourceId,
		&s.UserId,
		&s.ServiceProfileId,
		&s.Value,
		&s.LastRefreshAt,
		&s.DueRefresh,
		&status,
		&s.TokensUsed,
		&s.TokensLeft,
		&s.TokensResetAt,
		&s.IsActive,
		&s.IsHidden,
		&s.OverwrittenBySubscriptionId,
		&s.CreatedAt,
		&s.UpdatedAt,
	)

	if err == sql.ErrNoRows {
		return err
	}

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return err
	}

	s.Status = &Status{Value: status}
	s.Source = s.GetSource()
	if s.UserId.Valid {
		s.User = s.GetUser()
	}
	if s.ServiceProfileId.Valid {
		s.ServiceProfile = s.GetServiceProfile()
	}

	return err
}

func (s *Subscription) LoadBySource(src *Source, value string) bool {
	where := ""

	if src.IsPerUser {
		where = `
            source_id = $1 AND
            value = $2 AND
            user_id = $3
        `
	} else {
		where = `
            source_id = $1 AND
            value = $2
        `
	}

	query := fmt.Sprintf(`
		SELECT
			id,
			source_id,
			user_id,
			profile_id,
			value,
			last_refresh_at,
			due_refresh,
			status,
			tokens_used,
			tokens_left,
			tokens_reset_at,
			is_active,
			is_hidden,
			overwritten_by_subscription_id,
			created_at,
			updated_at
		FROM
			subscription
		WHERE
			%s
	`, where)

	var err error
	if src.IsPerUser {
		err = s.mapRow(postgres.NewPostgres().DB().QueryRow(query, src.Id, value, s.User.Id))
	} else {
		err = s.mapRow(postgres.NewPostgres().DB().QueryRow(query, src.Id, value))
	}

	return err == nil
}

func (s *Subscription) Insert() bool {
	query := `
		INSERT INTO subscription (
			source_id,
			user_id,
			profile_id,
			value,
			is_active,
			is_hidden,
			due_refresh,
			status,
			created_at,
			updated_at
		) VALUES (
			$1,
			$2,
			$3,
			$4,
			true,
			false,
			true,
			$5,
			NOW(),
			NOW()
		)
		RETURNING id
	`

	var id int64
	err := postgres.NewPostgres().DB().QueryRow(
		query,
		s.SourceId,
		s.UserId,
		s.ServiceProfileId,
		s.Value,
		"ready",
	).Scan(&id)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return false
	}

	s.Id = id

	return true
}

func (s *Subscription) AddSubscription(user *User, profile *ServiceProfile) bool {
	start := time.Now()

	if s.SourceId == 0 {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error("SourceId not assigned")

		return false
	}

	// TODO: refactor
	s.Source = s.GetSource()
	if s.GetSource().IsPerUser {
		s.User = user
		s.UserId = null.NewInt(user.Id, true)
	}
	if s.GetSource().IsPerUser && profile != nil {
		s.ServiceProfile = profile
		s.ServiceProfileId = null.NewInt(profile.Id, true)
	} else {
		s.ServiceProfile = nil
		s.ServiceProfileId = null.NewInt(0, false)
	}

	if !s.Source.AllowValue {
		s.Value = null.NewString("", false)
	}

	if found := s.LoadBySource(s.GetSource(), s.Value.String); found {
		return user.AddSubscriber(s)
	}

	//s.Source = s.GetSource()
	//if s.GetSource().IsPerUser {
	//	s.User = user
	//	s.UserId = null.NewInt(user.Id, true)
	//}
	//if s.GetSource().IsPerUser && profile != nil {
	//	s.ServiceProfile = profile
	//	s.ServiceProfileId = null.NewInt(profile.Id, true)
	//}
	//if !s.Source.AllowValue {
	//	s.Value = null.NewString("", false)
	//}

	if saved := s.Insert(); !saved {
		return false
	}

	log.WithFields(logrus.Fields{
		"took":     time.Since(start),
		"took_str": time.Since(start).String(),
		"user_id":  user.Id,
		"username": user.IndexName(),
		"service":  s.Source.GetService().ShortName,
		"source":   s.Source.Name,
		"value":    s.Value.String,
		"action":   "AddSubscription",
	}).Debug("Add Subscription")

	return user.AddSubscriber(s)
}
