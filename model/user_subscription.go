package model

import (
	"bitbucket.org/epicglue/api/connection/database/postgres"
	"database/sql"
	"github.com/Sirupsen/logrus"
	"github.com/yezooz/null"
	"runtime/debug"
)

type UserSubscription struct {
	Id             int64           `json:"id"`
	Subscription   *Subscription   `json:"subscription"`
	SubscriptionId int64           `json:"-"`
	User           *User           `json:"user"`
	UserId         int64           `json:"-"`
	Profile        *ServiceProfile `json:"profile"`
	ProfileId      null.Int        `json:"-"`
	CreatedAt      null.Time       `json:"-"`
	UpdatedAt      null.Time       `json:"-"`
}

func LoadUserSubscription(id int64) *UserSubscription {
	query := `
		SELECT
			id,
			subscription_id,
			user_id,
			profile_id,
			created_at,
			updated_at
		FROM
			user_subscription
		WHERE
			id = $1
	`

	u := UserSubscription{}
	if err := u.mapRow(postgres.NewPostgres().DB().QueryRow(query, id)); err != nil {
		return nil
	} else {
		return &u
	}
}

func (u *UserSubscription) mapRow(row *sql.Row) error {
	err := row.Scan(
		&u.Id,
		&u.SubscriptionId,
		&u.UserId,
		&u.ProfileId,
		&u.CreatedAt,
		&u.UpdatedAt,
	)

	if err != nil && err != sql.ErrNoRows {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)
	}

	return err
}

func (u *UserSubscription) GetSubscription() *Subscription {
	if u.Subscription == nil {
		u.Subscription = LoadSubscription(u.SubscriptionId)
	}

	return u.Subscription
}

func (u *UserSubscription) GetUser() *User {
	if u.User == nil {
		u.User = LoadUser(u.UserId)
	}

	return u.User
}

func (u *UserSubscription) GetProfile() *ServiceProfile {
	if u.Profile == nil && u.ProfileId.Valid {
		u.Profile = LoadServiceProfile(u.ProfileId.Int64)
	}

	return u.Profile
}
