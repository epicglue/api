package subscription

import "github.com/yezooz/null"

// JSON objects returned by API

type Category struct {
	Name        string      `json:"name"`
	Description null.String `json:"description,emitempty"`
}

type Service struct {
	Id               int64     `json:"id"`
	Name             string    `json:"name"`
	ShortName        string    `json:"short_name"`
	Description      string    `json:"description,omitempty"`
	Category         *Category `json:"category"`
	Sources          []*Source `json:"sources"`
	IsLocked         bool      `json:"is_locked"`
	IsLockedOriginal bool      `json:"is_locked_original"`
	ServiceUser      string    `json:"user_id,omitempty"`
	ServiceUsername  string    `json:"username,omitempty"`
}

type Source struct {
	Id            int64           `json:"id"`
	Name          string          `json:"name"`
	Description   string          `json:"description,omitempty"`
	Value         string          `json:"value,omitempty"`
	ValueAllowed  bool            `json:"value_allowed"`
	ValueHint     string          `json:"value_hint,omitempty"`
	IsLocked      bool            `json:"is_locked"`
	Subscriptions []*Subscription `json:"subscriptions"`
}

type Subscription struct {
	Id    int64  `json:"id"`
	Value string `json:"value,omitempty"`
}

type ServiceList struct {
	Services []*Service `json:"data"`
}
