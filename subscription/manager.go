package subscription

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/item/data_source"
	"bitbucket.org/epicglue/api/model"
)

// Managing Subscriptions

var log = helpers.GetLogger("subscription")

type SubscriptionManager interface {
	Subscribe(*model.Source, *model.ServiceProfile, string) (*model.Subscription, error)
	Unsubscribe(*model.UserSubscription) error

	Counters(*data_source.DataSource) map[string]uint64
	Subscriptions() (*ServiceList, error)
	UserSubscriptions() (*ServiceList, error)
}
