package subscription_manager

import (
	"bitbucket.org/epicglue/api/item/data_source"
	"fmt"
)

func (m *DefaultSubscriptionManager) Counters(dataSource *data_source.DataSource) map[string]uint64 {
	defer m.getMetric().FetchedCounters(m.user)

	log.Info("%d got counters", m.user.Id)

	return m.getSubs(dataSource)
}

func (m *DefaultSubscriptionManager) getSubs(dataSource *data_source.DataSource) map[string]uint64 {
	var (
		items map[string]uint64 = make(map[string]uint64)
	)

	for _, subId := range m.getDB().SubscriptionIds(m.user) {
		items[fmt.Sprintf("%d", subId)] = m.unreadItemsCountForSubId(int64(subId), dataSource)
	}

	return items
}

func (m *DefaultSubscriptionManager) unreadItemsCountForSubId(subId int64, dataSource *data_source.DataSource) uint64 {
	ds := *dataSource
	ds.SetKeyAndValue(data_source.PARAM_SUBSCRIPTION, fmt.Sprintf("%d", subId))

	return uint64(m.getIndex().Count(m.user, &ds))
}
