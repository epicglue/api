package subscription_manager

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/model"
	"bitbucket.org/epicglue/api/subscription/store/cache/redis"
	"bitbucket.org/epicglue/api/subscription/store/db/postgres"
	"bitbucket.org/epicglue/api/subscription/store/index/elasticsearch"
	"bitbucket.org/epicglue/api/subscription/store/metric/postgres"
	"net/http"
)

var log = helpers.GetLogger("subscription")

type DefaultSubscriptionManager struct {
	user   *model.User
	db     *subscription_store_postgres.SubscriptionStorePostgres
	index  *subscription_store_elasticsearch.SubscriptionStoreElasticsearch
	cache  *subscription_store_redis.SubscriptionStoreRedis
	metric *subscription_store_metric_postgres.SubscriptionMetricPostgres
}

func NewDefaultSubscriptionManager(r *http.Request) *DefaultSubscriptionManager {
	if user := model.LoadUserByUsername(r.Header.Get("User")); user != nil {
		return &DefaultSubscriptionManager{
			user: user,
		}
	}

	log.Panic("User not set")
	return nil
}

func NewDefaultSubscriptionManagerWithUser(user *model.User) *DefaultSubscriptionManager {
	return &DefaultSubscriptionManager{
		user: user,
	}
}

func (m *DefaultSubscriptionManager) getDB() *subscription_store_postgres.SubscriptionStorePostgres {
	if m.db == nil {
		m.db = subscription_store_postgres.NewSubscriptionStorePostgres()
	}
	return m.db
}

func (m *DefaultSubscriptionManager) getIndex() *subscription_store_elasticsearch.SubscriptionStoreElasticsearch {
	if m.index == nil {
		m.index = subscription_store_elasticsearch.NewSubscriptionStoreElasticsearch()
	}
	return m.index
}

func (m *DefaultSubscriptionManager) getCache() *subscription_store_redis.SubscriptionStoreRedis {
	if m.cache == nil {
		m.cache = subscription_store_redis.NewSubscriptionStoreRedis()
	}

	return m.cache
}

func (m *DefaultSubscriptionManager) getMetric() *subscription_store_metric_postgres.SubscriptionMetricPostgres {
	if m.metric == nil {
		m.metric = subscription_store_metric_postgres.NewSubscriptionMetricPostgres()
	}
	return m.metric
}
