package subscription_manager

import (
	"bitbucket.org/epicglue/api/model"
)

func (m *DefaultSubscriptionManager) Subscribe(source *model.Source, serviceProfile *model.ServiceProfile, value string) (*model.Subscription, error) {
	sub, err := m.getDB().Subscribe(m.user, source, serviceProfile, value)

	if err != nil {
		return nil, err
	}

	m.getMetric().Subscribed(sub, m.user)

	log.Infof("%d subscribed to %s %s", m.user.Id, source.Name, value)

	return sub, nil
}
