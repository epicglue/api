package subscription_manager

import (
	"bitbucket.org/epicglue/api/subscription"
)

func (m *DefaultSubscriptionManager) Subscriptions() (*subscription.ServiceList, error) {
	services, err := m.getDB().Subscriptions()

	m.getMetric().FetchedSubscriptions()

	log.Infof("%d got subscriptions", m.user.Id)

	return services, err
}

func (m *DefaultSubscriptionManager) UserSubscriptions() (*subscription.ServiceList, error) {
	services, err := m.getDB().SubscriptionsForUser(m.user)

	m.getMetric().FetchedUserSubscriptions(m.user)

	log.Infof("%d got user subscriptions", m.user.Id)

	return services, err
}
