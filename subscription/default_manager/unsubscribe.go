package subscription_manager

import (
	"bitbucket.org/epicglue/api/item/data_source"
	"bitbucket.org/epicglue/api/item/default_manager"
	"bitbucket.org/epicglue/api/model"
	"fmt"
)

func (m *DefaultSubscriptionManager) Unsubscribe(unSub *model.UserSubscription) error {
	if err := m.getDB().Unsubscribe(unSub); err != nil {
		return err
	}

	dataSource := &data_source.DataSource{}
	dataSource.SetKeyAndValue(data_source.PARAM_SUBSCRIPTION, fmt.Sprintf("%d", unSub.Id))

	itemManager := item_manager.NewDefaultItemManagerWithUser(unSub.User)
	itemManager.Delete(dataSource)

	m.getMetric().Unsubscribed(unSub)

	log.Info("%d unsubscribed from %s", m.user.Id, unSub.GetSubscription().GetSource().Name)

	return nil
}
