package request

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/item/data_source"
	"bitbucket.org/epicglue/api/subscription"
	"bitbucket.org/epicglue/api/subscription/default_manager"
	"encoding/json"
	"errors"
	"github.com/Sirupsen/logrus"
	"io"
	"net/http"
	"runtime/debug"
	"time"
)

var log = helpers.GetLogger("subscription")

// Subscribe based on SubscribeRequest

type CountRequest struct {
	manager subscription.SubscriptionManager

	IsUnread  bool       `json:"is_unread"`
	IsGlued   bool       `json:"is_glued"`
	Blacklist string     `json:"blacklist"`
	After     *time.Time `json:"after"`
}

func NewCountRequest(r *http.Request) (*CountRequest, error) {
	request := CountRequest{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&request)

	if err != nil && err != io.EOF {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)
	}

	if err := request.validate(); err != nil {
		return nil, err
	}

	request.manager = subscription_manager.NewDefaultSubscriptionManager(r)

	return &request, nil
}

func (req *CountRequest) validate() error {
	return nil
}

func (req *CountRequest) validationError(msg string) error {
	return errors.New(msg)
}

func (req *CountRequest) Count() map[string]uint64 {
	ds := data_source.DataSource{}

	if req.IsUnread {
		ds.SetKey(data_source.PARAM_IS_UNREAD)
	}
	if req.IsGlued {
		ds.SetKey(data_source.PARAM_IS_GLUED)
	}
	if req.Blacklist != "" {
		ds.SetKeyAndValue(data_source.PARAM_BLACKLIST_ID, req.Blacklist)
	}
	//if null.TimeFrom(*req.After).Valid {
	//    ds.SetKeyAndValue(data_source.PARAM_AFTER, req.After.String())
	//}

	return req.manager.Counters(&ds)
}
