package request

import (
	"bitbucket.org/epicglue/api/subscription"
	"bitbucket.org/epicglue/api/subscription/default_manager"
	"errors"
	"net/http"
)

type ListSubscriptionsRequest struct {
	manager subscription.SubscriptionManager
}

func NewListSubscriptionsFromHttpRequest(r *http.Request) (ListSubscriptionsRequest, error) {
	request := ListSubscriptionsRequest{}

	// TODO: is it correct pattern? Manager should use Request IMO
	request.manager = subscription_manager.NewDefaultSubscriptionManager(r)

	return request, nil
}

func (request ListSubscriptionsRequest) validate() error {
	return nil
}

func (request ListSubscriptionsRequest) validationError(msg string) error {
	return errors.New(msg)
}

func (request ListSubscriptionsRequest) Subscriptions() (*subscription.ServiceList, error) {
	return request.manager.Subscriptions()
}

func (request ListSubscriptionsRequest) UserSubscriptions() (*subscription.ServiceList, error) {
	return request.manager.UserSubscriptions()
}
