package request

import (
	"bitbucket.org/epicglue/api/model"
	"bitbucket.org/epicglue/api/subscription"
	"bitbucket.org/epicglue/api/subscription/default_manager"
	"encoding/json"
	"errors"
	"github.com/Sirupsen/logrus"
	"net/http"
	"runtime/debug"
)

// Subscribe based on SubscribeRequest

type SubscribeRequest struct {
	manager subscription.SubscriptionManager
	source  *model.Source
	profile *model.ServiceProfile

	SourceId  int64  `json:"source_id"`
	ProfileId int64  `json:"profile_id,omitempty"`
	Value     string `json:"value,omitempty"`
}

func NewSubscribeRequest(r *http.Request) (*SubscribeRequest, error) {
	request := SubscribeRequest{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&request)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)
	}

	if err := request.validate(); err != nil {
		return nil, err
	}

	request.manager = subscription_manager.NewDefaultSubscriptionManager(r)

	return &request, nil
}

func (req *SubscribeRequest) validate() error {
	// Check basic values
	if req.SourceId == 0 {
		return req.validationError("Values invalid")
	}

	// Check Source
	source := model.LoadSource(req.SourceId)
	if source == nil {
		return req.validationError("Failed to load source")
	}

	if source.AllowValue && req.Value == "" {
		return req.validationError("Value should not be empty")
	}

	// Check Profile
	if req.ProfileId != 0 {
		profile := model.LoadServiceProfile(req.ProfileId)
		if profile == nil {
			return req.validationError("Failed to load profile")
		}

		req.profile = profile
	}

	req.source = source

	return nil
}

func (req *SubscribeRequest) validationError(msg string) error {
	return errors.New(msg)
}

func (req *SubscribeRequest) Subscribe() bool {
	_, err := req.manager.Subscribe(req.source, req.profile, req.Value)
	return err == nil
}
