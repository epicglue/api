package request

import (
	"bitbucket.org/epicglue/api/model"
	"bitbucket.org/epicglue/api/subscription"
	"bitbucket.org/epicglue/api/subscription/default_manager"
	"encoding/json"
	"errors"
	"github.com/Sirupsen/logrus"
	"net/http"
	"runtime/debug"
)

type UnsubscribeRequest struct {
	manager      subscription.SubscriptionManager
	subscription *model.UserSubscription
	user         *model.User

	UserSubId int64 `json:"id"`
}

func NewUnsubscribeRequest(r *http.Request) (*UnsubscribeRequest, error) {
	request := UnsubscribeRequest{
		user: model.LoadUserByUsername(r.Header.Get("User")),
	}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&request)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)
	}

	if err := request.validate(); err != nil {
		return nil, err
	}

	// TODO: is it correct pattern? Manager should use Request IMO
	request.manager = subscription_manager.NewDefaultSubscriptionManager(r)

	return &request, nil
}

func (req *UnsubscribeRequest) validate() error {
	// Check basic values
	if req.UserSubId == 0 {
		return req.validationError("Id invalid")
	}

	sub := model.LoadUserSubscription(req.UserSubId)
	if sub == nil {
		return req.validationError("Failed to load user subscription")
	}

	if sub.GetUser().Id != req.user.Id {
		return req.validationError("Invalid user id")
	}

	req.subscription = sub

	return nil
}

func (req *UnsubscribeRequest) validationError(msg string) error {
	return errors.New(msg)
}

func (req *UnsubscribeRequest) Unsubscribe() bool {
	return req.manager.Unsubscribe(req.subscription) == nil
}
