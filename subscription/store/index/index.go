package subscription_store_index

import (
	"bitbucket.org/epicglue/api/item/data_source"
	"bitbucket.org/epicglue/api/model"
)

type SubscriptionStoreIndex interface {
	Count(*model.User, *data_source.DataSource) int64
}
