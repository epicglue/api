package subscription_store_elasticsearch

import (
	"bitbucket.org/epicglue/api/item/data_source"
	"bitbucket.org/epicglue/api/model"
	"github.com/Sirupsen/logrus"
	"time"
)

func (m SubscriptionStoreElasticsearch) Count(user *model.User, ds *data_source.DataSource) int64 {
	start := time.Now()

	count := m.Connection.Count(user.IndexName(), ds.BuildSearchQuery(), m.CountLimit)

	log.WithFields(logrus.Fields{
		"ds":       ds.LogFields(),
		"user_id":  user.Id,
		"result":   count,
		"took":     time.Since(start),
		"took_str": time.Since(start).String(),
	}).Debug("Count")

	return count
}
