package subscription_store_elasticsearch

import (
	"bitbucket.org/epicglue/api/config"
	"bitbucket.org/epicglue/api/connection/index/elasticsearch"
	"bitbucket.org/epicglue/api/helpers"
)

var log = helpers.GetLogger("subscription")

type SubscriptionStoreElasticsearch struct {
	Connection *elasticsearch.Elasticsearch
	CountLimit int64
}

func NewSubscriptionStoreElasticsearch() *SubscriptionStoreElasticsearch {
	config := config.LoadConfig()

	return &SubscriptionStoreElasticsearch{
		Connection: elasticsearch.NewElasticsearch(),
		CountLimit: config.API.CountLimit,
	}
}
