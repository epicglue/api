package subscription_store_postgres

import (
	"bitbucket.org/epicglue/api/connection/database/postgres"
	"bitbucket.org/epicglue/api/model"
	"github.com/Sirupsen/logrus"
	"runtime/debug"
)

func (db SubscriptionStorePostgres) SubscriptionIds(user *model.User) []uint64 {
	query := `
		SELECT
			us.id AS user_sub_id
		FROM
			source AS src
		JOIN
			service AS srv ON (srv.id = src.service_id)
		JOIN
			category AS cat ON (cat.id = srv.category_id)
		LEFT JOIN
			subscription AS s ON (src.id = s.source_id)
		LEFT JOIN
			user_subscription AS us ON (us.subscription_id = s.id)
		WHERE
			src.is_active = true AND
			src.is_public = true AND
			(
				us.user_id = $1 AND
				s.is_active = true AND
				s.is_hidden = false
			) OR (
				us.user_id = NULL
			)
	`

	rows, err := postgres.NewPostgres().DB().Query(query, user.Id)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return make([]uint64, 0)
	}

	var (
		items []uint64 = make([]uint64, 0)
		subId uint64
	)

	for rows.Next() {
		err = rows.Scan(
			&subId,
		)

		if err != nil {
			log.WithFields(logrus.Fields{
				"stack": string(debug.Stack()),
			}).Warningf("SubscriptionIds %s", err)

			continue
		}

		items = append(items, subId)
	}
	rows.Close()

	return items
}
