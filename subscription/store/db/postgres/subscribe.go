package subscription_store_postgres

import (
	"bitbucket.org/epicglue/api/model"
	"errors"
	"github.com/yezooz/null"
)

func (db SubscriptionStorePostgres) Subscribe(user *model.User, source *model.Source, serviceProfile *model.ServiceProfile, value string) (*model.Subscription, error) {
	sub := model.Subscription{
		SourceId: source.Id,
		Source:   source,
		Value:    null.StringFrom(value),
	}

	if serviceProfile == nil {
		serviceProfile = db.firstActiveProfileForSource(user, source.Service)
	}

	if success := sub.AddSubscription(
		user,
		serviceProfile,
	); !success {
		return nil, errors.New("Failed to subscribe")
	}

	// TODO: copy items, if possible

	return &sub, nil
}
