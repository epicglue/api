package subscription_store_postgres

import (
	"bitbucket.org/epicglue/api/model"
	"bitbucket.org/epicglue/api/subscription"
	"database/sql"
	"github.com/Sirupsen/logrus"
	"github.com/yezooz/null"
	"runtime/debug"
	"time"
)

func (db SubscriptionStorePostgres) Subscriptions() (*subscription.ServiceList, error) {
	start := time.Now()

	rows, err := db.getSubsResults()
	if err != nil {
		return nil, err
	}

	var (
		lastService *subscription.Service
		lastSource  *subscription.Source
		items       *subscription.ServiceList = &subscription.ServiceList{
			Services: make([]*subscription.Service, 0),
		}
		sourceId            int64
		sourceName          string
		sourceDescription   null.String
		sourceURL           string
		sourceIsPerUser     bool
		sourceValue         null.String
		sourceAllowValue    bool
		sourceValueHint     null.String
		sourceIsLocked      null.Bool
		serviceId           int64
		serviceShortName    string
		serviceName         string
		serviceDescription  null.String
		serviceIsLocked     null.Bool
		categoryName        string
		categoryDescription null.String
	)

	for rows.Next() {
		err = rows.Scan(
			&sourceId,
			&sourceName,
			&sourceDescription,
			&sourceURL,
			&sourceIsPerUser,
			&sourceValue,
			&sourceAllowValue,
			&sourceValueHint,
			&sourceIsLocked,
			&serviceId,
			&serviceShortName,
			&serviceName,
			&serviceDescription,
			&serviceIsLocked,
			&categoryName,
			&categoryDescription,
		)

		if err != nil {
			log.WithFields(logrus.Fields{
				"took":     time.Since(start),
				"took_str": time.Since(start).String(),
				"stack":    string(debug.Stack()),
			}).Error(err)

			continue
		}

		if lastSource == nil || lastSource.Id != sourceId {
			if lastSource != nil {
				lastService.Sources = append(lastService.Sources, lastSource)
			}

			lastSource = &subscription.Source{
				Id:            sourceId,
				Name:          sourceName,
				Description:   sourceDescription.String,
				IsLocked:      sourceIsLocked.Bool,
				ValueAllowed:  sourceAllowValue,
				ValueHint:     sourceValueHint.String,
				Subscriptions: make([]*subscription.Subscription, 0),
			}
		}

		if lastService == nil || lastService.Id != serviceId {
			if lastService != nil {
				items.Services = append(items.Services, lastService)
			}

			lastService = &subscription.Service{
				Category: &subscription.Category{
					Name:        categoryName,
					Description: categoryDescription,
				},
				Id:          serviceId,
				Name:        serviceName,
				ShortName:   serviceShortName,
				Description: serviceDescription.String,
				IsLocked:    serviceIsLocked.Bool,
				Sources:     make([]*subscription.Source, 0),
			}
		}
	}
	rows.Close()

	if lastSource != nil && lastService != nil {
		lastService.Sources = append(lastService.Sources, lastSource)
	}

	if lastService != nil {
		items.Services = append(items.Services, lastService)
	}

	log.WithFields(logrus.Fields{
		"took":     time.Since(start),
		"took_str": time.Since(start).String(),
		"module":   "subscription",
		"method":   "GetAll",
	}).Debug("Subscriptions")

	return items, nil
}

func (db SubscriptionStorePostgres) SubscriptionsForUser(user *model.User) (*subscription.ServiceList, error) {
	start := time.Now()

	rows, err := db.getUserSubsResults(user)
	if err != nil {
		return nil, err
	}

	allSubscriptions, err := db.Subscriptions()
	if err != nil {
		return nil, err
	}

	var (
		allItems            *subscription.ServiceList = allSubscriptions
		subId               null.Int
		subStatus           null.String
		subValue            null.String
		userSubId           null.Int
		sourceId            int64
		sourceName          string
		sourceDescription   null.String
		sourceURL           string
		sourceIsPerUser     bool
		sourceValue         null.String
		sourceAllowValue    bool
		sourceValueHint     null.String
		sourceIsLocked      null.Bool
		profileIdentifier   null.String
		profileFriendlyName null.String
	)

	for rows.Next() {
		err = rows.Scan(
			&subId,
			&subValue,
			&sourceId,
			&sourceName,
			&sourceDescription,
			&sourceURL,
			&sourceIsPerUser,
			&sourceValue,
			&sourceAllowValue,
			&sourceValueHint,
			&sourceIsLocked,
			&subStatus,
			&profileIdentifier,
			&profileFriendlyName,
			&userSubId,
		)

		if err != nil {
			log.WithFields(logrus.Fields{
				"took":     time.Since(start),
				"took_str": time.Since(start).String(),
				"stack":    string(debug.Stack()),
			}).Error(err)

			continue
		}

		for _, srv := range allItems.Services {
			for _, src := range srv.Sources {
				if src.Id == sourceId {
					if profileIdentifier.Valid {
						srv.ServiceUser = profileIdentifier.String
						srv.ServiceUsername = profileFriendlyName.String
					}

					src.Subscriptions = append(src.Subscriptions, &subscription.Subscription{
						Id:    userSubId.Int64,
						Value: subValue.String,
					})
					break
				}
			}
		}
	}
	rows.Close()

	log.WithFields(logrus.Fields{
		"took":     time.Since(start),
		"took_str": time.Since(start).String(),
		"module":   "subscription",
		"method":   "GetUsers",
	}).Debug("SubscriptionsForUser")

	for _, srv := range allItems.Services {
		hasTokenToUnlock := false
		srv.IsLockedOriginal = srv.IsLocked
		if srv.IsLocked && db.hasActiveTokenForSource(user, srv) {
			hasTokenToUnlock = true
			srv.IsLocked = false

			activeProfile := db.firstActiveProfileForSource(user, &model.Service{
				Id: srv.Id,
			})
			srv.ServiceUsername = activeProfile.FriendlyName
			srv.ServiceUser = activeProfile.Identifier
		}

		for _, src := range srv.Sources {
			if src.IsLocked && hasTokenToUnlock {
				src.IsLocked = false
			}
		}
	}

	return allItems, nil
}

// TODO: use method below to reuse most code
func (db SubscriptionStorePostgres) hasActiveTokenForSource(user *model.User, service *subscription.Service) bool {
	var numberOfReturnedTokens int

	sqlQuery := `
		SELECT
			COUNT(*)
		FROM
			"user_service_token" AS ust
		LEFT JOIN
		    "user_service_profile" AS usp ON (usp.id = ust.profile_id)
		WHERE
			ust.is_active = TRUE AND
			usp.service_id = $1 AND
			usp.user_id = $2
	`

	if err := db.Connection.DB().QueryRow(sqlQuery, service.Id, user.Id).Scan(&numberOfReturnedTokens); err != nil {
		log.WithFields(logrus.Fields{
			"service_id": service.Id,
			"user_id":    user.Id,
			"stack":      string(debug.Stack()),
		}).Error(err)

		return false
	}

	return numberOfReturnedTokens > 0
}

func (db SubscriptionStorePostgres) firstActiveProfileForSource(user *model.User, service *model.Service) *model.ServiceProfile {
	var (
		id           int64
		identifier   string
		friendlyName string
	)

	sqlQuery := `
		SELECT
		    usp.id,
			usp.identifier,
			usp.friendly_name
		FROM
			"user_service_token" AS ust
		LEFT JOIN
		    "user_service_profile" AS usp ON (usp.id = ust.profile_id)
		WHERE
			ust.is_active = TRUE AND
			usp.service_id = $1 AND
			usp.user_id = $2
	`

	if err := db.Connection.DB().QueryRow(sqlQuery, service.Id, user.Id).Scan(&id, &identifier, &friendlyName); err != nil {
		if err == sql.ErrNoRows {
			return nil
		}

		log.WithFields(logrus.Fields{
			"stack":      string(debug.Stack()),
			"service_id": service.Id,
			"user_id":    user.Id,
		}).Error(err)

		return nil
	}

	if identifier == "" {
		return nil
	}

	return &model.ServiceProfile{
		Id:           id,
		User:         user,
		Service:      service,
		Identifier:   identifier,
		FriendlyName: friendlyName,
	}
}

func (db SubscriptionStorePostgres) getSubsResults() (*sql.Rows, error) {
	query := `
		SELECT
			src.id AS src_id,
			src.name,
			src.description,
			src.url,
			src.is_per_user,
			src.value AS src_value,
			src.allow_value AS src_allow_value,
			src.value_hint AS src_value_hint,
			src.is_locked AS src_is_locked,
			srv.id AS srv_id,
			srv.short_name AS srv_short_name,
			srv.name AS srv_name,
			srv.description AS srv_description,
			srv.is_locked AS srv_is_locked,
			cat.name AS cat_name,
			cat.description AS cat_description
		FROM
			source AS src
		JOIN
			service AS srv ON (srv.id = src.service_id)
		JOIN
			category AS cat ON (cat.id = srv.category_id)
		WHERE
			srv.is_visible = TRUE AND
			src.is_active = TRUE AND
			src.is_public = TRUE
		ORDER BY
			srv.name,
			src.name
	`

	rows, err := db.Connection.DB().Query(query)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return nil, err
	}

	return rows, err
}

func (db SubscriptionStorePostgres) getUserSubsResults(user *model.User) (*sql.Rows, error) {
	query := `
		SELECT
			s.id AS sub_id,
			s.value AS s_value,
			src.id AS src_id,
			src.name,
			src.description,
			src.url,
			src.is_per_user,
			src.value AS src_value,
			src.allow_value AS src_allow_value,
			src.value_hint AS src_value_hint,
			src.is_locked AS src_is_locked,
			s.status,
			p.identifier,
			p.friendly_name,
			us.id AS user_sub_id
		FROM
			source AS src
		JOIN
			subscription AS s ON (src.id = s.source_id)
		JOIN
			user_subscription AS us ON (us.subscription_id = s.id)
		LEFT JOIN
			user_service_profile AS p ON (p.id = us.profile_id)
		JOIN
			"user" AS u ON (u.id = us.user_id)
		WHERE
			us.user_id = $1 AND
			src.is_active = TRUE AND
			src.is_public = TRUE AND
			s.is_active = TRUE AND
			s.is_hidden = FALSE
		ORDER BY
			s.value
	`

	rows, err := db.Connection.DB().Query(query, user.Id)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return nil, err
	}

	return rows, err
}
