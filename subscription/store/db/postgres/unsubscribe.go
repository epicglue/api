package subscription_store_postgres

import (
	"bitbucket.org/epicglue/api/connection/database/postgres"
	"bitbucket.org/epicglue/api/model"
	"github.com/Sirupsen/logrus"
	"runtime/debug"
	"time"
)

func (db SubscriptionStorePostgres) Unsubscribe(usub *model.UserSubscription) error {
	start := time.Now()

	// Cascade delete
	query := "DELETE FROM user_item_subscription WHERE user_subscription_id = $1"

	_, err := db.Connection.DB().Exec(query, usub.Id)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return err
	}

	query = "DELETE FROM user_subscription WHERE id = $1 AND user_id = $2"

	_, err = postgres.NewPostgres().DB().Exec(query, usub.Id, usub.UserId)
	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return err
	}

	log.WithFields(logrus.Fields{
		"took":     time.Since(start),
		"took_str": time.Since(start).String(),
		"user_id":  usub.GetUser().Id,
		"source":   usub.GetSubscription().GetSource().Name,
		"value":    usub.GetSubscription().Value.String,
		"action":   "RemoveSubscription",
	}).Debugf("Remove Subscription")

	return nil
}
