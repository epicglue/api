package subscription_store_postgres

import (
	"bitbucket.org/epicglue/api/connection/database/postgres"
	"bitbucket.org/epicglue/api/helpers"
	"github.com/jinzhu/gorm"
)

var log = helpers.GetLogger("item_store")

type SubscriptionStorePostgres struct {
	Connection *gorm.DB
}

func NewSubscriptionStorePostgres() *SubscriptionStorePostgres {
	return &SubscriptionStorePostgres{
		Connection: postgres.NewPostgres(),
	}
}
