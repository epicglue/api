package subscription_store_db

import (
	"bitbucket.org/epicglue/api/model"
	"bitbucket.org/epicglue/api/subscription"
)

type SubscriptionStoreDB interface {
	Subscriptions() (*subscription.ServiceList, error)
	SubscriptionsForUser(*model.User) (*subscription.ServiceList, error)
	SubscriptionIds(*model.User) []uint64

	Subscribe(*model.User, *model.Source, *model.ServiceProfile, string) (*model.Subscription, error)
	Unsubscribe(*model.UserSubscription) error
}
