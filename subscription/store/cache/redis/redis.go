package subscription_store_redis

import (
	"bitbucket.org/epicglue/api/connection/key_value_store"
	"bitbucket.org/epicglue/api/connection/key_value_store/redis"
)

type SubscriptionStoreRedis struct {
	Connection key_value_store.KeyValueStore
}

func NewSubscriptionStoreRedis() *SubscriptionStoreRedis {
	return &SubscriptionStoreRedis{
		Connection: redis.NewRedis(),
	}
}
