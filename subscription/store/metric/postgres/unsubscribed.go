package subscription_store_metric_postgres

import (
	"bitbucket.org/epicglue/api/metric/postgres"
	"bitbucket.org/epicglue/api/model"
	"fmt"
)

func (m SubscriptionMetricPostgres) Unsubscribed(sub *model.UserSubscription) {
	// General metrics
	m.metric = metric_postgres.NewPostgresMetric()
	m.metric.Dec("subs")
	m.metric.Dec(fmt.Sprintf("subs.%s", sub.Subscription.Source.Service.ShortName))
	m.metric.Inc("subs.rem")
	m.metric.Inc(fmt.Sprintf("subs.%s.rem", sub.Subscription.Source.Service.ShortName))

	if sub.Subscription.Value.Valid {
		m.metric.Dec(fmt.Sprintf("subs.%s.%s", sub.Subscription.Source.Service.ShortName, sub.Subscription.Value.String))
		m.metric.Inc(fmt.Sprintf("subs.%s.%s.rem", sub.Subscription.Source.Service.ShortName, sub.Subscription.Value.String))
	}

	// Per-user metrics
	m.metric = metric_postgres.NewPostgresMetricWithUser(sub.User)
	m.metric.Dec("subs")
	m.metric.Dec(fmt.Sprintf("subs.%s", sub.Subscription.Source.Service.ShortName))
	m.metric.Inc("subs.rem")
	m.metric.Inc(fmt.Sprintf("subs.%s.rem", sub.Subscription.Source.Service.ShortName))

	if sub.Subscription.Value.Valid {
		m.metric.Dec(fmt.Sprintf("subs.%s.%s", sub.Subscription.Source.Service.ShortName, sub.Subscription.Value.String))
		m.metric.Inc(fmt.Sprintf("subs.%s.%s.rem", sub.Subscription.Source.Service.ShortName, sub.Subscription.Value.String))
	}
}
