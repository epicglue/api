package subscription_store_metric_postgres

import (
	"bitbucket.org/epicglue/api/metric/postgres"
	"bitbucket.org/epicglue/api/model"
)

func (m SubscriptionMetricPostgres) FetchedCounters(user *model.User) {
	// General metrics
	m.metric = metric_postgres.NewPostgresMetric()
	m.metric.Inc("subs.count")

	// Per-user metrics
	m.metric = metric_postgres.NewPostgresMetricWithUser(user)
	m.metric.Inc("user.subs.count")
}
