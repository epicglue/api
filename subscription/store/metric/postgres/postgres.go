package subscription_store_metric_postgres

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/metric"
)

var log = helpers.GetLogger("metric")

type SubscriptionMetricPostgres struct {
	metric metric.Metric
}

func NewSubscriptionMetricPostgres() *SubscriptionMetricPostgres {
	return &SubscriptionMetricPostgres{
		metric: nil,
	}
}
