package subscription_store_metric_postgres

import (
	"bitbucket.org/epicglue/api/metric/postgres"
	"bitbucket.org/epicglue/api/model"
	"fmt"
)

func (m SubscriptionMetricPostgres) Subscribed(sub *model.Subscription, user *model.User) {
	// General metrics
	m.metric = metric_postgres.NewPostgresMetric()
	m.metric.Inc("subs")
	m.metric.Inc(fmt.Sprintf("subs.%s", sub.Source.Service.ShortName))
	m.metric.Inc("subs.add")
	m.metric.Inc(fmt.Sprintf("subs.%s.add", sub.Source.Service.ShortName))

	if sub.Value.Valid {
		m.metric.Inc(fmt.Sprintf("subs.%s.%s", sub.Source.Service.ShortName, sub.Value.String))
		m.metric.Inc(fmt.Sprintf("subs.%s.%s.add", sub.Source.Service.ShortName, sub.Value.String))
	}

	// Per-user metrics
	m.metric = metric_postgres.NewPostgresMetricWithUser(user)
	m.metric.Inc("subs")
	m.metric.Inc(fmt.Sprintf("subs.%s", sub.Source.Service.ShortName))
	m.metric.Inc("subs.add")
	m.metric.Inc(fmt.Sprintf("subs.%s.add", sub.Source.Service.ShortName))

	if sub.Value.Valid {
		m.metric.Inc(fmt.Sprintf("subs.%s.%s", sub.Source.Service.ShortName, sub.Value.String))
		m.metric.Inc(fmt.Sprintf("subs.%s.%s.add", sub.Source.Service.ShortName, sub.Value.String))
	}
}
