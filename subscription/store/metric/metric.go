package subscription_store_metric

import "bitbucket.org/epicglue/api/model"

type SubscriptionStoreMetric interface {
	FetchedSubscriptions()
	FetchedUserSubscriptions(*model.User)
	FetchedCounters(*model.User)

	Subscribed(*model.Subscription, *model.User)
	Unsubscribed(*model.UserSubscription)
}
