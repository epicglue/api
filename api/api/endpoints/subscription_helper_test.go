// +build integration

package endpoints_test

import (
	"bitbucket.org/epicglue/api/connection/database/postgres"
	"bitbucket.org/epicglue/api/model"
)

func findSourceByAttributes(is_per_user bool, allow_value bool) int64 {
	query := `
		SELECT
			id
		FROM
			source
		WHERE
			is_per_user = $1 AND
			allow_value = $2
	`

	var sourceId int64
	row := postgres.NewPostgres().DB().QueryRow(query, is_per_user, allow_value)
	row.Scan(&sourceId)

	return sourceId
}

func findSourcePerUserWithValue() int64 {
	return findSourceByAttributes(true, true)
}

func findSourceWithValue() int64 {
	return findSourceByAttributes(false, true)
}

func findSourcePerUserWithoutValue() int64 {
	return findSourceByAttributes(true, false)
}

func findSourceWithoutValue() int64 {
	return findSourceByAttributes(false, false)
}

func howManySubscriptionsUserHas(userId int64) int64 {
	query := `
		SELECT
			COUNT(*)
		FROM
			user_subscription
		WHERE
			user_id = $1
	`

	var count int64
	row := postgres.NewPostgres().DB().QueryRow(query, userId)
	row.Scan(&count)

	return count
}

func latestSubscriptionOfUser(userId int64) *model.UserSubscription {
	query := `
		SELECT
			id
		FROM
			user_subscription
		WHERE
			user_id = $1
		ORDER BY
		    created_at DESC
	`

	var lastSubId int64
	row := postgres.NewPostgres().DB().QueryRow(query, userId)
	row.Scan(&lastSubId)

	return model.LoadUserSubscription(lastSubId)
}
