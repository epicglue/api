// +build integration

package endpoints_test

import (
	"bitbucket.org/epicglue/api/helpers/test"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"testing"
)

func TestPlans(t *testing.T) {
	client := http.Client{}

	req := test_helper.MakeGetRequest("/v1/payment/plans", nil)
	res, err := client.Do(req)

	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, http.StatusOK, res.StatusCode)

	resBody, _ := ioutil.ReadAll(res.Body)
	assert.NotEmpty(t, len(string(resBody)))

	// TODO: test if contains actual Plan objects
}

func TestPayWithFakeReceipt(t *testing.T) {

}

func TestPayWithOldReceipt(t *testing.T) {

}

func TestPay(t *testing.T) {

}

func TestPaymentHistory(t *testing.T) {

}
