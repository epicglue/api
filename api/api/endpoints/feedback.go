package endpoints

import (
	"bitbucket.org/epicglue/api/user/default_manager"
	"bitbucket.org/epicglue/api/user/request"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

func Feedback(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	feedbackRequest := request.NewFeedbackFromHttpRequest(r)

	if feedbackRequest == nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	manager := user_manager.NewDefaultUserManagerFromRequest(r)
	if err := manager.StoreFeedback(feedbackRequest.Feedback); err != nil {
		w.WriteHeader(http.StatusBadRequest)
	} else {
		w.WriteHeader(http.StatusOK)
	}
}
