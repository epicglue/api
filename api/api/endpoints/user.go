package endpoints

import (
	"bitbucket.org/epicglue/api/user/default_manager"
	"bitbucket.org/epicglue/api/user/request"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

type UserValidation struct {
	Errors []string `json:"errors"`
}

func Me(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	userManager := user_manager.NewDefaultUserManagerFromRequest(r)

	writeJSON(w, map[string]interface{}{"data": userManager.GetUser()})
}

func LoginByEmail(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	loginByEmailRequest, err := request.NewEmailRequestFromHttpRequest(r)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeJSON(w, map[string]string{"error": err.Error()})
		return
	}

	user, err := loginByEmailRequest.Login(user_manager.NewDefaultUserManagerFromRequest(r))

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeJSON(w, map[string]string{"error": err.Error()})
		return
	}

	w.WriteHeader(http.StatusOK)
	writeJSON(w, map[string]string{
		"token": user.Token,
	})
}

func RegisterByEmail(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	registerByEmailRequest, err := request.NewEmailRequestFromHttpRequest(r)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeJSON(w, map[string]string{"error": err.Error()})
		return
	}

	user, err := registerByEmailRequest.Register(user_manager.NewDefaultUserManagerFromRequest(r))

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeJSON(w, map[string]string{"error": err.Error()})
		return
	}

	w.WriteHeader(http.StatusOK)
	writeJSON(w, map[string]string{
		"token": user.Token,
	})
}

func RegisterByService(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	registerByServiceRequest, err := request.NewRegisterByServiceFromHttpRequest(r)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeJSON(w, map[string]string{"error": err.Error()})
		return
	}

	user, err := registerByServiceRequest.Register(user_manager.NewDefaultUserManagerFromRequest(r))

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeJSON(w, map[string]string{"error": err.Error()})
		return
	}

	w.WriteHeader(http.StatusOK)
	writeJSON(w, map[string]string{
		"token": user.Token,
	})
}

func RegisterByDevice(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	registerByDeviceRequest, err := request.NewDeviceRequestFromHttpRequest(r)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeJSON(w, map[string]string{"error": err.Error()})
		return
	}

	user, err := registerByDeviceRequest.Register(user_manager.NewDefaultUserManagerFromRequest(r))

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeJSON(w, map[string]string{"error": err.Error()})
		return
	}

	w.WriteHeader(http.StatusOK)
	writeJSON(w, map[string]string{
		"token": user.Token,
	})
}
