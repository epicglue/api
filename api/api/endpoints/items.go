package endpoints

import (
	"bitbucket.org/epicglue/api/item/data_source"
	"bitbucket.org/epicglue/api/item/default_manager"
	"github.com/Sirupsen/logrus"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"time"
)

func GetItems(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	start := time.Now()

	itemManager := item_manager.NewDefaultItemManagerFromRequest(r)
	list, err := itemManager.Search(data_source.NewDataSourceFromHttpRequest(r))

	if err != nil {
		return
	}

	log.WithFields(logrus.Fields{
		"took":     time.Since(start),
		"took_str": time.Since(start).String(),
		"action":   "items",
	}).Debug("Get Items")

	if len(list.Items) == 0 {
		writeJSON(w, map[string]interface{}{
			"data": []string{},
		})
	} else {
		writeJSON(w, map[string]interface{}{
			"data": list,
		})
	}
}

func ReadItems(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	start := time.Now()

	itemManager := item_manager.NewDefaultItemManagerFromRequest(r)
	itemManager.Read(data_source.NewDataSourceFromHttpRequest(r))
	w.WriteHeader(http.StatusOK)

	log.WithFields(logrus.Fields{
		"took":     time.Since(start),
		"took_str": time.Since(start).String(),
		"action":   "read",
	}).Debug("Read Items")
}

func UnreadItem(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	start := time.Now()

	itemManager := item_manager.NewDefaultItemManagerFromRequest(r)
	itemManager.Unread(data_source.NewDataSourceFromHttpRequest(r))
	w.WriteHeader(http.StatusOK)

	log.WithFields(logrus.Fields{
		"took":     time.Since(start),
		"took_str": time.Since(start).String(),
		"action":   "unread",
	}).Debug("Unread Items")
}

func GlueItem(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	start := time.Now()

	itemManager := item_manager.NewDefaultItemManagerFromRequest(r)
	itemManager.Glue(data_source.NewDataSourceFromHttpRequest(r))
	w.WriteHeader(http.StatusOK)

	log.WithFields(logrus.Fields{
		"took":     time.Since(start),
		"took_str": time.Since(start).String(),
		"action":   "glue",
	}).Debug("Glued Items")
}

func UnglueItem(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	start := time.Now()

	itemManager := item_manager.NewDefaultItemManagerFromRequest(r)
	itemManager.Unglue(data_source.NewDataSourceFromHttpRequest(r))
	w.WriteHeader(http.StatusOK)

	log.WithFields(logrus.Fields{
		"took":     time.Since(start),
		"took_str": time.Since(start).String(),
		"action":   "unglue",
	}).Debug("Unglue Items")
}

func DeleteItem(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	start := time.Now()

	itemManager := item_manager.NewDefaultItemManagerFromRequest(r)
	itemManager.Delete(data_source.NewDataSourceFromHttpRequest(r))
	w.WriteHeader(http.StatusOK)

	log.WithFields(logrus.Fields{
		"took":     time.Since(start),
		"took_str": time.Since(start).String(),
		"action":   "delete",
	}).Debug("Deleted Items")
}
