// +build integration

package endpoints_test

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/helpers/test"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

//func TestMe(t *testing.T) {
//	client := http.Client{}
//
//	req := test_helper.MakeGetRequestWithUserId("/v1/me", nil, USER_ID)
//	res, err := client.Do(req)
//
//	if err != nil {
//		t.Error(err)
//	}
//
//	assert.Equal(t, http.StatusOK, res.StatusCode)
//
//	response := test_helper.ReadResponse(res)
//
//	assert.Contains(t, response, "data")
//	fmt.Println(response)
//	//assert.Contains(t, response["data"], "username")
//	//assert.Contains(t, response["data"], "email")
//	//assert.Contains(t, response["data"], "plan")
//}

func TestLoginByWrongEmail(t *testing.T) {
	client := http.Client{}

	req := test_helper.MakePostRequest("/v1/login", map[string]string{
		"email":    helpers.RandomEmail(),
		"password": "dupadupa",
	})
	res, err := client.Do(req)

	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, http.StatusBadRequest, res.StatusCode)
}

func TestLoginByEmail(t *testing.T) {
	client := http.Client{}

	req := test_helper.MakePostRequest("/v1/login", map[string]string{
		"email":    "marek.mikuliszyn@gmail.com",
		"password": "dupadupa",
	})
	res, err := client.Do(req)

	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, http.StatusOK, res.StatusCode)

	response := test_helper.ReadResponse(res)

	assert.Contains(t, response, "token")
}

func TestRegisterByExistingEmail(t *testing.T) {
	client := http.Client{}

	req := test_helper.MakePostRequest("/v1/register/email", map[string]string{
		"email":    "marek.mikuliszyn@gmail.com",
		"password": "dupadupa",
	})
	res, err := client.Do(req)

	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, http.StatusBadRequest, res.StatusCode)
}

func TestRegisterByWrongEmail(t *testing.T) {
	client := http.Client{}

	req := test_helper.MakePostRequest("/v1/register/email", map[string]string{
		"email":    helpers.RandomString(8),
		"password": helpers.RandomString(8),
	})
	res, err := client.Do(req)

	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, http.StatusBadRequest, res.StatusCode)
}

func TestRegisterByEmail(t *testing.T) {
	client := http.Client{}

	req := test_helper.MakePostRequest("/v1/register/email", map[string]string{
		"email":    helpers.RandomEmail(),
		"password": helpers.RandomString(8),
	})
	res, err := client.Do(req)

	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, http.StatusOK, res.StatusCode)

	response := test_helper.ReadResponse(res)

	assert.Contains(t, response, "token")
}

func TestRegisterByService(t *testing.T) {

}
