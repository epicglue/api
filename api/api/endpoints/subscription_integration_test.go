// +build integration

package endpoints_test

import (
	"bitbucket.org/epicglue/api/helpers/test"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

const (
	USER_ID    = 3
	PROFILE_ID = 1
)

// Subscriptions
func TestSubscriptionsWithIncorrectInput(t *testing.T) {
	client := http.Client{}

	req := test_helper.MakeGetRequest("/v1/subscriptions", nil)
	res, err := client.Do(req)

	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, http.StatusUnauthorized, res.StatusCode)
}

//func TestSubscriptions(t *testing.T) {
//	client := http.Client{}
//
//	req := test_helper.MakeGetRequestWithUserId("/v1/subscriptions", nil, USER_ID)
//	res, err := client.Do(req)
//
//	if err != nil {
//		t.Error(err)
//	}
//
//	assert.Equal(t, http.StatusOK, res.StatusCode)
//
//	response := test_helper.ReadResponse(res)
//
//	assert.Contains(t, response, "data")
//}
//
//// Subscribe
//func subscribeWithPayload(t *testing.T, payload map[string]interface{}) {
//	client := http.Client{}
//	req := test_helper.MakePutRequestWithUserId("/v1/subscription", payload, USER_ID)
//	res, err := client.Do(req)
//
//	if err != nil {
//		t.Error(err)
//	}
//
//	assert.Equal(t, http.StatusOK, res.StatusCode)
//
//	response := test_helper.ReadResponse(res)
//	assert.Empty(t, response)
//}
//
//func TestSubscribePerUserWithValue(t *testing.T) {
//	numberOfSubs := howManySubscriptionsUserHas(USER_ID)
//
//	payload := map[string]interface{}{
//		"source_id":  findSourcePerUserWithValue(),
//		"profile_id": PROFILE_ID,
//		"value":      helpers.RandomString(8),
//	}
//
//	subscribeWithPayload(t, payload)
//
//	assert.Equal(t, numberOfSubs+1, howManySubscriptionsUserHas(USER_ID))
//
//	//addedSubscription := latestSubscriptionOfUser(USER_ID)
//	//assert.Equal(t, payload["source_id"].(int64), addedSubscription.GetSubscription().GetSource().Id)
//	//assert.Equal(t, int64(payload["profile_id"].(int)), addedSubscription.GetProfile().Id)
//	//assert.Equal(t, payload["value"].(string), addedSubscription.GetSubscription().Value.String)
//}
//
//func TestSubscribePerUserWithoutValue(t *testing.T) {
//	numberOfSubs := howManySubscriptionsUserHas(USER_ID)
//
//	payload := map[string]interface{}{
//		"source_id":  findSourcePerUserWithoutValue(),
//		"profile_id": PROFILE_ID,
//	}
//
//	subscribeWithPayload(t, payload)
//
//	assert.Equal(t, numberOfSubs, howManySubscriptionsUserHas(USER_ID))
//
//	//addedSubscription := latestSubscriptionOfUser(USER_ID)
//	//assert.Equal(t, payload["source_id"].(int64), addedSubscription.GetSubscription().GetSource().Id)
//	//assert.Equal(t, int64(payload["profile_id"].(int)), addedSubscription.GetProfile().Id)
//	//assert.True(t, addedSubscription.GetSubscription().Value.IsZero())
//}
//
//func TestSubscribeWithValue(t *testing.T) {
//	numberOfSubs := howManySubscriptionsUserHas(USER_ID)
//
//	payload := map[string]interface{}{
//		"source_id": findSourceWithValue(),
//		"value":     helpers.RandomString(8),
//	}
//
//	subscribeWithPayload(t, payload)
//
//	assert.Equal(t, numberOfSubs+1, howManySubscriptionsUserHas(USER_ID))
//
//	//addedSubscription := latestSubscriptionOfUser(USER_ID)
//	//assert.Equal(t, payload["source_id"].(int64), addedSubscription.GetSubscription().GetSource().Id)
//	//assert.True(t, addedSubscription.ProfileId.IsZero())
//	//assert.Equal(t, payload["value"].(string), addedSubscription.GetSubscription().Value.String)
//}

func TestSubscribeWithoutValue(t *testing.T) {
	//numberOfSubs := howManySubscriptionsUserHas(USER_ID)
	//
	//payload := map[string]interface{}{
	//	"source_id": findSourceWithoutValue(),
	//}
	//
	//subscribeWithPayload(t, payload)

	//assert.Equal(t, numberOfSubs, howManySubscriptionsUserHas(USER_ID))

	//addedSubscription := latestSubscriptionOfUser(USER_ID)
	//assert.Equal(t, payload["source_id"].(int64), addedSubscription.GetSubscription().GetSource().Id)
	//assert.True(t, addedSubscription.ProfileId.IsZero())
	//assert.True(t, addedSubscription.GetSubscription().Value.IsZero())
}

//func TestUnsubscribe(t *testing.T) {
//	client := http.Client{}
//
//	lastSubscription := latestSubscriptionOfUser(USER_ID)
//
//	req := test_helper.MakeDeleteRequestWithUserId("/v1/subscription", map[string]interface{}{
//		"id": lastSubscription.Id,
//	}, USER_ID)
//	res, err := client.Do(req)
//
//	if err != nil {
//		t.Error(err)
//	}
//
//	assert.Equal(t, http.StatusOK, res.StatusCode)
//
//	response := test_helper.ReadResponse(res)
//	assert.Empty(t, response)
//
//	assert.Nil(t, model.LoadUserSubscription(lastSubscription.Id))
//}

//func TestCounters(t *testing.T) {
//
//}
//
//func TestGlueCounters(t *testing.T) {
//
//}
