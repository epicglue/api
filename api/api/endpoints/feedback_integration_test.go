// +build integration

package endpoints_test

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestFeedbackUnauthorized(t *testing.T) {
	client := http.Client{}

	req := MakePostRequest("/v1/feedback", map[string]string{
		"feedback": "This is feedback",
	})
	res, err := client.Do(req)

	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, http.StatusUnauthorized, res.StatusCode)
}

//func TestFeedbackInvalidValue(t *testing.T) {
//	client := http.Client{}
//
//	req := MakePostRequestWithUserId("/v1/feedback", map[string]string{}, USER_ID)
//	res, err := client.Do(req)
//
//	if err != nil {
//		t.Error(err)
//	}
//
//	assert.Equal(t, http.StatusBadRequest, res.StatusCode)
//}
//
//func TestFeedbackEmptyValue(t *testing.T) {
//	client := http.Client{}
//
//	req := MakePostRequestWithUserId("/v1/feedback", map[string]string{
//		"feedback": "",
//	}, USER_ID)
//	res, err := client.Do(req)
//
//	if err != nil {
//		t.Error(err)
//	}
//
//	assert.Equal(t, http.StatusBadRequest, res.StatusCode)
//}
//
//func TestFeedback(t *testing.T) {
//	client := http.Client{}
//
//	req := MakePostRequestWithUserId("/v1/feedback", map[string]string{
//		"feedback": "This is feedback",
//	}, USER_ID)
//	res, err := client.Do(req)
//
//	if err != nil {
//		t.Error(err)
//		return
//	}
//
//	assert.Equal(t, http.StatusOK, res.StatusCode)
//}
