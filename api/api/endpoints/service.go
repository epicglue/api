package endpoints

import (
	"bitbucket.org/epicglue/api/user/default_manager"
	"bitbucket.org/epicglue/api/user/request"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

func ConnectService(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	connectServiceRequest, err := request.NewConnectServiceFromHttpRequest(r)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeJSON(w, map[string]string{"error": err.Error()})
		return
	}

	err = connectServiceRequest.Connect(user_manager.NewDefaultUserManagerFromRequest(r))

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeJSON(w, map[string]string{"error": err.Error()})
		return
	}

	w.WriteHeader(http.StatusOK)
}

func DisconnectService(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	disconnectServiceRequest, err := request.NewDisconnectServiceFromHttpRequest(r)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeJSON(w, map[string]string{"error": err.Error()})
		return
	}

	manager := user_manager.NewDefaultUserManagerFromRequest(r)
	err = manager.DeleteService(disconnectServiceRequest.Service, disconnectServiceRequest.Identifier)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeJSON(w, map[string]string{"error": err.Error()})
		return
	}

	w.WriteHeader(http.StatusOK)
}
