// +build integration

package endpoints_test

import (
	"bitbucket.org/epicglue/api/connection/database/postgres"
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/model"
	"fmt"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func getTestRequest() map[string]string {
	return map[string]string{
		"service":         "twitter",
		"service_user_id": fmt.Sprintf("%d", helpers.RandomNumber(16)),
		"token":           helpers.RandomString(32),
		"username":        "John Doe",
	}
}

func lastConnectedServiceForUser(userId int64) *model.UserSubscription {
	query := `
		SELECT
			id
		FROM
			user_subscription
		WHERE
			user_id = $1
		ORDER BY
		    created_at DESC
	`

	var lastSubId int64
	row := postgres.NewPostgres().DB().QueryRow(query, userId)
	row.Scan(&lastSubId)

	return model.LoadUserSubscription(lastSubId)
}

func TestConnectServiceUnauthorized(t *testing.T) {
	client := http.Client{}

	req := MakePutRequest("/v1/me/service", getTestRequest())
	res, err := client.Do(req)

	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, http.StatusUnauthorized, res.StatusCode)
}

//func TestConnectServiceInvalidRequest(t *testing.T) {
//	client := http.Client{}
//
//	req := MakePutRequestWithUserId("/v1/me/service", map[string]string{}, USER_ID)
//	res, err := client.Do(req)
//
//	if err != nil {
//		t.Error(err)
//	}
//
//	assert.Equal(t, http.StatusBadRequest, res.StatusCode)
//}
//
//func TestConnectServiceEmptyToken(t *testing.T) {
//	testRequest := getTestRequest()
//	testRequest["token"] = ""
//
//	client := http.Client{}
//
//	req := MakePutRequestWithUserId("/v1/me/service", testRequest, USER_ID)
//	res, err := client.Do(req)
//
//	if err != nil {
//		t.Error(err)
//	}
//
//	assert.Equal(t, http.StatusBadRequest, res.StatusCode)
//}
//
//func TestConnectServiceInvalidService(t *testing.T) {
//	testRequest := getTestRequest()
//	testRequest["token"] = ""
//
//	client := http.Client{}
//
//	req := MakePutRequestWithUserId("/v1/me/service", testRequest, USER_ID)
//	res, err := client.Do(req)
//
//	if err != nil {
//		t.Error(err)
//	}
//
//	assert.Equal(t, http.StatusBadRequest, res.StatusCode)
//}

func TestConnectServiceDuplicateToken(t *testing.T) {

}

func TestConnectService(t *testing.T) {

}

func TestDisconnectService(t *testing.T) {

}
