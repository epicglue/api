package endpoints

import (
	"bitbucket.org/epicglue/api/model"
	"bitbucket.org/epicglue/api/user/payment"
	"github.com/Sirupsen/logrus"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"time"
)

func Plans(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	start := time.Now()

	w.WriteHeader(http.StatusOK)
	writeJSON(w, model.LoadPlans())

	log.WithFields(logrus.Fields{
		"took":     time.Since(start),
		"took_str": time.Since(start).String(),
		"action":   "plans",
	}).Debug("Plans")
}

func Pay(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	startTime := time.Now()

	httpRequest := payment.NewPayRequest(r)

	if httpRequest == nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if err := payment.Pay(httpRequest); err != nil {
		w.WriteHeader(http.StatusBadRequest)
	} else {
		w.WriteHeader(http.StatusOK)
	}

	log.WithFields(logrus.Fields{
		"took":     time.Since(startTime),
		"took_str": time.Since(startTime).String(),
		"action":   "pay",
	}).Debug("Pay")
}

func PaymentHistory(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	startTime := time.Now()

	httpResponse := payment.PaymentHistory(payment.NewPaymentHistoryRequest(r))

	if httpResponse != nil {
		w.WriteHeader(http.StatusOK)
		writeJSON(w, httpResponse)
	} else {
		w.WriteHeader(http.StatusBadRequest)
	}

	log.WithFields(logrus.Fields{
		"took":     time.Since(startTime),
		"took_str": time.Since(startTime).String(),
		"action":   "payment history",
	}).Debug("Payment History")
}
