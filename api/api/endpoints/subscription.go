package endpoints

import (
	"bitbucket.org/epicglue/api/subscription/request"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

// Subscriptions list of all user's subscriptions
func Subscriptions(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	listSubsRequest, err := request.NewListSubscriptionsFromHttpRequest(r)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeJSON(w, map[string]string{"error": err.Error()})
		return
	}

	subs, err := listSubsRequest.UserSubscriptions()

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeJSON(w, map[string]string{"error": err.Error()})
		return
	}

	writeJSON(w, subs)
}

// Subscribe to given service_id, filters and values payload
func Subscribe(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	subRequest, err := request.NewSubscribeRequest(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeJSON(w, map[string]string{"error": err.Error()})
		return
	}

	if !subRequest.Subscribe() {
		w.WriteHeader(http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
}

// Unsubscribe by sub_id
func Unsubscribe(w http.ResponseWriter, r *http.Request, c httprouter.Params) {
	unSubRequest, err := request.NewUnsubscribeRequest(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeJSON(w, map[string]string{"error": err.Error()})
		return
	}

	if !unSubRequest.Unsubscribe() {
		w.WriteHeader(http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
}

func Counters(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	countRequest, err := request.NewCountRequest(r)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeJSON(w, map[string]string{"error": err.Error()})
		return
	}

	writeJSON(w, countRequest.Count())
}
