package main

import (
	"bitbucket.org/epicglue/api/api/api"
	"bitbucket.org/epicglue/api/connection"
)

func main() {
	connection.SystemCheck()

	api.Run()
}
