package connection

import (
	"bitbucket.org/epicglue/api/config"
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"regexp"
	"runtime/debug"
)

type DBConnection struct {
	db *gorm.DB
}

func newDBConnection() *DBConnection {
	log.Debug("NewDB")

	conf := config.LoadConfig()

	db, err := gorm.Open("postgres", fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s port=%d", conf.Db.Host, conf.Db.User, conf.Db.Name, conf.Db.Pass, conf.Db.Port))
	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return nil
	}

	return &DBConnection{db}
}

func (c *DBConnection) Get() *gorm.DB {
	return c.db
}

func (c *DBConnection) Check() bool {
	versionExpression, _ := regexp.Compile("PostgreSQL 9.5.([0-9]+)")

	var version string
	result := c.db.DB().QueryRow("select version()")
	result.Scan(&version)

	if !versionExpression.MatchString(version) {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Errorf("Invalid PostgreSQL version (%s)", version)

		return false
	}

	return c.db.DB().Ping() == nil
}
