package connection

import (
	"bitbucket.org/epicglue/api/config"
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/garyburd/redigo/redis"
	"runtime/debug"
	"time"
)

var openConnections int64 = 1

type CacheConnection struct {
	conf *config.Config
	pool *redis.Pool
}

func newRedisConnection() *CacheConnection {
	conn := &CacheConnection{}
	conn.conf = config.LoadConfig()
	conn.pool = conn.newPool()

	return conn
}

func (c *CacheConnection) Get() redis.Conn {
	openConnections++
	//log.Debugf("Redis connection acquired (%d)", openConnections)

	//if config.LoadConfig().App.IsTest {
	//	var logger *logging.Logger
	//	logger = logging.New(os.Stdout, "LOG: ", logging.Ldate|logging.Ltime|logging.Lshortfile)
	//	return redis.NewLoggingConn(c.pool.Get(), logger, "REDIS")
	//}

	return c.pool.Get()
}

func (c *CacheConnection) Release(conn redis.Conn) {
	openConnections--
	//log.Debugf("Redis connection released (%d)", openConnections)

	if err := conn.Close(); err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)
	}
}

func (c *CacheConnection) Check() bool {
	if conn := c.pool.Get(); conn == nil {
		return false
	} else {
		defer c.Release(conn)
		return true
	}
}

func (c *CacheConnection) newPool() *redis.Pool {
	return &redis.Pool{
		MaxActive:   c.conf.Redis.MaxConnections,
		MaxIdle:     c.conf.Redis.MaxConnections,
		IdleTimeout: time.Duration(c.conf.Redis.IdleTimeout) * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", fmt.Sprintf("%s:%d", c.conf.Redis.Host, c.conf.Redis.Port))
			if err != nil {
				log.WithFields(logrus.Fields{
					"stack": string(debug.Stack()),
				}).Error(err)

				return nil, err
			}

			return c, err
		},
		TestOnBorrow: func(r redis.Conn, t time.Time) error {
			_, err := r.Do("PING")
			return err
		},
	}
}
