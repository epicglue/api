package index

import (
	"bitbucket.org/epicglue/api/model"
	"github.com/mattbaird/elastigo/lib"
)

type Index interface {
	//	Search(indexName string, query string, params ...int) (IndexStoreResults, error)
	Search(indexName string, query string, params ...int) (*elastigo.Hits, error)

	Insert(indexName string, id string, json string) error
	Update(indexName string, id string, update string) error
	Delete(indexName string, id string) error
	UpdateWithTTL(indexName string, id string, ttl string, update string) error

	Count(indexName string, query string, documentLimit int64) int64

	CreateIndex(indexName string) error

	RemoveField(indexName string, itemId string, fieldName string) error

	AddIntToList(indexName string, itemId string, listName string, value int64) error

	ScanAll(indexName string, query string, fn func(*model.Items)) error

	GetItem(indexName string, itemId string, item interface{}) error
}

type IndexResults interface {
}
