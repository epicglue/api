package elasticsearch

import (
	"bitbucket.org/epicglue/api/model"
	"fmt"
	"github.com/Sirupsen/logrus"
	"runtime/debug"
	"time"
)

func (inx Elasticsearch) ScanAll(index string, query string, fn func(*model.Items)) error {
	var search_query string

	if len(query) > 2 {
		search_query = fmt.Sprintf(`{"size":%d,%s}`, 1000, query[1:])
	} else {
		search_query = fmt.Sprintf(`{"size":%d}`, 1000)
	}

	log.Debug(search_query)

	scroll, err := inx.Connection.Search(index, DEFAULT_TYPE, map[string]interface{}{
		"scroll":      "1m",
		"search_type": "scan",
	}, search_query)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return err
	}

	token := scroll.ScrollId

	total := 0

	for {
		startTime := time.Now()

		out, err := inx.Connection.Scroll(map[string]interface{}{
			"scroll": "1m",
		}, token)

		log.Debugf("Received batch [%s]", time.Since(startTime).String())

		if err != nil {
			log.WithFields(logrus.Fields{
				"stack": string(debug.Stack()),
			}).Error(err)

			return err
		}

		if out.Hits.Len() == 0 {
			inx.DeleteScroll(token)

			return nil
		}

		items, err := model.ItemsFromHits(&out.Hits)
		if err != nil {
			log.WithFields(logrus.Fields{
				"stack": string(debug.Stack()),
			}).Error(err)

			return err
		}

		fn(items)

		log.Debugf("Finished processing batch of %d items", out.Hits.Len())

		token = out.ScrollId
		total += out.Hits.Len()
	}

	return nil
}

func (inx Elasticsearch) DeleteScroll(scrollId string) error {
	var url string
	url = "/_search/scroll"

	_, err := inx.Connection.DoCommand("DELETE", url, nil, scrollId)
	if err != nil {
		//log.WithFields(logrus.Fields{
		//	"stack":     string(debug.Stack()),
		//	"scroll_id": scrollId,
		//}).Error(err)

		return err
	}

	return nil
}
