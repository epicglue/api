package elasticsearch

import (
	"bitbucket.org/epicglue/api/connection"
	"github.com/mattbaird/elastigo/lib"
)

type Elasticsearch struct {
	Connection *elastigo.Conn
}

func NewElasticsearch() *Elasticsearch {
	return &Elasticsearch{
		Connection: connection.NewConnector().GetES(),
	}
}
