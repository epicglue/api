package elasticsearch

import (
	"fmt"
)

func (inx Elasticsearch) RemoveField(indexName string, itemId string, fieldName string) error {
	return inx.Update(
		indexName,
		itemId,
		fmt.Sprintf(`{
			"script_file": "remove_field",
			"lang": "groovy",
			"params": {
				"field": "%s"
			}
		}`, fieldName),
	)
}

func (inx Elasticsearch) AddIntToList(indexName string, itemId string, listName string, value int64) error {
	return inx.Update(indexName, itemId, fmt.Sprintf(`{
		"script_file": "add_to_list",
		"lang": "groovy",
		"params": {
			"list": "%s",
			"value": %d
		}
	}`, listName, value))
}
