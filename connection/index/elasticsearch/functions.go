package elasticsearch

import (
	"bitbucket.org/epicglue/api/helpers"
	"errors"
	"github.com/Sirupsen/logrus"
	"github.com/mattbaird/elastigo/lib"
	"runtime/debug"
)

var log = helpers.GetLogger("connector")

const (
	DEFAULT_TYPE = "item"
	BATCH_SIZE   = 50
)

func (inx Elasticsearch) Count(indexName string, query string, documentLimit int64) int64 {
	var (
		out elastigo.CountResponse
		err error
	)

	if documentLimit > 0 {
		out, err = inx.Connection.Count(indexName, DEFAULT_TYPE, map[string]interface{}{
			"terminate_after": int(documentLimit),
		}, query)
	} else {
		out, err = inx.Connection.Count(indexName, DEFAULT_TYPE, nil, query)
	}

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return 0
	}

	return int64(out.Count)
}

func (inx Elasticsearch) Insert(indexName string, id string, insert string) error {
	if id == "" {
		err := errors.New("ID cannot be empty")

		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
			"index": indexName,
			"id":    id,
			"json":  insert,
		}).Error(err)

		return err
	}

	_, err := inx.Connection.Index(indexName, DEFAULT_TYPE, id, nil, insert)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
			"index": indexName,
			"id":    id,
			"json":  insert,
		}).Error(err)

		return err
	}

	log.WithFields(logrus.Fields{
		"method": "Update",
		"index":  indexName,
		"id":     id,
		"json":   insert,
	}).Debug("Inserted Item")

	return nil
}

func (inx Elasticsearch) Update(indexName string, id string, update string) error {
	if id == "" {
		err := errors.New("ID cannot be empty")

		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
			"index": indexName,
			"id":    id,
			"json":  update,
		}).Panic(err)

		return err
	}

	_, err := inx.Connection.Update(indexName, DEFAULT_TYPE, id, nil, update)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
			"index": indexName,
			"id":    id,
			"json":  update,
		}).Error(err)

		return err
	}

	log.WithFields(logrus.Fields{
		"index": indexName,
		"id":    id,
		"json":  update,
	}).Debug("Updated Item")

	return nil
}

func (inx Elasticsearch) UpdateWithTTL(indexName string, id string, ttl string, update string) error {
	if id == "" {
		err := errors.New("ID cannot be empty")

		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
			"index": indexName,
			"id":    id,
			"json":  update,
		}).Error(err)

		return err
	}

	_, err := inx.Connection.Update(indexName, DEFAULT_TYPE, id, map[string]interface{}{
		"ttl": ttl,
	}, update)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
			"index": indexName,
			"id":    id,
			"json":  update,
		}).Error(err)

		return err
	}

	log.WithFields(logrus.Fields{
		"method": "Update",
		"index":  indexName,
		"id":     id,
		"ttl":    ttl,
		"json":   update,
	}).Debug("Updated Item With TTL")

	return nil
}

func (inx Elasticsearch) Delete(indexName string, id string) error {
	_, err := inx.Connection.Delete(indexName, DEFAULT_TYPE, id, nil)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
			"index": indexName,
			"id":    id,
		}).Error(err)

		return err
	}

	log.WithFields(logrus.Fields{
		"method": "Delete",
		"index":  indexName,
		"id":     id,
	}).Debug("Deleted Item")

	return nil
}

func (inx Elasticsearch) GetItem(indexName string, itemId string, item interface{}) error {
	return inx.Connection.GetSource(indexName, DEFAULT_TYPE, itemId, nil, item)
}
