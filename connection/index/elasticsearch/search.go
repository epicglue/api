package elasticsearch

import (
	"bitbucket.org/epicglue/api/config"
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/mattbaird/elastigo/lib"
	"runtime/debug"
	"time"
)

func (inx Elasticsearch) Search(indexName string, query string, params ...int) (*elastigo.Hits, error) {
	start := 0
	size := BATCH_SIZE

	if len(params) > 0 {
		start = params[0]
	}
	if len(params) > 1 {
		size = params[1]
	}

	res, err := inx.searchES(indexName, query, start, size)

	if err != nil {
		return nil, err
	}

	return res, err
}

func (inx Elasticsearch) searchES(indexName string, query string, start int, size int) (*elastigo.Hits, error) {
	startTime := time.Now()

	params := map[string]interface{}{
		"timeout": config.LoadConfig().Elasticsearch.SearchTimeout,
	}

	out, err := inx.Connection.Search(indexName, DEFAULT_TYPE, params, inx.formatSearchQuery(query, start, size))

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return nil, err
	}

	log.WithFields(logrus.Fields{
		"took":     time.Since(startTime),
		"took_str": time.Since(startTime).String(),
		"index":    indexName,
		"query":    inx.formatSearchQuery(query, start, size),
	}).Debug("")

	if out.Hits.Len() == 0 {
		return &out.Hits, nil
	}

	return &out.Hits, nil
}

func (inx Elasticsearch) formatSearchQuery(query string, start int, size int) string {
	if len(query) > 2 {
		return fmt.Sprintf(`{"from":%d,"size":%d,"sort":{"content_order_by":{"order":"desc"}},%s`, start, size, query[1:])
	}

	return fmt.Sprintf(`{"from":%d,"size":%d,"sort":{"content_order_by":{"order":"desc"}}}`, start, size)
}
