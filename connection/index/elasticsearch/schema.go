package elasticsearch

import (
	"fmt"
)

const defaultSchema = `{
	"settings" : {
		"number_of_shards" : 5,
		"number_of_replicas" : 0
	},
	"mappings" : {
		"item" : {
			"_source" : {
				"enabled" : true
			},
			"_ttl" : {
				"enabled": true
			},
			"_all": {
				"enabled": false
			},
			"_timestamp": {
				"enabled": false
			},
			"properties": {
				"image_large": {
					"type": "string",
					"index": "no"
				},
				"subs": {
					"type": "integer"
				},
				"image": {
					"type": "string",
					"index": "no"
				},
				"video": {
					"type": "string",
					"index": "no"
				},
				"url_in": {
					"type": "string"
				},
				"content_created_at": {
					"type": "date",
					"format": "strict_date_optional_time||epoch_millis"
				},
				"id": {
					"type": "string",
					"index": "no"
				},
				"image_small_width": {
					"type": "short",
					"index": "no"
				},
				"image_large_height": {
					"type": "short",
					"index": "no"
				},
				"location_name": {
					"type": "string"
				},
				"service": {
					"type": "string",
					"index": "not_analyzed"
				},
				"author": {
					"type": "string",
					"index": "not_analyzed"
				},
				"image_width": {
					"type": "short",
					"index": "no"
				},
				"comments": {
					"type": "integer"
				},
				"video_height": {
					"type": "short",
					"index": "no"
				},
				"points": {
					"type": "integer"
				},
				"media_type": {
					"type": "string",
					"index": "not_analyzed"
				},
				"content_order_by": {
					"type": "date",
					"format": "strict_date_optional_time||epoch_millis"
				},
				"description": {
					"type": "string",
					"analyzer": "english"
				},
				"tags": {
					"type": "string",
					"index": "not_analyzed"
				},
				"read": {
					"type": "boolean"
				},
				"glued": {
					"type": "boolean"
				},
				"video_width": {
					"type": "short",
					"index": "no"
				},
				"image_large_width": {
					"type": "short",
					"index": "no"
				},
				"author_image_large": {
					"type": "string",
					"index": "no"
				},
				"url": {
					"type": "string",
					"index": "not_analyzed"
				},
				"title": {
					"type": "string",
					"analyzer": "english"
				},
				"image_small": {
					"type": "string",
					"index": "no"
				},
				"item_type": {
					"type": "string",
					"index": "not_analyzed"
				},
				"image_height": {
					"type": "short",
					"index": "no"
				},
				"image_small_height": {
					"type": "short",
					"index": "no"
				},
				"author_image": {
					"type": "string",
					"index": "no"
				},
				"url_ext": {
					"type": "string",
					"index": "not_analyzed"
				},
				"location": {
					"type": "geo_point",
					"fielddata" : {
						"format" : "compressed",
						"precision" : "1cm"
					}
				},
				"created_at": {
					"type": "date",
					"format": "date_time||date_time_no_millis",
					"index": "no"
				},
				"updated_at": {
					"type": "date",
					"format": "date_time||date_time_no_millis",
					"index": "no"
				}
			}
		}
	}
}
`

func (inx Elasticsearch) CreateIndex(indexName string) error {
	_, err := inx.Connection.DoCommand("PUT", fmt.Sprintf("/%s", indexName), nil, []byte(defaultSchema))

	return err
}
