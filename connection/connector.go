package connection

import (
	"bitbucket.org/epicglue/api/helpers"
	"github.com/jinzhu/gorm"
	elastigo "github.com/mattbaird/elastigo/lib"
	"sync"
)

var (
	log  = helpers.GetLogger("connector")
	c    *Connector
	once sync.Once
)

type Connector struct {
	db    *DBConnection
	es    *ESConnection
	cache *CacheConnection
}

func NewConnector() *Connector {
	once.Do(func() {
		c = &Connector{}
	})

	return c
}

func (c *Connector) GetDB() *gorm.DB {
	if c.db == nil {
		c.db = newDBConnection()
	}
	return c.db.Get()
}

func (c *Connector) GetES() *elastigo.Conn {
	if c.es == nil {
		c.es = newESConnection()
	}
	return c.es.Get()
}

func (c *Connector) GetCache() *CacheConnection {
	if c.cache == nil {
		c.cache = newRedisConnection()
	}

	return c.cache
}

func (c *Connector) CheckDB() bool {
	if c.db == nil {
		c.GetDB()
	}
	return c.db.Check()
}

func (c *Connector) CheckES() bool {
	if c.es == nil {
		c.GetES()
	}
	return c.es.Check()
}

func (c *Connector) CheckCache() bool {
	if c.cache == nil {
		c.GetCache()
	}
	return c.cache.Check()
}

func SystemCheck() {
	c := NewConnector()

	if !c.CheckCache() {
		panic("No Redis connectivity")
	}

	if !c.CheckDB() {
		panic("No DB connectivity")
	}

	if !c.CheckES() {
		panic("No Elasticsearch connectivity")
	}

	log.Info("All systems clear!")
}
