package key_value_store

type KeyValueStore interface {
	KeyExists(string) bool
	RemoveKeys(...string) error

	// Counters
	GetCount(string) (int64, error)
	SetCount(string, int64, int64) (int64, error)
	Increment(string, int64) (int64, error)
	Decrement(string, int64) (int64, error)

	FindKeysByPattern(string, int) ([]string, int)
	RemoveAllSubKeys(string, ...int) error

	// Sorted Set
	FindInSortedSet(key string, startValue int) ([]string, int)
	RemoveFromSortedSet(key string, items ...string) error

	// Set
	FindInSet(key string, startValue int) ([]string, int)
	AddToSet(key string, items ...string) error
	MoveToSet(target string, items ...string) error
	RemoveFromSet(key string, items ...string) error
	MoveAllToSet(sourceKey string, targetKey string, removeFromKeys []string, params ...int)

	// Slices
	LeftSlice(key string, start int, length int) []string
	LeftSlicePosition(key string, item string) int
}
