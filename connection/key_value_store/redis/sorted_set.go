package redis

import (
	"github.com/Sirupsen/logrus"
	"github.com/garyburd/redigo/redis"
	"runtime/debug"
	"time"
)

func (r Redis) FindInSortedSet(key string, startValue int) ([]string, int) {
	var (
		res []interface{}
		err error
	)

	startTime := time.Now()

	rConn := r.get()
	defer r.release(rConn)

	res, err = redis.Values(rConn.Do("ZSCAN", key, startValue))
	log.Infof("ZSCAN [%s]", time.Since(startTime))

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":   string(debug.Stack()),
			"command": "ZSCAN",
		}).Error(err)

		return []string{}, 0
	}

	var (
		limit    uint
		items    []string
		hashList []string
	)

	startTime = time.Now()
	redis.Scan(res, &limit, &items)

	for i := 0; i < len(items); i += 2 {
		hashList = append(hashList, items[i])
	}
	log.Debugf("ZSCAN [%s]", time.Since(startTime))

	return hashList, int(limit)
}

func (r Redis) RemoveFromSortedSet(key string, items ...string) error {
	return r.removeFromX("ZREM", key, items...)
}
