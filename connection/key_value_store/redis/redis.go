package redis

import (
	"bitbucket.org/epicglue/api/connection"
	"bitbucket.org/epicglue/api/connection/key_value_store"
	"bitbucket.org/epicglue/api/helpers"
	"github.com/garyburd/redigo/redis"
)

var log = helpers.GetLogger("redis")

type Redis struct {
	cache *connection.CacheConnection
}

func NewRedis() key_value_store.KeyValueStore {
	return &Redis{
		cache: connection.NewConnector().GetCache(),
	}
}

func (r Redis) get() redis.Conn {
	return r.cache.Get()
}

func (r Redis) release(connection redis.Conn) {
	r.cache.Release(connection)
}
