package redis

import (
	"github.com/Sirupsen/logrus"
	"github.com/garyburd/redigo/redis"
	"runtime/debug"
	"time"
)

func (r Redis) LeftSlice(key string, start int, length int) []string {
	startTime := time.Now()

	rConn := r.get()
	defer r.release(rConn)

	end := start + length
	res, err := redis.Strings(rConn.Do("ZREVRANGE", key, start, end))

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":    string(debug.Stack()),
			"command":  "ZREVRANGE",
			"start":    start,
			"end":      end,
			"took":     time.Since(startTime),
			"took_str": time.Since(startTime).String(),
		}).Error(err)

		return []string{}
	}

	return res
}

func (r Redis) LeftSlicePosition(key string, item string) int {
	startTime := time.Now()

	rConn := r.get()
	defer r.release(rConn)

	res, err := redis.Uint64(rConn.Do("ZREVRANK", key, item))

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":    string(debug.Stack()),
			"command":  "ZREVRANK",
			"key":      key,
			"item":     item,
			"took":     time.Since(startTime),
			"took_str": time.Since(startTime).String(),
		}).Error(err)

		return 0
	}

	return int(res)
}
