package redis

import (
	"bitbucket.org/epicglue/api/helpers"
	"github.com/Sirupsen/logrus"
	"github.com/garyburd/redigo/redis"
	"runtime/debug"
	"time"
)

func (r Redis) FindInSet(key string, startValue int) ([]string, int) {
	var (
		res []interface{}
		err error
	)

	startTime := time.Now()

	rConn := r.get()
	defer r.release(rConn)

	res, err = redis.Values(rConn.Do("SSCAN", key, startValue, "COUNT", 1000))

	if err != nil {
		log.WithFields(logrus.Fields{
			"command":  "SSCAN",
			"stack":    string(debug.Stack()),
			"took":     time.Since(startTime),
			"took_str": time.Since(startTime).String(),
		}).Error(err)

		return []string{}, 0
	}

	var (
		limit    uint
		items    []string
		hashList []string
	)

	startTime = time.Now()
	redis.Scan(res, &limit, &items)

	for i := 0; i < len(items); i++ {
		hashList = append(hashList, items[i])
	}

	log.WithFields(logrus.Fields{
		"command":  "SSCAN",
		"took":     time.Since(startTime),
		"took_str": time.Since(startTime).String(),
	}).Debug("SSCAN")

	return hashList, int(limit)
}

func (r Redis) AddToSet(key string, items ...string) error {
	var list []string = []string{key}

	combinedList := append(list, items...)

	startTime := time.Now()

	rConn := r.get()
	defer r.release(rConn)

	_, err := rConn.Do("SADD", helpers.ConvertStringListToInterfaceList(combinedList)...)

	if err != nil {
		log.WithFields(logrus.Fields{
			"command":  "SADD",
			"stack":    string(debug.Stack()),
			"took":     time.Since(startTime),
			"took_str": time.Since(startTime).String(),
		}).Error(err)

		return err
	}

	log.WithFields(logrus.Fields{
		"command":  "SADD",
		"took":     time.Since(startTime),
		"took_str": time.Since(startTime).String(),
	}).Debug("SADD")

	return nil
}

func (r Redis) MoveToSet(target string, items ...string) error {
	var list []string = []string{target}

	combinedList := append(list, items...)

	startTime := time.Now()

	rConn := r.get()
	defer r.release(rConn)

	_, err := rConn.Do("SADD", helpers.ConvertStringListToInterfaceList(combinedList)...)

	if err != nil {
		log.WithFields(logrus.Fields{
			"command":  "SADD",
			"stack":    string(debug.Stack()),
			"took":     time.Since(startTime),
			"took_str": time.Since(startTime).String(),
		}).Error(err)

		return err
	}

	log.WithFields(logrus.Fields{
		"command":  "SADD",
		"took":     time.Since(startTime),
		"took_str": time.Since(startTime).String(),
	}).Debug("MoveToSet")

	return nil
}

func (r Redis) RemoveFromSet(key string, items ...string) error {
	return r.removeFromX("SREM", key, items...)
}

func (r Redis) MoveAllToSet(sourceKey string, targetKey string, removeFromKeys []string, params ...int) {
	limit := 0
	if len(params) > 0 {
		limit = params[0]
	}

	hashList, limit := r.FindInSortedSet(sourceKey, limit)

	if len(hashList) == 0 {
		return
	}

	r.MoveToSet(targetKey, hashList...)

	for _, removeKey := range removeFromKeys {
		r.RemoveFromSortedSet(removeKey, hashList...)
	}

	if limit > 0 {
		r.MoveAllToSet(sourceKey, targetKey, removeFromKeys, limit)
	}
}
