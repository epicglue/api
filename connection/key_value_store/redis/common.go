package redis

import (
	"bitbucket.org/epicglue/api/helpers"
	"github.com/Sirupsen/logrus"
	"runtime/debug"
	"strings"
	"time"
)

func (r Redis) removeFromX(command string, key string, items ...string) error {
	var list []string = []string{key}

	combinedList := append(list, items...)

	startTime := time.Now()

	rConn := r.get()
	defer r.release(rConn)

	_, err := rConn.Do(command, helpers.ConvertStringListToInterfaceList(combinedList)...)
	log.Debugf("%s [%s]", command, time.Since(startTime))

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":    string(debug.Stack()),
			"command":  command,
			"key":      key,
			"params":   strings.Join(items, " "),
			"took":     time.Since(startTime),
			"took_str": time.Since(startTime).String(),
		}).Error(err)

		return err
	}

	return nil
}
