package redis

import (
	"github.com/Sirupsen/logrus"
	"github.com/garyburd/redigo/redis"
	"runtime/debug"
	"time"
)

func (r Redis) GetCount(key string) (int64, error) {
	startTime := time.Now()

	rConn := r.get()
	defer r.release(rConn)

	n, err := redis.Int64(rConn.Do("GET", key))

	if err == redis.ErrNil {
		return 0, err
	}

	if err == redis.ErrPoolExhausted {
		panic(err)
	}

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":    string(debug.Stack()),
			"command":  "GET",
			"key":      key,
			"took":     time.Since(startTime),
			"took_str": time.Since(startTime).String(),
		}).Error(err)

		return 0, err
	}

	return n, nil
}

func (r Redis) SetCount(key string, value int64, expiry int64) (int64, error) {
	startTime := time.Now()

	rConn := r.get()
	defer r.release(rConn)

	_, err := redis.String(rConn.Do("SET", key, value, "EX", expiry))

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":    string(debug.Stack()),
			"command":  "SET",
			"key":      key,
			"expiry":   expiry,
			"took":     time.Since(startTime),
			"took_str": time.Since(startTime).String(),
		}).Error(err)

		return 0, err
	}

	return value, nil
}

func (r Redis) Increment(key string, value int64) (int64, error) {
	startTime := time.Now()

	rConn := r.get()
	defer r.release(rConn)

	n, err := redis.Int64(rConn.Do("INCRBY", key, value))

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":    string(debug.Stack()),
			"command":  "INCR",
			"key":      key,
			"took":     time.Since(startTime),
			"took_str": time.Since(startTime).String(),
		}).Error(err)

		return 0, err
	}

	return n, nil
}

func (r Redis) Decrement(key string, value int64) (int64, error) {
	startTime := time.Now()

	rConn := r.get()
	defer r.release(rConn)

	n, err := redis.Int64(rConn.Do("DECRBY", key, value))

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":    string(debug.Stack()),
			"command":  "DECR",
			"key":      key,
			"took":     time.Since(startTime),
			"took_str": time.Since(startTime).String(),
		}).Error(err)

		return 0, err
	}

	return n, nil
}
