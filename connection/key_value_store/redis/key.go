package redis

import (
	"bitbucket.org/epicglue/api/helpers"
	"github.com/Sirupsen/logrus"
	"github.com/garyburd/redigo/redis"
	"runtime/debug"
	"time"
)

func (r Redis) KeyExists(key string) bool {
	startTime := time.Now()

	rConn := r.get()
	defer r.release(rConn)

	v, err := redis.Bool(rConn.Do("GET", key))
	log.Infof("GET [%s]", time.Since(startTime))

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":    string(debug.Stack()),
			"command":  "GET",
			"key":      key,
			"took":     time.Since(startTime),
			"took_str": time.Since(startTime).String(),
		}).Error(err)

		return false
	}

	return v
}

func (r Redis) RemoveKeys(keys ...string) error {
	rConn := r.get()
	defer r.release(rConn)

	_, err := rConn.Do("DEL", helpers.ConvertStringListToInterfaceList(keys)...)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":   string(debug.Stack()),
			"command": "REM",
		}).Error(err)

		return err
	}

	return nil
}

func (r Redis) FindKeysByPattern(pattern string, startValue int) ([]string, int) {
	var (
		res []interface{}
		err error
	)

	rConn := r.get()
	defer r.release(rConn)

	res, err = redis.Values(rConn.Do("SCAN", startValue, "MATCH", pattern))

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":   string(debug.Stack()),
			"command": "SCAN",
		}).Error(err)

		return []string{}, 0
	}

	var (
		limit   uint
		items   []string
		keyList []string
	)

	redis.Scan(res, &limit, &items)

	for i := 0; i < len(items); i++ {
		keyList = append(keyList, items[i])
	}

	return keyList, int(limit)
}

func (r Redis) RemoveAllSubKeys(pattern string, params ...int) error {
	limit := 0
	if len(params) > 0 {
		limit = params[0]
	}

	keys, newLimit := r.FindKeysByPattern(pattern, limit)

	if len(keys) == 0 {
		return nil
	}

	err := r.RemoveKeys(keys...)

	if err != nil && limit > 0 {
		r.RemoveAllSubKeys(pattern, newLimit)
	}

	return nil
}
