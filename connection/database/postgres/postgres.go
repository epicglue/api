package postgres

import (
	"bitbucket.org/epicglue/api/connection"
	"github.com/jinzhu/gorm"
)

func NewPostgres() *gorm.DB {
	return connection.NewConnector().GetDB()
}
