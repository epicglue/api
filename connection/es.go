package connection

import (
	"bitbucket.org/epicglue/api/config"
	"fmt"
	elastigo "github.com/mattbaird/elastigo/lib"
	"sync"
	"time"
)

var (
	onceES sync.Once
	conn   *elastigo.Conn
)

type ESConnection struct {
	conf *config.Config
}

func newESConnection() *ESConnection {
	es := ESConnection{}
	es.conf = config.LoadConfig()

	return &es
}

func (c *ESConnection) Get() *elastigo.Conn {
	onceES.Do(func() {
		conn = &elastigo.Conn{
			Protocol:       c.conf.Elasticsearch.Protocol,
			Domain:         c.conf.Elasticsearch.Hosts[0],
			ClusterDomains: c.conf.Elasticsearch.Hosts,
			Port:           fmt.Sprintf("%d", c.conf.Elasticsearch.Port),
			DecayDuration:  time.Duration(time.Duration(c.conf.Elasticsearch.DecayDuration) * time.Second),
		}
	})

	return conn
}

func (c *ESConnection) Check() bool {
	_, err := c.Get().Health()
	return err == nil
}
