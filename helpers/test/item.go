package test_helper

import (
	"bitbucket.org/epicglue/api/connection/database/postgres"
	"bitbucket.org/epicglue/api/enum/item_type"
	"bitbucket.org/epicglue/api/enum/media_type"
	"bitbucket.org/epicglue/api/enum/service"
	"bitbucket.org/epicglue/api/enum/visibility"
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/model"
	"github.com/yezooz/null"
	"time"
)

func TestItem() *model.Item {
	item := &model.Item{
		Id:               helpers.RandomString(64),
		ContentId:        helpers.RandomString(16),
		ItemType:         item_type.HackerNewsPost,
		MediaType:        media_type.Link,
		ServiceName:      service.HackerNews,
		Title:            null.StringFrom(helpers.RandomString(16)),
		URL:              "http://www.epicglue.com",
		Visibility:       visibility.Public,
		Points:           null.IntFrom(helpers.RandomNumber(3)),
		Comments:         null.IntFrom(helpers.RandomNumber(2)),
		IsGlued:          false,
		IsRead:           false,
		CreatedAt:        time.Now(),
		ContentCreatedAt: time.Unix(time.Now().Unix(), 0),
		ContentOrderBy:   time.Unix(time.Now().Unix(), 0),
	}

	if _, err := postgres.NewPostgres().DB().Exec(`INSERT INTO "item" (hash, content_id, item_type, media_type, service, title, author, url, visibility, points, comments, created_at, content_created_at, content_order_by_date) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)`, item.Id, item.ContentId, item.ItemType, item.MediaType, item.ServiceName, item.Title, item.Author, item.URL, item.Visibility, item.Points.Int64, item.Comments.Int64, item.CreatedAt, item.ContentCreatedAt, item.ContentOrderBy); err != nil {
		panic(err)
	}

	return item
}

func TestUserItem(user *model.User, item *model.Item) *model.UserItem {
	userItem := &model.UserItem{
		IsRead:    false,
		IsGlued:   false,
		User:      user,
		CreatedAt: null.TimeFrom(time.Now()),
	}

	if _, err := postgres.NewPostgres().DB().Exec(`INSERT INTO "user_item" (item_id, user_id, is_read, is_glued, created_at) VALUES ($1, $2, $3, $4, $5)`, item.Id, user.Id, userItem.IsRead, userItem.IsGlued, userItem.CreatedAt.Time); err != nil {
		panic(err)
	}

	return userItem
}
