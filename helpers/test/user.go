package test_helper

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/model"
	"github.com/yezooz/null"
)

func MakeTestUser() model.User {
	freshUser := model.User{
		Email:    helpers.RandomEmail(),
		Password: null.StringFrom(helpers.RandomString(64)),
	}

	return freshUser
}
