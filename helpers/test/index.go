package test_helper

import "bitbucket.org/epicglue/api/connection"

func HasIndex(indexName string) bool {
	es := connection.NewConnector().GetES()

	has, err := es.IndicesExists(indexName)

	return has == true && err == nil
}
