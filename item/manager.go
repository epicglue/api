package item

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/item/data_source"
	"bitbucket.org/epicglue/api/model"
)

var log = helpers.GetLogger("items")

type ItemManager interface {
	// TODO: all should return errors
	Search(*data_source.DataSource) (*model.Items, error)
	Count(*data_source.DataSource) (int64, error)

	Glue(*data_source.DataSource)
	Unglue(*data_source.DataSource)

	Read(*data_source.DataSource)
	Unread(*data_source.DataSource)

	Delete(*data_source.DataSource) error
}
