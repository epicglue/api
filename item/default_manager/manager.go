package item_manager

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/item/store/cache/redis"
	"bitbucket.org/epicglue/api/item/store/db/postgres"
	"bitbucket.org/epicglue/api/item/store/index/elasticsearch"
	"bitbucket.org/epicglue/api/model"
	"net/http"
)

var log = helpers.GetLogger("item")

type DefaultItemManager struct {
	user  *model.User
	db    *item_store_postgres.ItemStorePostgres
	index *item_store_elasticsearch.ItemStoreElasticsearch
	cache *item_store_redis.ItemStoreRedis
}

func NewDefaultItemManagerFromRequest(r *http.Request) *DefaultItemManager {
	if user := model.LoadUserByUsername(r.Header.Get("User")); user != nil {
		return &DefaultItemManager{
			user: user,
		}
	}

	log.Panic("User not set")

	return nil
}

func NewDefaultItemManagerWithUser(user *model.User) *DefaultItemManager {
	return &DefaultItemManager{
		user: user,
	}
}

func NewDefaultItemManager(userId int64) *DefaultItemManager {
	if user := model.LoadUser(userId); user != nil {
		return NewDefaultItemManagerWithUser(user)
	}

	return nil
}

func (m DefaultItemManager) LogFields() map[string]interface{} {
	return m.user.LogFields()
}

func (m DefaultItemManager) getDB() *item_store_postgres.ItemStorePostgres {
	if m.db == nil {
		m.db = item_store_postgres.NewItemStorePostgres()
	}

	return m.db
}

func (m DefaultItemManager) getIndex() *item_store_elasticsearch.ItemStoreElasticsearch {
	if m.index == nil {
		m.index = item_store_elasticsearch.NewItemStoreElasticsearch()
	}

	return m.index
}

func (m DefaultItemManager) getCache() *item_store_redis.ItemStoreRedis {
	if m.cache == nil {
		m.cache = item_store_redis.NewItemStoreRedis()
	}

	return m.cache
}
