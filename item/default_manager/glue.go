package item_manager

import (
	"bitbucket.org/epicglue/api/item/data_source"
	"bitbucket.org/epicglue/api/model"
)

func (m DefaultItemManager) Glue(dataSource *data_source.DataSource) {
	tempList := make([]string, 0)

	m.getIndex().ProcessAllItems(m.user, dataSource, func(items *model.Items) {
		for _, item := range items.Items {
			tempList = append(tempList, item.Id)

			if len(tempList) == 100 {
				m.glueItemSet(tempList)
				m.readItemSet(tempList)

				tempList = make([]string, 0)
			}
		}
	})

	m.glueItemSet(tempList)
	m.readItemSet(tempList)
}

func (m DefaultItemManager) Unglue(dataSource *data_source.DataSource) {
	tempList := make([]string, 0)

	m.getIndex().ProcessAllItems(m.user, dataSource, func(items *model.Items) {
		for _, item := range items.Items {
			tempList = append(tempList, item.Id)

			if len(tempList) == 100 {
				m.unglueItemSet(tempList)

				tempList = make([]string, 0)
			}
		}
	})

	m.unglueItemSet(tempList)
}

func (m DefaultItemManager) glueItemSet(itemIds []string) error {
	if len(itemIds) == 0 {
		return nil
	}

	if err := m.getDB().Glue(m.user, itemIds); err != nil {
		log.Error(err)
		return err
	}

	for _, itemId := range itemIds {
		if err := m.getIndex().Glue(m.user, itemId); err != nil {
			log.Error(err)
			return err
		}
	}

	return nil
}

func (m DefaultItemManager) unglueItemSet(itemIds []string) error {
	if len(itemIds) == 0 {
		return nil
	}

	if err := m.getDB().Unglue(m.user, itemIds); err != nil {
		log.Error(err)
		return err
	}

	for _, itemId := range itemIds {
		if err := m.getIndex().Unglue(m.user, itemId); err != nil {
			log.Error(err)
			return err
		}
	}

	return nil
}
