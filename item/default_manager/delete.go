package item_manager

import (
	"bitbucket.org/epicglue/api/item/data_source"
	"bitbucket.org/epicglue/api/model"
)

func (m DefaultItemManager) Delete(dataSource *data_source.DataSource) error {
	var err error
	tempList := make([]string, 0)

	m.getIndex().ProcessAllItems(m.user, dataSource, func(items *model.Items) {
		for _, item := range items.Items {
			tempList = append(tempList, item.Id)

			if len(tempList) == 100 {
				if inErr := m.deleteItemSet(tempList); inErr != nil {
					err = inErr
					return
				}

				tempList = make([]string, 0)
			}
		}
	})

	if err != nil {
		return err
	}

	return m.deleteItemSet(tempList)
}

func (m DefaultItemManager) deleteItemSet(itemIds []string) error {
	if len(itemIds) == 0 {
		return nil
	}

	if err := m.getDB().Delete(m.user, itemIds); err != nil {
		log.Error(err)
		return err
	}

	for _, itemId := range itemIds {
		if err := m.getIndex().Delete(m.user, itemId); err != nil {
			log.Error(err)
			return err
		}
	}

	return nil
}
