package item_manager

import (
	"bitbucket.org/epicglue/api/item/data_source"
	"bitbucket.org/epicglue/api/model"
)

func (m DefaultItemManager) Read(dataSource *data_source.DataSource) {
	tempList := make([]string, 0)

	m.getIndex().ProcessAllItems(m.user, dataSource, func(items *model.Items) {
		for _, item := range items.Items {
			tempList = append(tempList, item.Id)

			if len(tempList) == 100 {
				m.readItemSet(tempList)

				tempList = make([]string, 0)
			}
		}
	})

	m.readItemSet(tempList)
}

func (m DefaultItemManager) Unread(dataSource *data_source.DataSource) {
	tempList := make([]string, 0)

	m.getIndex().ProcessAllItems(m.user, dataSource, func(items *model.Items) {
		for _, item := range items.Items {
			tempList = append(tempList, item.Id)

			if len(tempList) == 100 {
				m.unreadItemSet(tempList)

				tempList = make([]string, 0)
			}
		}
	})

	m.unreadItemSet(tempList)
}

func (m DefaultItemManager) readItemSet(itemIds []string) error {
	if len(itemIds) == 0 {
		return nil
	}

	if err := m.getDB().Read(m.user, itemIds); err != nil {
		log.Error(err)
		return err
	}

	for _, itemId := range itemIds {
		if err := m.getIndex().Read(m.user, itemId); err != nil {
			log.Error(err)
			return err
		}
	}

	return nil
}

func (m DefaultItemManager) unreadItemSet(itemIds []string) error {
	if len(itemIds) == 0 {
		return nil
	}

	if err := m.getDB().Unread(m.user, itemIds); err != nil {
		log.Error(err)
		return err
	}

	for _, itemId := range itemIds {
		if err := m.getIndex().Unread(m.user, itemId); err != nil {
			log.Error(err)
			return err
		}
	}

	return nil
}
