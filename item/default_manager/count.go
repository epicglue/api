package item_manager

import (
	"bitbucket.org/epicglue/api/item/data_source"
	"github.com/Sirupsen/logrus"
	"github.com/imdario/mergo"
	"time"
)

func (m DefaultItemManager) Count(dataSource *data_source.DataSource) (int64, error) {
	startedAt := time.Now()

	count := m.getIndex().Count(m.user, dataSource)

	// ??????? TODO: remove ???????????

	logFields := logrus.Fields{
		"took":     time.Since(startedAt),
		"took_str": time.Since(startedAt).String(),
		"query":    dataSource.BuildSearchQuery(),
		"module":   "item",
		"method":   "Count",
		"result":   count,
	}
	mergo.Map(&logFields, dataSource.LogFields())
	log.WithFields(logFields).Debug("Count")

	return count, nil
}
