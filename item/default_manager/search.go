package item_manager

import (
	"bitbucket.org/epicglue/api/item/data_source"
	"bitbucket.org/epicglue/api/model"
	"github.com/Sirupsen/logrus"
	"github.com/imdario/mergo"
	"time"
)

func (m DefaultItemManager) Search(dataSource *data_source.DataSource) (*model.Items, error) {
	startedAt := time.Now()

	items := m.getIndex().Search(m.user, dataSource)

	logFields := logrus.Fields{
		"took":     time.Since(startedAt),
		"took_str": time.Since(startedAt).String(),
		"query":    dataSource.BuildSearchQuery(),
	}
	mergo.Map(&logFields, dataSource.LogFields())
	log.WithFields(logFields).Info("Search")

	return items, nil
}
