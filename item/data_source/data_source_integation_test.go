// +build integration

package data_source_test

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/item/data_source"
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"net/http"
	"runtime/debug"
	"testing"
)

var log = helpers.GetLogger("tests")

func TestWithSubscription(t *testing.T) {
	ds := data_source.NewDataSourceFromHttpRequest(buildURLWith("sub:1|service:twitter,instagram|tag:0001|author:deadmau5|media:photo,video|before:1341429364|search:ass,toys,one+of|blacklist:3,4,5|unread|no_glue"))

	assert.Equal(t, `{"query":{"filtered":{"query":{"bool":{"must":[{"bool":{"should":[{"multi_match":{"query":"ass","fields":["title","description","location_name","url"]}},{"multi_match":{"query":"toys","fields":["title","description","location_name","url"]}},{"multi_match":{"query":"one of","fields":["title","description","location_name","url"]}}]}},{"term":{"author":"deadmau5"}},{"in":{"media_type":["photo","video"]}},{"in":{"service":["twitter","instagram"]}},{"term":{"tags":"0001"}}]}},"filter":{"and":[{"not":{"ids":{"values":["3","4","5"]}}},{"term":{"subs":1}},{"range":{"content_order_by":{"gt":"2012-07-04T21:16:04+02:00"}}},{"missing":{"field":"glued"}},{"missing":{"field":"read"}}]}}}}`, ds.BuildSearchQuery())
}

func TestJustFilter(t *testing.T) {
	ds := data_source.NewDataSourceFromHttpRequest(buildURLWith("before:1341429364"))
	assert.Equal(t, `{"query":{"filtered":{"filter":{"range":{"content_order_by":{"gt":"2012-07-04T21:16:04+02:00"}}}}}}}`, ds.BuildSearchQuery())
}

func TestJustQuery(t *testing.T) {
	ds := data_source.NewDataSourceFromHttpRequest(buildURLWith("author:deadmau5"))
	assert.Equal(t, `{"query":{"bool":{"must":[{"term":{"author":"deadmau5"}}]}}}`, ds.BuildSearchQuery())
}

func TestNone(t *testing.T) {
	ds := data_source.NewDataSourceFromHttpRequest(buildURLWith(""))
	assert.Equal(t, `{}`, ds.BuildSearchQuery())
}

func buildURLWith(param string) *http.Request {
	req, err := http.NewRequest("GET", fmt.Sprintf("http://api.epicglue.com/items?q=%s", param), nil)
	req.Header.Set("User", "marek")
	req.Header.Set("UserId", "1")

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)
	}

	return req
}
