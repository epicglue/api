package data_source

import (
	"bitbucket.org/epicglue/api/item/data_source/filter"
	"fmt"
	"strings"
)

func (ds *DataSource) BuildQuery() string {
	queries := []string{}
	var tempQueue *Queries

	if len(ds.GetSearchQueries()) > 0 {
		tempQueue = &Queries{}
		for _, q := range ds.GetSearchQueries() {
			tempQueue.Add(&Query{
				Fields: []string{"title", "description", "location_name", "url"},
				Value:  q.Value,
			})
		}

		queries = append(queries, tempQueue.Join())
	}

	if len(ds.GetAuthors()) > 0 {
		tempQueue = &Queries{}
		for _, q := range ds.GetAuthors() {
			tempQueue.Add(&Query{
				Fields: []string{"author"},
				Value:  q,
			})
		}

		queries = append(queries, tempQueue.Join())
	}

	if len(ds.GetMediaTypes()) > 0 {
		tempQueue = &Queries{}
		for _, q := range ds.GetMediaTypes() {
			tempQueue.Add(&Query{
				Fields: []string{"media_type"},
				Value:  q,
			})
		}

		queries = append(queries, tempQueue.Join())
	}

	if len(ds.GetServices()) > 0 {
		tempQueue = &Queries{}
		for _, q := range ds.GetServices() {
			tempQueue.Add(&Query{
				Fields: []string{"service"},
				Value:  q.ShortName,
			})
		}
		queries = append(queries, tempQueue.Join())
	}

	if len(ds.GetTags()) > 0 {
		tempQueue = &Queries{}
		for _, q := range ds.GetTags() {
			tempQueue.Add(&Query{
				Fields: []string{"tags"},
				Value:  q,
			})
		}
		queries = append(queries, tempQueue.Join())
	}

	if len(queries) == 0 {
		return ""
	}

	return fmt.Sprintf(`"query":{"bool":{"must":[%s]}}`, strings.Join(queries, ","))
}

func (ds *DataSource) BuildFilters() string {
	var filters []string

	if len(ds.GetItemIds()) > 0 {
		iif := &filter.ItemIdFilter{
			Ids: ds.GetItemIds(),
		}
		filters = append(filters, iif.BuildFilter())
	}

	if len(ds.GetBlacklistItemIds()) > 0 {
		bf := &filter.BlacklistFilter{
			Ids: ds.GetBlacklistItemIds(),
		}
		filters = append(filters, bf.BuildFilter())
	}

	if len(ds.GetSubscriptions()) > 0 {
		sf := &filter.SubscriptionFilter{
			Subscriptions: ds.GetSubscriptions(),
		}
		filters = append(filters, sf.BuildFilter())
	}

	if !ds.GetBefore().IsZero() {
		drf := &filter.DateRangeFilter{
			Gt: ds.GetBefore(),
		}
		filters = append(filters, drf.BuildFilter())
	}

	if !ds.GetAfter().IsZero() {
		drf := &filter.DateRangeFilter{
			Lt: ds.GetAfter(),
		}
		filters = append(filters, drf.BuildFilter())
	}

	if ds.GetOnlyGlued() || ds.GetOnlyNotGlued() {
		f := &filter.GlueFilter{
			OnlyGlued:    ds.GetOnlyGlued(),
			OnlyNotGlued: ds.GetOnlyNotGlued(),
		}

		filters = append(filters, f.BuildFilter())
	}

	if ds.GetOnlyRead() || ds.GetOnlyUnread() {
		f := &filter.UnreadFilter{
			OnlyRead:   ds.GetOnlyRead(),
			OnlyUnread: ds.GetOnlyUnread(),
		}

		filters = append(filters, f.BuildFilter())
	}

	switch len(filters) {
	case 0:
		return ""
	case 1:
		return fmt.Sprintf(`"filter":%s}`, filters[0])
	default:
		return fmt.Sprintf(`"filter":{"and":[%s]}`, strings.Join(filters, ","))
	}
}

func (ds *DataSource) BuildSearchQuery() string {
	query := ds.BuildQuery()
	filter := ds.BuildFilters()

	if query == "" && filter == "" {
		return "{}"
	} else if filter == "" {
		return fmt.Sprintf(`{%s}`, query)
	} else if query == "" {
		return fmt.Sprintf(`{"query":{"filtered":{%s}}}`, filter)
	} else {
		return fmt.Sprintf(`{"query":{"filtered":{%s,%s}}}`, query, filter)
	}
}
