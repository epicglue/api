package filter

type GlueFilter struct {
	OnlyGlued    bool
	OnlyNotGlued bool
}

func (f *GlueFilter) BuildFilter() string {
	if f.OnlyGlued && f.OnlyNotGlued {
		return ""
	}

	if f.OnlyNotGlued {
		return `{"missing":{"field":"glued"}}`
	}

	if f.OnlyGlued {
		return `{"exists":{"field":"glued"}}`
	}

	return ""
}
