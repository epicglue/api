package filter_test

import (
	"bitbucket.org/epicglue/api/item/data_source/filter"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func getTestDate() time.Time {
	return time.Date(2016, time.January, 1, 3, 37, 6, 66, time.UTC)
}

func getTestDateWithTimezone() time.Time {
	tz, _ := time.LoadLocation("America/New_York")
	return time.Date(2016, time.January, 1, 3, 37, 6, 66, tz)
}

func TestGreaterThan(t *testing.T) {
	f := filter.DateRangeFilter{
		Gt: getTestDate(),
	}

	assert.Equal(t, `{"range":{"content_order_by":{"gt":"2016-01-01T03:37:06Z"}}}`, f.BuildFilter())

	f = filter.DateRangeFilter{
		Gt: getTestDateWithTimezone(),
	}

	assert.Equal(t, `{"range":{"content_order_by":{"gt":"2016-01-01T03:37:06-05:00"}}}`, f.BuildFilter())
}

func TestGreaterOrEqual(t *testing.T) {
	f := filter.DateRangeFilter{
		Gte: getTestDate(),
	}

	assert.Equal(t, `{"range":{"content_order_by":{"gte":"2016-01-01T03:37:06Z"}}}`, f.BuildFilter())

	f = filter.DateRangeFilter{
		Gte: getTestDateWithTimezone(),
	}

	assert.Equal(t, `{"range":{"content_order_by":{"gte":"2016-01-01T03:37:06-05:00"}}}`, f.BuildFilter())
}

func TestLessThan(t *testing.T) {
	f := filter.DateRangeFilter{
		Lt: getTestDate(),
	}

	assert.Equal(t, `{"range":{"content_order_by":{"lt":"2016-01-01T03:37:06Z"}}}`, f.BuildFilter())

	f = filter.DateRangeFilter{
		Lt: getTestDateWithTimezone(),
	}

	assert.Equal(t, `{"range":{"content_order_by":{"lt":"2016-01-01T03:37:06-05:00"}}}`, f.BuildFilter())
}

func TestLessOrEqual(t *testing.T) {
	f := filter.DateRangeFilter{
		Lte: getTestDate(),
	}

	assert.Equal(t, `{"range":{"content_order_by":{"lte":"2016-01-01T03:37:06Z"}}}`, f.BuildFilter())

	f = filter.DateRangeFilter{
		Lte: getTestDateWithTimezone(),
	}

	assert.Equal(t, `{"range":{"content_order_by":{"lte":"2016-01-01T03:37:06-05:00"}}}`, f.BuildFilter())
}

//func TestDateRangeFilter(t *testing.T) {
//	var drf filter.DateRangeFilter
//	now := time.Now()
//
//	assert.Equal(t, fmt.Sprintf(`{"range":{"content_order_by":{"gte":"%s"}}}`, now.Format(time.RFC3339)), drf.BuildFilter())
//
//	drf = item.DateRangeFilter{
//		Gt: now,
//		Lt: now,
//	}
//	assert.Equal(t, fmt.Sprintf(`{"range":{"content_order_by":{"gt":"%s","lt":"%s"}}}`, now.Format(time.RFC3339), now.Format(time.RFC3339)), drf.BuildFilter())
//
//	drf = item.DateRangeFilter{
//		Lte:      now,
//		Timezone: "+1:00",
//	}
//	assert.Equal(t, fmt.Sprintf(`{"range":{"content_order_by":{"lte":"%s","time_zone":"+1:00"}}}`, now.Format(time.RFC3339)), drf.BuildFilter())
//}
