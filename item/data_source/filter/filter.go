package filter

type Filters []Filter

type Filter interface {
	BuildFilter() string
}
