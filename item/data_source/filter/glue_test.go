package filter_test

import (
	"bitbucket.org/epicglue/api/item/data_source/filter"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGlueFilter(t *testing.T) {
	var pf filter.GlueFilter

	pf = filter.GlueFilter{}
	assert.Equal(t, "", pf.BuildFilter())

	pf = filter.GlueFilter{
		OnlyGlued: true,
	}
	assert.Equal(t, `{"exists":{"field":"glued"}}`, pf.BuildFilter())

	pf = filter.GlueFilter{
		OnlyNotGlued: true,
	}
	assert.Equal(t, `{"missing":{"field":"glued"}}`, pf.BuildFilter())

	pf = filter.GlueFilter{
		OnlyGlued:    true,
		OnlyNotGlued: true,
	}
	assert.Equal(t, "", pf.BuildFilter())
}
