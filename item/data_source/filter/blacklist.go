package filter

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/model"
	"fmt"
)

type BlacklistFilter struct {
	Ids []*model.Item
}

func (f *BlacklistFilter) BuildFilter() string {
	list := []string{}
	for _, item := range f.Ids {
		list = append(list, item.Id)
	}

	if len(list) == 0 {
		return ""
	}

	return fmt.Sprintf(`{"not":{"ids":{"values":%s}}}`, helpers.FlattenArrayOfStrings(list))
}
