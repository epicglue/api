package filter_test

import (
	"bitbucket.org/epicglue/api/item/data_source/filter"
	"bitbucket.org/epicglue/api/model"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestBlacklistFilter(t *testing.T) {
	var bf filter.BlacklistFilter
	i1 := &model.Item{
		Id: "1",
	}
	i2 := &model.Item{
		Id: "2",
	}

	bf = filter.BlacklistFilter{}
	assert.Equal(t, "", bf.BuildFilter())

	bf = filter.BlacklistFilter{
		Ids: []*model.Item{i1},
	}
	assert.Equal(t, `{"not":{"ids":{"values":["1"]}}}`, bf.BuildFilter())

	bf = filter.BlacklistFilter{
		Ids: []*model.Item{i1, i2},
	}
	assert.Equal(t, `{"not":{"ids":{"values":["1","2"]}}}`, bf.BuildFilter())
}
