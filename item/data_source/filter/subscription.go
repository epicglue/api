package filter

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/model"
	"fmt"
)

type SubscriptionFilter struct {
	Subscriptions []*model.Subscription
}

func (f *SubscriptionFilter) BuildFilter() string {
	list := []int64{}
	for _, sub := range f.Subscriptions {
		list = append(list, sub.Id)
	}

	if len(list) == 0 {
		return ""
	}

	if len(list) == 1 {
		return fmt.Sprintf(`{"term":{"subs":%d}}`, list[0])
	} else {
		return fmt.Sprintf(`{"in":{"subs":%s}}`, helpers.FlattenArrayOfInts(list))
	}
}
