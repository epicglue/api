package filter_test

import (
	"bitbucket.org/epicglue/api/item/data_source/filter"
	"bitbucket.org/epicglue/api/model"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestFilterWithNoSubs(t *testing.T) {
	filter := filter.SubscriptionFilter{
		Subscriptions: []*model.Subscription{},
	}

	assert.Equal(t, "", filter.BuildFilter())
}

func TestFilterWithSingleSub(t *testing.T) {
	subs := []*model.Subscription{
		&model.Subscription{
			Id: 1,
		},
	}

	filter := filter.SubscriptionFilter{
		Subscriptions: subs,
	}

	assert.Equal(t, `{"term":{"subs":1}}`, filter.BuildFilter())
}

func TestFilterWithMultipleSubs(t *testing.T) {
	subs := []*model.Subscription{
		&model.Subscription{
			Id: 1,
		},
		&model.Subscription{
			Id: 2,
		},
		&model.Subscription{
			Id: 3,
		},
	}

	filter := filter.SubscriptionFilter{
		Subscriptions: subs,
	}

	assert.Equal(t, `{"in":{"subs":[1,2,3]}}`, filter.BuildFilter())
}
