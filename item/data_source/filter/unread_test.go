package filter_test

import (
	"bitbucket.org/epicglue/api/item/data_source/filter"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestUnreadFilter(t *testing.T) {
	var uf filter.UnreadFilter

	uf = filter.UnreadFilter{}
	assert.Equal(t, "", uf.BuildFilter())

	uf = filter.UnreadFilter{
		OnlyUnread: true,
	}
	assert.Equal(t, `{"missing":{"field":"read"}}`, uf.BuildFilter())

	uf = filter.UnreadFilter{
		OnlyRead: true,
	}
	assert.Equal(t, `{"exists":{"field":"read"}}`, uf.BuildFilter())

	uf = filter.UnreadFilter{
		OnlyRead:   true,
		OnlyUnread: true,
	}
	assert.Equal(t, "", uf.BuildFilter())
}
