package filter

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/model"
	"fmt"
)

type ItemIdFilter struct {
	Ids []*model.Item
}

func (f *ItemIdFilter) BuildFilter() string {
	list := []string{}
	for _, item := range f.Ids {
		list = append(list, item.Id)
	}

	if len(list) == 0 {
		return ""
	}

	return fmt.Sprintf(`{"ids":{"values":%s}}`, helpers.FlattenArrayOfStrings(list))
}
