package filter

type UnreadFilter struct {
	OnlyUnread bool
	OnlyRead   bool
}

func (f *UnreadFilter) BuildFilter() string {
	if f.OnlyRead && f.OnlyUnread {
		return ""
	}

	if f.OnlyUnread {
		return `{"missing":{"field":"read"}}`
	}

	if f.OnlyRead {
		return `{"exists":{"field":"read"}}`
	}

	return ""
}
