package filter

import (
	"fmt"
	"strings"
	"time"
)

type DateRangeFilter struct {
	Gte      time.Time
	Lte      time.Time
	Gt       time.Time
	Lt       time.Time
	Timezone string
}

func (f *DateRangeFilter) BuildFilter() string {
	filters := []string{}

	if !f.Gt.IsZero() {
		filters = append(filters, fmt.Sprintf(`"gt":"%s"`, f.Gt.Format(time.RFC3339)))
	}
	if !f.Gte.IsZero() {
		filters = append(filters, fmt.Sprintf(`"gte":"%s"`, f.Gte.Format(time.RFC3339)))
	}
	if !f.Lt.IsZero() {
		filters = append(filters, fmt.Sprintf(`"lt":"%s"`, f.Lt.Format(time.RFC3339)))
	}
	if !f.Lte.IsZero() {
		filters = append(filters, fmt.Sprintf(`"lte":"%s"`, f.Lte.Format(time.RFC3339)))
	}

	if len(filters) == 0 {
		return ""
	}

	if f.Timezone != "" {
		filters = append(filters, fmt.Sprintf(`"time_zone":"%s"`, f.Timezone))
	}

	return fmt.Sprintf(`{"range":{"content_order_by":{%s}}}`, strings.Join(filters, ","))
}
