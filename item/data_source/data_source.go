package data_source

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/model"
	"errors"
	"fmt"
	"github.com/Sirupsen/logrus"
	"net/http"
	"runtime/debug"
	"strconv"
	"strings"
	"time"
)

var log = helpers.GetLogger("data_source")

// DataSource holds various values to figure out what endpoint to call. Also helps with managing before/after and build Redis keys
type DataSource struct {
	itemIds          []*model.Item
	blacklistItemIds []*model.Item
	services         []*model.Service
	subscriptions    []*model.Subscription
	tags             []string
	authors          []string
	mediaTypes       []string
	before           time.Time
	after            time.Time
	queries          []*Query
	// TODO: naming
	onlyRead     bool
	onlyNotRead  bool
	onlyGlued    bool
	onlyNotGlued bool
}

const (
	PARAM_ID           string = "id"
	PARAM_BLACKLIST_ID string = "blacklist"
	PARAM_SUBSCRIPTION string = "sub"
	PARAM_SERVICE      string = "service"
	PARAM_TAG          string = "tag"
	PARAM_AUTHOR       string = "author"
	PARAM_MEDIA_TYPE   string = "media"
	PARAM_BEFORE       string = "before"
	PARAM_AFTER        string = "after"
	PARAM_SEARCH       string = "search"
	PARAM_IS_GLUED     string = "glue"
	PARAM_IS_NOT_GLUED string = "no_glue"
	PARAM_IS_READ      string = "read"
	PARAM_IS_UNREAD    string = "unread"
)

func NewDataSourceFromHttpRequest(r *http.Request) *DataSource {
	params := strings.Split(r.FormValue("q"), "|")

	ds := DataSource{}

	if r.FormValue("q") == "" {
		return &ds
	}

	ds.SetFromQueryString(params...)

	return &ds
}

func (ds *DataSource) SetFromQueryString(values ...string) {
	for _, group := range values {
		if err := ds.setParam(group); err != nil {
			continue
		}
	}
}

func (ds *DataSource) SetKey(key string) error {
	return ds.setParam(key)
}

func (ds *DataSource) SetKeyAndValue(key string, value string) error {
	return ds.setParam(fmt.Sprintf("%s:%s", key, value))
}

func (ds *DataSource) setParam(block string) error {
	if !strings.Contains(block, ":") {
		if block == PARAM_IS_READ {
			ds.setOnlyRead(true)
		} else if block == PARAM_IS_UNREAD {
			ds.setOnlyUnread(true)
		} else if block == PARAM_IS_GLUED {
			ds.setOnlyGlued(true)
		} else if block == PARAM_IS_NOT_GLUED {
			ds.setOnlyNotGlued(true)
		} else {
			return errors.New(fmt.Sprintf("Failed to match param %s", block))
		}

		return nil
	}

	kvGroup := strings.Split(block, ":")

	if len(kvGroup) != 2 {
		return errors.New(fmt.Sprintf("Invalid param %s", block))
	}

	key := kvGroup[0]
	values := strings.Split(kvGroup[1], ",")

	if key == PARAM_SUBSCRIPTION {
		ds.setSubscriptions(values)
	} else if key == PARAM_SERVICE {
		ds.setServices(values)
	} else if key == PARAM_TAG {
		ds.setTags(values)
	} else if key == PARAM_AUTHOR {
		ds.setAuthors(values)
	} else if key == PARAM_MEDIA_TYPE {
		ds.setMediaTypes(values)
	} else if key == PARAM_BEFORE {
		ds.setBefore(values)
	} else if key == PARAM_AFTER {
		ds.setAfter(values)
	} else if key == PARAM_SEARCH {
		ds.setSearchQueries(values)
	} else if key == PARAM_ID {
		ds.setItemIds(values)
	} else if key == PARAM_BLACKLIST_ID {
		ds.setBlacklistItemIds(values)
	} else {
		return errors.New(fmt.Sprintf("Failed to match param %s", block))
	}

	return nil
}

func (ds *DataSource) GetItemIds() []*model.Item {
	return ds.itemIds
}

func (ds *DataSource) setItemIds(ids []string) {
	for _, id := range ids {
		ds.itemIds = append(ds.itemIds, &model.Item{
			Id: id,
		})
	}
}

func (ds *DataSource) AddItemIds(ids []string) {
	ds.setItemIds(ids)
}

func (ds *DataSource) GetBlacklistItemIds() []*model.Item {
	return ds.blacklistItemIds
}

func (ds *DataSource) setBlacklistItemIds(ids []string) {
	for _, id := range ids {
		ds.blacklistItemIds = append(ds.blacklistItemIds, &model.Item{
			Id: id,
		})
	}
}

func (ds *DataSource) AddBlacklistItemIds(ids []string) {
	ds.setBlacklistItemIds(ids)
}

func (ds *DataSource) GetServices() []*model.Service {
	return ds.services
}

func (ds *DataSource) setServices(services []string) {
	for _, srv := range services {
		ds.services = append(ds.services, &model.Service{
			ShortName: srv,
		})
	}
}

func (ds *DataSource) GetSubscriptions() []*model.Subscription {
	return ds.subscriptions
}

func (ds *DataSource) setSubscriptions(subs []string) {
	ds.subscriptions = []*model.Subscription{}

	for _, sub := range subs {
		id, _ := strconv.ParseInt(sub, 10, 64)
		ds.subscriptions = append(ds.subscriptions, &model.Subscription{
			Id: id,
		})
	}
}

func (ds *DataSource) GetTags() []string {
	return ds.tags
}

func (ds *DataSource) setTags(tags []string) {
	ds.tags = tags
}

func (ds *DataSource) GetAuthors() []string {
	return ds.authors
}

func (ds *DataSource) setAuthors(authors []string) {
	ds.authors = authors
}

func (ds *DataSource) GetMediaTypes() []string {
	return ds.mediaTypes
}

func (ds *DataSource) setMediaTypes(mediaTypes []string) {
	ds.mediaTypes = mediaTypes
}

func (ds *DataSource) GetBefore() time.Time {
	return ds.before
}

func (ds *DataSource) setBefore(params []string) {
	if len(params) < 1 {
		log.Warning("Empty param sent to setBefore")
	}

	t, err := strconv.ParseInt(params[0], 10, 64)
	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Warningf("Param %s incorrect", params[0])
	}

	ds.before = time.Unix(t, 0)
}

func (ds *DataSource) GetAfter() time.Time {
	return ds.after
}

func (ds *DataSource) setAfter(params []string) {
	if len(params) < 1 {
		log.Warning("Empty param sent to setAfter")
	}

	t, err := strconv.ParseInt(params[0], 10, 64)
	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Warningf("Param %s incorrect", params[0])
	}
	ds.after = time.Unix(t, 0)
}

func (ds *DataSource) GetSearchQueries() []*Query {
	return ds.queries
}

func (ds *DataSource) setSearchQueries(queries []string) {
	for _, query := range queries {
		ds.queries = append(ds.queries, &Query{
			Value: query,
		})
	}
}

func (ds *DataSource) GetOnlyRead() bool {
	return ds.onlyRead
}

func (ds *DataSource) setOnlyRead(v bool) {
	ds.onlyRead = v
}

func (ds *DataSource) GetOnlyUnread() bool {
	return ds.onlyNotRead
}

func (ds *DataSource) setOnlyUnread(v bool) {
	ds.onlyNotRead = v
}

func (ds *DataSource) GetOnlyGlued() bool {
	return ds.onlyGlued
}

func (ds *DataSource) setOnlyGlued(v bool) {
	ds.onlyGlued = v
}

func (ds *DataSource) GetOnlyNotGlued() bool {
	return ds.onlyNotGlued
}

func (ds *DataSource) setOnlyNotGlued(v bool) {
	ds.onlyNotGlued = v
}
