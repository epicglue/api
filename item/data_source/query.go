package data_source

import (
	"bitbucket.org/epicglue/api/helpers"
	"fmt"
	"strings"
)

type Query struct {
	Value  string
	Fields []string
}

type Queries struct {
	queries []*Query
}

func (q *Queries) Add(query *Query) {
	q.queries = append(q.queries, query)
}

func (q *Queries) Join() string {
	// Multi Match or IN if len(fields) == 1
	if len(q.queries[0].Fields) == 1 {
		join := []string{}
		for _, query := range q.queries {
			join = append(join, query.Value)
		}

		if len(join) == 1 {
			return fmt.Sprintf(`{"term":{"%s":"%s"}}`, q.queries[0].Fields[0], join[0])
		} else {
			return fmt.Sprintf(`{"in":{"%s":%s}}`, q.queries[0].Fields[0], helpers.FlattenArrayOfStrings(join))
		}

	}

	join := []string{}
	for _, query := range q.queries {
		join = append(join, fmt.Sprintf(`{"multi_match":{"query":"%s","fields":%s}}`, query.Value, helpers.FlattenArrayOfStrings(query.Fields)))
	}

	return fmt.Sprintf(`{"bool":{"should":[%s]}}`, strings.Join(join, ","))
}
