package data_source

import (
	"fmt"
	"strings"
)

func (ds *DataSource) LogFields() map[string]interface{} {
	fields := map[string]interface{}{}

	if len(ds.GetTags()) > 0 {
		fields[PARAM_TAG] = strings.Join(ds.GetTags(), ",")
	}

	if len(ds.GetAuthors()) > 0 {
		fields[PARAM_AUTHOR] = strings.Join(ds.GetAuthors(), ",")
	}

	if len(ds.GetMediaTypes()) > 0 {
		fields[PARAM_MEDIA_TYPE] = strings.Join(ds.GetMediaTypes(), ",")
	}

	// Services
	services := []string{}
	for _, item := range ds.GetServices() {
		services = append(services, item.ShortName)
	}
	if len(services) > 0 {
		fields[PARAM_SERVICE] = strings.Join(services, ",")
	}

	// Subscriptions
	subscriptions := []string{}
	for _, item := range ds.GetSubscriptions() {
		subscriptions = append(subscriptions, fmt.Sprintf("%d", item.Id))
	}
	if len(subscriptions) > 0 {
		fields[PARAM_SUBSCRIPTION] = strings.Join(subscriptions, ",")
	}

	// Queries
	queries := []string{}
	for _, item := range ds.GetSearchQueries() {
		queries = append(queries, item.Value)
	}
	if len(queries) > 0 {
		fields[PARAM_SEARCH] = strings.Join(queries, ",")
	}

	// Before/After
	if !ds.GetAfter().IsZero() {
		fields[PARAM_AFTER] = ds.GetAfter().String()
	}
	if !ds.GetBefore().IsZero() {
		fields[PARAM_BEFORE] = ds.GetBefore().String()
	}

	// IDs
	if len(ds.GetItemIds()) > 0 {
		fields[PARAM_ID] = ds.GetItemIds()
	}
	if len(ds.GetBlacklistItemIds()) > 0 {
		fields[PARAM_BLACKLIST_ID] = ds.GetBlacklistItemIds()
	}

	// Flags
	if ds.GetOnlyGlued() {
		fields[PARAM_IS_GLUED] = true
	}
	if ds.GetOnlyNotGlued() {
		fields[PARAM_IS_NOT_GLUED] = true
	}
	if ds.GetOnlyRead() {
		fields[PARAM_IS_READ] = true
	}
	if ds.GetOnlyUnread() {
		fields[PARAM_IS_UNREAD] = true
	}

	return fields
}
