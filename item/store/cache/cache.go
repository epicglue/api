package item_store_cache

import "bitbucket.org/epicglue/api/worker/model"

type ItemStoreCache interface {
	AddTask(*worker_model.Task) error
	AddTasks([]*worker_model.Task) error
}
