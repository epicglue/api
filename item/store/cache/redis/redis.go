package item_store_redis

import (
	"bitbucket.org/epicglue/api/connection/key_value_store"
	"bitbucket.org/epicglue/api/connection/key_value_store/redis"
	"bitbucket.org/epicglue/api/helpers"
)

var log = helpers.GetLogger("redis")

type ItemStoreRedis struct {
	Connection key_value_store.KeyValueStore
}

func NewItemStoreRedis() *ItemStoreRedis {
	return &ItemStoreRedis{
		Connection: redis.NewRedis(),
	}
}
