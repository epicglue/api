package item_store_redis

import "bitbucket.org/epicglue/api/worker/model"

func (r ItemStoreRedis) AddTask(task *worker_model.Task) error {
	return r.Connection.AddToSet("tasks", task.String())
}

func (r ItemStoreRedis) AddTasks(tasks []*worker_model.Task) error {
	limit := 1000
	i := 0
	taskStrings := []string{}

	for _, task := range tasks {
		if i == limit {
			if err := r.Connection.AddToSet("tasks", taskStrings...); err != nil {
				return err
			}

			i = 0
		}

		taskStrings = append(taskStrings, task.String())
		i++
	}

	return nil
}
