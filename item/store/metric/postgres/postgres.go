package item_store_metric_postgres

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/metric"
)

var log = helpers.GetLogger("item_metric")

type ItemMetricPostgres struct {
	metric metric.Metric
}

func NewItemMetricPostgres() *ItemMetricPostgres {
	return &ItemMetricPostgres{
		metric: nil,
	}
}
