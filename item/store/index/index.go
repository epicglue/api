package item_store_index

import (
	"bitbucket.org/epicglue/api/item/data_source"
	"bitbucket.org/epicglue/api/model"
)

type ItemStoreIndex interface {
	Search(*model.User, *data_source.DataSource) *model.Items
	Count(*model.User, *data_source.DataSource) int64

	Delete(*model.User, string) error

	Glue(*model.User, string) error
	Unglue(*model.User, string) error

	Read(*model.User, string) error
	Unread(*model.User, string) error

	ProcessAllItems(*model.User, *data_source.DataSource, func(*model.Items)) error
}
