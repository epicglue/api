package item_store_elasticsearch

import (
	"bitbucket.org/epicglue/api/item/data_source"
	"bitbucket.org/epicglue/api/model"
)

func (es ItemStoreElasticsearch) Count(user *model.User, ds *data_source.DataSource) int64 {
	return es.connection.Count(user.IndexName(), ds.BuildSearchQuery(), es.countLimit)
}
