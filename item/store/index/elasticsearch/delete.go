package item_store_elasticsearch

import (
	"bitbucket.org/epicglue/api/model"
)

func (i ItemStoreElasticsearch) Delete(user *model.User, itemId string) error {
	if err := i.connection.Delete(user.IndexName(), itemId); err != nil {
		return err
	}

	return nil
}
