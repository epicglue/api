package item_store_elasticsearch

import (
	"bitbucket.org/epicglue/api/model"
)

func (i ItemStoreElasticsearch) Glue(user *model.User, itemId string) error {
	return i.connection.Update(user.IndexName(), itemId, `{"doc": {"glued":true}}`)
}

func (i ItemStoreElasticsearch) Unglue(user *model.User, itemId string) error {
	return i.connection.RemoveField(user.IndexName(), itemId, "glued")
}
