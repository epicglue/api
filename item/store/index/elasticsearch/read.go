package item_store_elasticsearch

import (
	"bitbucket.org/epicglue/api/model"
)

func (i ItemStoreElasticsearch) Read(user *model.User, itemId string) error {
	return i.connection.Update(user.IndexName(), itemId, `{"doc": {"read":true}}`)
}

func (i ItemStoreElasticsearch) Unread(user *model.User, itemId string) error {
	return i.connection.RemoveField(user.IndexName(), itemId, "read")
}
