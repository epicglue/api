package item_store_elasticsearch

import (
	"bitbucket.org/epicglue/api/config"
	"bitbucket.org/epicglue/api/connection/index"
	"bitbucket.org/epicglue/api/connection/index/elasticsearch"
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/item/data_source"
	"bitbucket.org/epicglue/api/model"
)

var log = helpers.GetLogger("item")

type ItemStoreElasticsearch struct {
	connection index.Index
	countLimit int64
}

func NewItemStoreElasticsearch() *ItemStoreElasticsearch {
	config := config.LoadConfig()

	return &ItemStoreElasticsearch{
		connection: elasticsearch.NewElasticsearch(),
		countLimit: config.API.CountLimit,
	}
}

func (i ItemStoreElasticsearch) ProcessAllItems(user *model.User, dataSource *data_source.DataSource, fn func(*model.Items)) error {
	if err := i.connection.ScanAll(user.IndexName(), dataSource.BuildSearchQuery(), fn); err != nil {
		return err
	}

	log.Debugf("Process items for query: %s", dataSource.BuildSearchQuery())

	return nil
}
