package item_store_elasticsearch

import (
	"bitbucket.org/epicglue/api/item/data_source"
	"bitbucket.org/epicglue/api/model"
)

func (es ItemStoreElasticsearch) Search(user *model.User, ds *data_source.DataSource) *model.Items {
	results, err := es.connection.Search(user.IndexName(), ds.BuildSearchQuery())
	if err != nil {
		return es.emptyResultSet()
	}

	items, err := model.ItemsFromHits(results)
	if err != nil {
		return es.emptyResultSet()
	}

	return items
}

func (es ItemStoreElasticsearch) emptyResultSet() *model.Items {
	return &model.Items{
		Items: make([]*model.Item, 0),
	}
}
