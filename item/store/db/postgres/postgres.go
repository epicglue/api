package item_store_postgres

import (
	"bitbucket.org/epicglue/api/connection/database/postgres"
	"bitbucket.org/epicglue/api/helpers"
	"github.com/jinzhu/gorm"
)

var log = helpers.GetLogger("item_store")

type ItemStorePostgres struct {
	connection *gorm.DB
}

func NewItemStorePostgres() *ItemStorePostgres {
	return &ItemStorePostgres{
		connection: postgres.NewPostgres(),
	}
}
