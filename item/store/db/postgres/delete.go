package item_store_postgres

import (
	"bitbucket.org/epicglue/api/model"
	"fmt"
	"github.com/Sirupsen/logrus"
	"runtime/debug"
	"strings"
	"time"
)

func (i ItemStorePostgres) Delete(user *model.User, itemIds []string) error {
	start := time.Now()

	query := fmt.Sprintf(`
			UPDATE
				user_item
			SET
				is_deleted = TRUE
			WHERE
			    user_id = %d AND
				item_id IN ('%s')`, user.Id, strings.Join(itemIds, `','`))

	_, err := i.connection.DB().Exec(query)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":    string(debug.Stack()),
			"took":     time.Since(start),
			"took_str": time.Since(start).String(),
			"query":    query,
		}).Error(err)

		return err
	}

	return nil
}

func (i ItemStorePostgres) Undelete(user *model.User, itemIds []string) error {
	start := time.Now()

	query := fmt.Sprintf(`
			UPDATE
				user_item
			SET
				is_deleted = FALSE,
				is_indexed = FALSE
			WHERE
			    user_id = %d AND
				item_id IN ('%s')`, user.Id, strings.Join(itemIds, `','`))

	_, err := i.connection.DB().Exec(query)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":    string(debug.Stack()),
			"took":     time.Since(start),
			"took_str": time.Since(start).String(),
			"query":    query,
		}).Error(err)

		return err
	}

	return nil
}
