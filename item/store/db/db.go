package item_store_db

import (
	"bitbucket.org/epicglue/api/model"
)

type ItemStoreDB interface {
	Glue(*model.User, []string) error
	Unglue(*model.User, []string) error

	Read(*model.User, []string) error
	Unread(*model.User, []string) error

	Delete(*model.User, []string) error
}
