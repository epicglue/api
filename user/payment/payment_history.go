package payment

import "time"

func PaymentHistory(req *PaymentHistoryRequest) *PaymentHistoryResponse {
	phs := &PaymentHistoryResponse{}
	phs.Items = append(phs.Items, &PaymentHistoryItem{
		PlanName:    "pro",
		Amount:      10.00,
		ActiveFrom:  time.Date(2015, time.September, 1, 0, 0, 0, 0, &time.Location{}),
		ActiveUntil: time.Date(2015, time.October, 1, 0, 0, 0, 0, &time.Location{}),
	})
	phs.Items = append(phs.Items, &PaymentHistoryItem{
		PlanName:    "pro",
		Amount:      10.00,
		ActiveFrom:  time.Date(2015, time.October, 1, 0, 0, 0, 0, &time.Location{}),
		ActiveUntil: time.Date(2015, time.November, 1, 0, 0, 0, 0, &time.Location{}),
	})

	return phs
}
