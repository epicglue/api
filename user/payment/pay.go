package payment

import (
	"bitbucket.org/epicglue/api/model"
)

func Pay(req *PayRequest) error {
	log.Infof("Plan %s (%s) valid until ???", req.user.GetPlan().Name, req.user.GetPlan().ShortName)

	for i, receipt := range req.iapResponse.Receipt.InApp {
		log.Debugf("Receipt.InApp[%d] = %s", i, receipt.ProductID)

		if err := model.AddPayment(req.user, receipt); err != nil {
			continue
		}

		if err := model.AddPlan(req.user, model.LoadPlan(receipt.ProductID)); err != nil {
			panic(err.Error())
		}
	}

	return nil
}
