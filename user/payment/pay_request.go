package payment

import (
	"bitbucket.org/epicglue/api/model"
	"encoding/json"
	"errors"
	"github.com/Sirupsen/logrus"
	"github.com/dogenzaka/go-iap/appstore"
	"net/http"
	"runtime/debug"
)

type PayRequest struct {
	ReceiptData string `json:"receipt"`
	user        *model.User
	iapResponse *appstore.IAPResponse
}

func NewPayRequest(r *http.Request) *PayRequest {
	request := &PayRequest{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(request)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return nil
	}

	if err := request.validate(); err != nil {
		return nil
	}

	request.user = model.LoadUserByUsername(r.Header.Get("User"))

	return request
}

func (req *PayRequest) validate() error {
	// Check basic values
	if len(req.ReceiptData) == 0 {
		return req.validationError("Receipt data missing")
	}

	// Receipt
	client := appstore.New()
	iapRequest := appstore.IAPRequest{
		ReceiptData: req.ReceiptData,
	}
	iapResponse := &appstore.IAPResponse{}
	err := client.Verify(iapRequest, iapResponse)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return req.validationError("Invalid receipt")
	}

	req.iapResponse = iapResponse

	return nil
}

func (req *PayRequest) validationError(msg string) error {
	return errors.New(msg)
}
