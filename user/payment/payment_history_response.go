package payment

import "time"

type PaymentHistoryResponse struct {
	Items []*PaymentHistoryItem `json:"data"`
}

type PaymentHistoryItem struct {
	PlanName     string    `json:"plan"`
	PaymentType  string    `json:"-"`
	Amount       float64   `json:"amount"`
	ActiveFrom   time.Time `json:"active_from,omitempty"`
	ActiveUntil  time.Time `json:"active_until,omitempty"`
	PurchaseDate time.Time `json:"purchase_date"`
}
