package user_store_db

import (
	"bitbucket.org/epicglue/api/model"
)

type UserStoreDB interface {
	// Account
	CreateUser(*model.User) error
	DeleteUser(*model.User) error

	AddService(*model.User, *model.Token) error
	DeleteService(*model.User, *model.Service, string) error

	ListOfAutomaticSourcesForService(*model.Service) []*model.Source

	// Session
	Login(*model.User) error
	//LoginWithService(*model.User, string, string, string) error
	Logout(*model.User) error

	IsTokenActive(string) bool
	GenerateAndStoreToken(*model.User) error

	// Feedback
	SaveFeedbackForUser(*model.User, string) error
}
