package user_store_postgres

import (
	"bitbucket.org/epicglue/api/model"
	"github.com/Sirupsen/logrus"
	"github.com/dgrijalva/jwt-go"
	"runtime/debug"
)

func (db UserStorePostgres) IsTokenActive(token string) bool {
	if !db.isTokenIsCurrentlyActive(token) {
		return false
	}

	return true
}

func (db UserStorePostgres) GenerateAndStoreToken(user *model.User) error {
	if user.Id == 0 {
		log.Fatal("User.id cannot be empty")
	}

	var token string

	JWTToken := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"userId": user.Id,
	})
	//	token.Claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

	token, _ = JWTToken.SignedString([]byte(conf.App.Secret))
	user.Token = token

	if db.isTokenIsCurrentlyActive(token) {
		return nil
	}

	if err := db.activateNewUserToken(user, token); err != nil {
		return err
	}

	return nil
}

func (db UserStorePostgres) activateNewUserToken(user *model.User, token string) error {
	db.deactivateUsersTokens(user)

	sqlQuery := `INSERT INTO "user_token" (user_id, token, is_active, created_at) VALUES ($1, $2, TRUE, NOW())`

	_, err := db.Connection.DB().Exec(sqlQuery, user.Id, token)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":   string(debug.Stack()),
			"user_id": user.Id,
		}).Error(err)

		return err
	}

	return nil
}

func (db UserStorePostgres) isTokenIsCurrentlyActive(token string) bool {
	var numberOfReturnedTokens int

	sqlQuery := `
		SELECT
			COUNT(*)
		FROM
			"user_token"
		WHERE
			token = $1 AND
			is_active = TRUE
	`

	if err := db.Connection.DB().QueryRow(sqlQuery, token).Scan(&numberOfReturnedTokens); err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return false
	}

	return numberOfReturnedTokens > 0
}

func (db UserStorePostgres) deactivateUsersTokens(user *model.User) error {
	sqlQuery := `
		UPDATE
			"user_token"
		SET
			is_active = FALSE,
			updated_at = NOW()
		WHERE
			user_id = $1
	`

	_, err := db.Connection.DB().Exec(sqlQuery, user.Id)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return err
	}

	return nil
}
