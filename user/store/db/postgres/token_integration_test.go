// +build integration

package user_store_postgres_test

import (
	"bitbucket.org/epicglue/api/connection/database"
	"bitbucket.org/epicglue/api/connection/database/postgres"
	"bitbucket.org/epicglue/api/helpers/test"
	"bitbucket.org/epicglue/api/model"
	"bitbucket.org/epicglue/api/user/store/db/postgres"
	"github.com/stretchr/testify/assert"
	"testing"
)

func putActiveTokenForUser(db database.Database, user *model.User, token string) {
	query := `
		INSERT INTO
			"user_token" (user_id, token, is_active, created_at)
		VALUES
			($1, $2, TRUE, NOW())`

	_, err := db.Exec(query, user.Id, token)

	if err != nil {
		panic(err.Error())
	}
}

func TestFreshToken(t *testing.T) {
	user := test_helper.MakeTestUser()
	token := model.UserToken{}

	userStore := user_store_postgres.NewUserStorePostgres()

	assert.Equal(t, token.ActiveTokenForUser(&user), "")

	if err := userStore.GenerateAndStoreToken(&user); err != nil {
		t.Errorf("Failed to generate token. %s", err)
	}

	assert.Equal(t, token.ActiveTokenForUser(&user), user.Token)

	userStore.DeleteUser(&user)
}

func TestExistingToken(t *testing.T) {
	user := test_helper.MakeTestUser()
	token := model.UserToken{}

	userStore := user_store_postgres.NewUserStorePostgres()

	putActiveTokenForUser(postgres.NewPostgres(), &user, "token123")

	assert.Equal(t, token.ActiveTokenForUser(&user), "token123")

	if err := userStore.GenerateAndStoreToken(&user); err != nil {
		t.Errorf("Failed to generate token. %s", err)
	}

	assert.NotEqual(t, token.ActiveTokenForUser(&user), "token123")
	assert.Equal(t, token.ActiveTokenForUser(&user), user.Token)

	userStore.DeleteUser(&user)
}
