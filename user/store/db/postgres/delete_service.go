package user_store_postgres

import (
	"bitbucket.org/epicglue/api/connection/database/postgres"
	"bitbucket.org/epicglue/api/model"
	"errors"
	"github.com/Sirupsen/logrus"
	"runtime/debug"
)

func (db UserStorePostgres) DeleteService(user *model.User, service *model.Service, identifier string) error {
	// load current profile
	currentProfileId := db.findProfile(user, service, identifier)

	if currentProfileId == 0 {
		return errors.New("Failed to load profile")
	}

	// delete user subscriptions
	if err := db.deleteUserSubscriptions(currentProfileId); err != nil {
		return err
	}

	// delete subscriptions
	if err := db.deleteSubscriptions(currentProfileId); err != nil {
		return err
	}

	// delete tokens
	if err := db.deleteServiceProfileTokens(currentProfileId); err != nil {
		return err
	}

	// delete profile
	if err := db.deleteServiceProfile(currentProfileId); err != nil {
		return err
	}

	return nil
}

func (db UserStorePostgres) findProfile(user *model.User, service *model.Service, identifier string) int64 {
	if currentProfile := model.LoadServiceProfileByIdentifier(service, user, identifier); currentProfile != nil {
		return currentProfile.Id
	}

	log.WithFields(logrus.Fields{
		"stack":      string(debug.Stack()),
		"user_id":    user.Id,
		"service":    service.ShortName,
		"service_id": service.Id,
		"identifier": identifier,
	}).Error(errors.New("Failed to load profile"))

	return 0
}

func (db UserStorePostgres) deleteUserSubscriptions(profileId int64) error {
	query := `DELETE FROM "user_subscription" WHERE profile_id = $1`

	if _, err := postgres.NewPostgres().DB().Exec(query, profileId); err != nil {
		return err
	}

	return nil
}

func (db UserStorePostgres) deleteSubscriptions(profileId int64) error {
	query := `DELETE FROM "subscription" WHERE profile_id = $1`

	if _, err := postgres.NewPostgres().DB().Exec(query, profileId); err != nil {
		return err
	}

	return nil
}

func (db UserStorePostgres) deleteServiceProfileTokens(profileId int64) error {
	query := `DELETE FROM "user_service_token" WHERE profile_id = $1`

	if _, err := postgres.NewPostgres().DB().Exec(query, profileId); err != nil {
		return err
	}

	return nil
}

func (db UserStorePostgres) deleteServiceProfile(profileId int64) error {
	query := `DELETE FROM "user_service_profile" WHERE id = $1`

	if _, err := postgres.NewPostgres().DB().Exec(query, profileId); err != nil {
		return err
	}

	return nil
}
