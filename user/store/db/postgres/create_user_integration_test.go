// +build integration

package user_store_postgres_test

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/helpers/test"
	"bitbucket.org/epicglue/api/model"
	"bitbucket.org/epicglue/api/user/store/db/postgres"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/yezooz/null"
	"testing"
)

func TestNewUserValidation(t *testing.T) {
	userStore := user_store_postgres.NewUserStorePostgres()

	assert.NotNil(t, userStore.CreateUser(nil))
	assert.NotNil(t, userStore.CreateUser(&model.User{
		Email: "random",
	}))
	assert.NotNil(t, userStore.CreateUser(&model.User{
		Email:    helpers.RandomEmail(),
		Password: null.StringFrom("short"),
	}))
}

func TestNewUser(t *testing.T) {
	userStore := user_store_postgres.NewUserStorePostgres()

	newUser := &model.User{
		Email:    helpers.RandomEmail(),
		Password: null.StringFrom(helpers.RandomString(8)),
	}

	assert.Nil(t, userStore.CreateUser(newUser))

	assert.NotNil(t, model.LoadUserByUsername(newUser.Email))

	assert.True(t, test_helper.HasRecordsInTableWithCondition("auth_user", fmt.Sprintf(`email = '%s'`, newUser.Email)))
	assert.True(t, test_helper.HasRecordsInTableWithCondition("user", fmt.Sprintf(`username = '%s'`, newUser.Email)))
}
