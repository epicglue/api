package user_store_postgres

import (
	"bitbucket.org/epicglue/api/config"
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/model"
	"errors"
	"github.com/asaskevich/govalidator"
	"strings"
)

var conf = config.LoadConfig()

func (db UserStorePostgres) Login(user *model.User) error {
	if err := db.validateLoginUserInput(user); err != nil {
		return err
	}

	userFromDB := model.LoadUserByUsername(user.Email)
	if userFromDB == nil {
		return errors.New("Failed to load user by username")
	}

	if !strings.HasSuffix(user.Email, "@ios.device") { // don't validate by-device-id users
		if !helpers.IsPasswordValid(user.Password.String, userFromDB.Password.String, userFromDB.Salt.String) {
			return errors.New("Password not valid")
		}
	}

	user.Id = userFromDB.Id

	return db.GenerateAndStoreToken(user)
}

func (db UserStorePostgres) validateLoginUserInput(user *model.User) error {
	if user == nil {
		return errors.New("User cannot be null")
	}

	if !govalidator.IsEmail(user.Email) {
		return errors.New("Username is not valid email address")
	}

	if !user.Password.Valid || len(user.Password.String) < 8 {
		return errors.New("Password must be at least 8 characters long")
	}

	if !db.emailExists(user.Email) {
		return errors.New("Email doesn't exist")
	}

	return nil
}
