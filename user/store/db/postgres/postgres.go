package user_store_postgres

import (
	"bitbucket.org/epicglue/api/connection/database/postgres"
	"bitbucket.org/epicglue/api/helpers"
	"github.com/jinzhu/gorm"
)

var log = helpers.GetLogger("user")

type UserStorePostgres struct {
	Connection *gorm.DB
}

func NewUserStorePostgres() *UserStorePostgres {
	return &UserStorePostgres{
		Connection: postgres.NewPostgres(),
	}
}
