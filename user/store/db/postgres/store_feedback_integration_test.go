// +build integration

package user_store_postgres_test

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/model"
	"bitbucket.org/epicglue/api/user/store/db/postgres"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestFeedbackValidation(t *testing.T) {
	userStore := user_store_postgres.NewUserStorePostgres()

	assert.NotNil(t, userStore.SaveFeedbackForUser(nil, ""))
	assert.NotNil(t, userStore.SaveFeedbackForUser(&model.User{
		Id: 1,
	}, ""))
}

func TestNewFeedback(t *testing.T) {
	userStore := user_store_postgres.NewUserStorePostgres()

	feedbackContent := helpers.RandomString(8)

	assert.Nil(t, userStore.SaveFeedbackForUser(&model.User{
		Id: 1,
	}, feedbackContent))

}
