package user_store_postgres

import (
	"bitbucket.org/epicglue/api/connection/database/postgres"
	"bitbucket.org/epicglue/api/model"
	"errors"
	"github.com/Sirupsen/logrus"
	"runtime/debug"
)

func (db UserStorePostgres) AddService(user *model.User, token *model.Token) error {
	if token.Service == nil {
		return errors.New("Service not found")
	}

	if err := db.addServiceProfile(user, token); err != nil {
		return err
	}

	if err := db.addServiceToken(user, token); err != nil {
		return err
	}

	return nil
}

func (db UserStorePostgres) addServiceProfile(user *model.User, token *model.Token) error {
	query := `
        INSERT INTO "user_service_profile" (
            user_id,
            service_id,
            identifier,
            friendly_name,
            created_at,
            updated_at
        ) VALUES (
            $1,
            $2,
            $3,
            $4,
            NOW(),
            NOW()
        )
        ON CONFLICT (user_id, service_id, identifier) DO UPDATE SET updated_at = NOW()`

	if _, err := postgres.NewPostgres().DB().Exec(query, user.Id, token.Service.Id, token.Identifier, token.FriendlyName); err != nil {
		log.WithFields(logrus.Fields{
			"stack":         string(debug.Stack()),
			"user_id":       user.Id,
			"service":       token.Service,
			"service_id":    token.Service.Id,
			"identifier":    token.Identifier,
			"friendly_name": token.FriendlyName,
		}).Error(err)

		return err
	}

	return nil
}

func (db UserStorePostgres) addServiceToken(user *model.User, token *model.Token) error {
	hasToken, err := db.isTokenIsCurrentlyActiveForService(user, token)

	if err != nil {
		return err
	}

	if hasToken {
		return nil
	}

	if err := db.activateNewUserTokenForService(user, token); err != nil {
		return err
	}

	return nil
}

func (db UserStorePostgres) activateNewUserTokenForService(user *model.User, token *model.Token) error {
	profile := model.LoadServiceProfileByIdentifier(token.Service, user, token.Identifier)
	if profile == nil {
		err := errors.New("Failed to activate new token")

		log.WithFields(logrus.Fields{
			"stack":         string(debug.Stack()),
			"user_id":       user.Id,
			"service":       token.Service.ShortName,
			"token":         token.Token,
			"token_secret":  token.TokenSecret.String,
			"token_refresh": token.RefreshToken.String,
			"token_expiry":  token.Expiry.Time,
		}).Error(err)

		return err
	}

	if err := db.deactivateUsersTokensForProfile(profile); err != nil {
		return err
	}

	sqlQuery := `
        INSERT INTO
            "user_service_token" (profile_id, token, token_secret, refresh_token, scope, expiry, is_active, created_at, updated_at)
        VALUES
            ($1, $2, $3, $4, $5, $6, TRUE, NOW(), NOW())
    `

	_, err := db.Connection.DB().Exec(sqlQuery, profile.Id, token.Token, token.TokenSecret, token.RefreshToken, token.Scope, token.Expiry)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":         string(debug.Stack()),
			"user_id":       user.Id,
			"service":       token.Service.ShortName,
			"token":         token.Token,
			"token_secret":  token.TokenSecret.String,
			"token_refresh": token.RefreshToken.String,
			"token_expiry":  token.Expiry.Time,
			"scope":         token.Scope,
		}).Error(err)

		return err
	}

	return nil
}

func (db UserStorePostgres) isTokenIsCurrentlyActiveForService(user *model.User, token *model.Token) (bool, error) {
	var numberOfReturnedTokens int

	sqlQuery := `
		SELECT
			COUNT(*)
		FROM
			"user_service_profile" AS usp
		LEFT JOIN
		    "user_service_token" AS ust ON (usp.id = ust.profile_id)
		WHERE
			ust.token = $1 AND
			ust.is_active = TRUE AND
			usp.service_id = $2 AND
			usp.user_id = $3
	`

	if err := db.Connection.DB().QueryRow(sqlQuery, token.Token, token.Service.Id, user.Id).Scan(&numberOfReturnedTokens); err != nil {
		log.WithFields(logrus.Fields{
			"stack":   string(debug.Stack()),
			"token":   token.Token,
			"service": token.Service.Id,
		}).Error(err)

		return false, errors.New("Failed to retrieve current token")
	}

	return numberOfReturnedTokens > 0, nil
}

func (db UserStorePostgres) deactivateUsersTokensForProfile(profile *model.ServiceProfile) error {
	sqlQuery := `
		UPDATE
		    "user_service_token"
		SET
			is_active = FALSE,
			updated_at = NOW()
		WHERE
			profile_id = $1
	`

	_, err := db.Connection.DB().Exec(sqlQuery, profile.Id)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":      string(debug.Stack()),
			"profile_id": profile.Id,
		}).Error(err)

		return err
	}

	return nil
}
