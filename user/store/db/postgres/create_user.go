package user_store_postgres

import (
	"bitbucket.org/epicglue/api/connection/database/postgres"
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/model"
	"crypto/sha1"
	"errors"
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/asaskevich/govalidator"
	"github.com/yezooz/null"
	"runtime/debug"
)

func (db UserStorePostgres) CreateUser(user *model.User) error {
	if err := db.validateCreateUserInput(user); err != nil {
		return err
	}

	return db.createUserEntries(user)
}

func (db UserStorePostgres) validateCreateUserInput(user *model.User) error {
	if user == nil {
		return errors.New("User cannot be null")
	}

	if !govalidator.IsEmail(user.Email) {
		return errors.New("Username is not valid email address")
	}

	if !user.Password.Valid || len(user.Password.String) < 8 {
		return errors.New("Password must be at least 8 characters long")
	}

	if db.emailExists(user.Email) {
		return errors.New("Email exists")
	}

	return nil
}

func (db UserStorePostgres) emailExists(email string) bool {
	query := `
		SELECT
			COUNT(*)
		FROM
			"user"
		WHERE
			username = $1
	`

	var id int
	if err := postgres.NewPostgres().DB().QueryRow(query, email).Scan(&id); err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return false
	}

	return id > 0
}

func (db UserStorePostgres) createUserEntries(user *model.User) error {
	db.saltUser(user)

	if err := db.insertAuthUser(user); err != nil {
		return err
	}

	if err := db.insertUser(user); err != nil {
		return err
	}

	return nil
}

func (db UserStorePostgres) insertAuthUser(user *model.User) error {
	query := `
		INSERT INTO
			"auth_user" (password, is_superuser, username, email, first_name, last_name, is_staff, is_active, date_joined)
		VALUES
			($1, FALSE, $2, $3, '', '', FALSE, TRUE, NOW())`

	r, err := db.Connection.DB().Exec(query, user.Password.String, user.Email, user.Email)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
			"query": query,
			"user":  user.Email,
		}).Error(err)

		return err
	}

	if n, _ := r.RowsAffected(); n != 1 {
		err := errors.New("Failed to add auth_user")

		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
			"query": query,
			"user":  user.Email,
		}).Error(err)

		return err
	}

	return db.findIdOfInsertedAuthUser(user)
}

func (db UserStorePostgres) findIdOfInsertedAuthUser(user *model.User) error {
	query := `
		SELECT
			id
		FROM
			"auth_user"
		WHERE
			username = $1
	`

	err := db.Connection.DB().QueryRow(query, user.Email).Scan(&user.Id)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
			"query": query,
			"user":  user.Email,
		}).Error(err)

		return err
	}

	return nil
}

func (db UserStorePostgres) insertUser(user *model.User) error {
	query := `
		INSERT INTO
			"user" (id, user_id, username, password, salt, created_at, updated_at)
		VALUES
			($1, $2, $3, $4, $5, NOW(), NOW()) `

	_, err := db.Connection.DB().Exec(query, user.Id, user.Id, user.Email, user.Password, user.Salt.String)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":   string(debug.Stack()),
			"query":   query,
			"user_id": user.Id,
		}).Error(err)

		_, err := db.Connection.DB().Exec(`DELETE FROM "auth_user" WHERE id = $1`, user.Id)

		if err != nil {
			log.WithFields(logrus.Fields{
				"stack":   string(debug.Stack()),
				"query":   query,
				"user_id": user.Id,
			}).Error(err)
		}

		return err
	}

	log.WithFields(logrus.Fields{
		"user_id": user.Id,
	}).Debug("User Registered")

	return nil
}

func (db UserStorePostgres) saltUser(user *model.User) {
	saltedPassword, salt := db.saltPassword(user.Password.String)
	user.Password = null.StringFrom(saltedPassword)
	user.Salt = null.StringFrom(salt)
}

// Takes password and returns SHA1'd password and salt
func (db UserStorePostgres) saltPassword(password string) (string, string) {
	salt := helpers.RandomString(8)

	h := sha1.New()
	h.Write([]byte(fmt.Sprintf("%s.%s", password, salt)))

	return fmt.Sprintf("%x", h.Sum(nil)), salt
}
