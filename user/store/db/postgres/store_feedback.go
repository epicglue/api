package user_store_postgres

import (
	"bitbucket.org/epicglue/api/model"
	"errors"
	"github.com/Sirupsen/logrus"
	"github.com/imdario/mergo"
	"runtime/debug"
)

func (db UserStorePostgres) SaveFeedbackForUser(user *model.User, feedback string) error {
	if err := db.validateStoreFeedbackInput(user, feedback); err != nil {
		return err
	}

	query := `INSERT INTO feedback (user_id, feedback, created_at) VALUES ($1, $2, NOW())`

	if _, err := db.Connection.DB().Exec(query, user.Id, feedback); err != nil {
		logFields := logrus.Fields{
			"stack":    string(debug.Stack()),
			"action":   "feedback",
			"feedback": feedback,
		}
		mergo.Map(&logFields, user.LogFields())
		log.WithFields(logFields).Error(err)

		return err
	}

	return nil
}

func (db UserStorePostgres) validateStoreFeedbackInput(user *model.User, feedback string) error {
	if user == nil {
		return errors.New("User cannot be null")
	}

	if len(feedback) == 0 {
		return errors.New("Feedback cannot be empty")
	}

	return nil
}
