package user_store_postgres

import (
	"bitbucket.org/epicglue/api/model"
	"database/sql"
	"github.com/Sirupsen/logrus"
	"runtime/debug"
)

func (db UserStorePostgres) ListOfAutomaticSourcesForService(service *model.Service) []*model.Source {
	list := make([]*model.Source, 0)

	var sourceId int64
	rows := db.findAutomaticSourcesForService(service)
	for rows.Next() {
		err := rows.Scan(
			&sourceId,
		)

		if err != nil {
			log.WithFields(logrus.Fields{
				"stack": string(debug.Stack()),
			}).Panic(err)
		}

		list = append(list, model.LoadSource(sourceId))
	}

	return list
}

func (db UserStorePostgres) findAutomaticSourcesForService(service *model.Service) *sql.Rows {
	query := `
		SELECT
			source.id
		FROM
			source
		LEFT JOIN
			service ON service.id = source.service_id
		WHERE
			do_auto_subscribe = True AND
			service.id = $1
	`

	rows, err := db.Connection.DB().Query(query, service.Id)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":   string(debug.Stack()),
			"query":   query,
			"service": service.Id,
		}).Error(err)

		return nil
	}

	return rows
}
