package user_store_postgres

import (
	"bitbucket.org/epicglue/api/model"
	"errors"
	"github.com/Sirupsen/logrus"
	"runtime/debug"
)

func (db UserStorePostgres) DeleteUser(user *model.User) error {
	return db.deleteUserEntries(user)
}

func (db UserStorePostgres) deleteUserEntries(user *model.User) error {
	if err := db.deleteTokensOfUser(user); err != nil {
		return err
	}

	if err := db.deleteUser(user); err != nil {
		return err
	}

	if err := db.deleteAuthUser(user); err != nil {
		return err
	}

	return nil
}

func (db UserStorePostgres) deleteAuthUser(user *model.User) error {
	query := `DELETE FROM "auth_user" WHERE id = $1`

	r, err := db.Connection.DB().Exec(query, user.Id)

	if err != nil {
		log.WithFields(logrus.Fields{
			"query":   query,
			"stack":   string(debug.Stack()),
			"user":    user.Email,
			"user_id": user.Id,
		}).Error(err)

		return err
	}

	if n, _ := r.RowsAffected(); n != 1 {
		err := errors.New("Failed to delete auth_user")

		log.WithFields(logrus.Fields{
			"stack":   string(debug.Stack()),
			"query":   query,
			"user":    user.Email,
			"user_id": user.Id,
		}).Error(err)

		return err
	}

	log.WithFields(logrus.Fields{
		"user":    user.Email,
		"user_id": user.Id,
	}).Debug("User Deleted")

	return nil
}

func (db UserStorePostgres) deleteUser(user *model.User) error {
	query := `DELETE FROM "user" WHERE id = $1 `

	r, err := db.Connection.DB().Exec(query, user.Id)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":   string(debug.Stack()),
			"query":   query,
			"user":    user.Email,
			"user_id": user.Id,
		}).Error(err)

		return err
	}

	if n, _ := r.RowsAffected(); n != 1 {
		err := errors.New("Failed to delete user")

		log.WithFields(logrus.Fields{
			"stack":   string(debug.Stack()),
			"query":   query,
			"user":    user.Email,
			"user_id": user.Id,
		}).Error(err)

		return err
	}

	return nil
}

func (db UserStorePostgres) deleteTokensOfUser(user *model.User) error {
	query := `DELETE FROM "user_token" WHERE user_id = $1 `

	_, err := db.Connection.DB().Exec(query, user.Id)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack":   string(debug.Stack()),
			"query":   query,
			"user":    user.Email,
			"user_id": user.Id,
		}).Error(err)

		return err
	}

	return nil
}
