// +build integration

package user_store_elasticsearch_test

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/helpers/test"
	"bitbucket.org/epicglue/api/model"
	"bitbucket.org/epicglue/api/user/store/index/elasticsearch"
	"github.com/stretchr/testify/assert"
	"github.com/yezooz/null"
	"testing"
)

func TestNewUser(t *testing.T) {
	userStore := user_store_elasticsearch.NewUserStoreElasticsearch()

	newUser := &model.User{
		Id:       helpers.RandomNumber(8),
		Email:    helpers.RandomEmail(),
		Password: null.StringFrom(helpers.RandomString(8)),
		PlanName: model.PLAN_FREE,
	}

	assert.Nil(t, userStore.CreateIndexForUser(newUser))

	assert.True(t, test_helper.HasIndex(newUser.IndexName()))
}
