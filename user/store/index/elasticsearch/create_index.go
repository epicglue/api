package user_store_elasticsearch

import (
	"bitbucket.org/epicglue/api/connection/index/elasticsearch"
	"bitbucket.org/epicglue/api/model"
)

func (inx UserStoreElasticsearch) CreateIndexForUser(user *model.User) error {
	es := elasticsearch.NewElasticsearch()
	return es.CreateIndex(user.IndexName())
}
