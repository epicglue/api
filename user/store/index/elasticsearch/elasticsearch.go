package user_store_elasticsearch

import (
	"bitbucket.org/epicglue/api/connection/index"
	"bitbucket.org/epicglue/api/connection/index/elasticsearch"
	"bitbucket.org/epicglue/api/helpers"
)

var log = helpers.GetLogger("user")

type UserStoreElasticsearch struct {
	Connection index.Index
}

func NewUserStoreElasticsearch() *UserStoreElasticsearch {
	return &UserStoreElasticsearch{
		Connection: elasticsearch.NewElasticsearch(),
	}
}
