package user_store_index

import "bitbucket.org/epicglue/api/model"

type UserStoreIndex interface {
	CreateIndexForUser(*model.User) error
	DeleteIndexForUser(*model.User) error

	PrepareIndexToUse(*model.User) error
}
