package user_store_metric

import "bitbucket.org/epicglue/api/model"

type UserStoreMetric interface {
	AddedUser(*model.User)
	RemovedUser(*model.User)
	LoggedIn(*model.User)

	ConnectedService(*model.User, *model.Token)
	DisconnectedService(*model.User, *model.Service, string)

	FeedbackAdded(*model.User)
}
