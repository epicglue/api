package user_store_metric_postgres

import (
	"bitbucket.org/epicglue/api/metric/postgres"
	"bitbucket.org/epicglue/api/model"
	"fmt"
)

func (m UserMetricPostgres) ConnectedService(user *model.User, token *model.Token) {
	m.metric = metric_postgres.NewPostgresMetricWithUser(user)

	m.metric.Inc("service.connected")
	m.metric.Inc(fmt.Sprintf("service.%s.connected", token.Service.ShortName))

	// TODO: add JSON logging
	log.Infof("%d has connected service %s.%s", user.Id, token.Service.ShortName, token.Identifier)
}
