package user_store_metric_postgres

import (
	"bitbucket.org/epicglue/api/metric/postgres"
	"bitbucket.org/epicglue/api/model"
	"fmt"
)

func (m UserMetricPostgres) DisconnectedService(user *model.User, service *model.Service, identifier string) {
	m.metric = metric_postgres.NewPostgresMetricWithUser(user)

	m.metric.Dec("service.connected")
	m.metric.Dec(fmt.Sprintf("service.%s.connected", service.ShortName))
	m.metric.Dec(fmt.Sprintf("service.%s.%s.connected", service.ShortName, identifier))

	// TODO: add JSON logging
	log.Infof("%d has disonnected service %s=%s", user.Id, service.ShortName, identifier)
}
