package user_store_metric_postgres

import (
	"bitbucket.org/epicglue/api/metric/postgres"
	"bitbucket.org/epicglue/api/model"
)

func (m UserMetricPostgres) AddedUser(user *model.User) {
	// General metrics
	m.metric = metric_postgres.NewPostgresMetric()
	m.metric.Inc("users")
	m.metric.Inc("users.active")

	// TODO: add JSON logging
	log.Infof("%d registered", user.Id)
}
