package user_store_metric_postgres

import (
	"bitbucket.org/epicglue/api/metric/postgres"
	"bitbucket.org/epicglue/api/model"
)

func (m UserMetricPostgres) LoggedIn(user *model.User) {
	// General metrics
	m.metric = metric_postgres.NewPostgresMetric()
	m.metric.Inc("user.login")

	// Per-user metrics
	m.metric = metric_postgres.NewPostgresMetricWithUser(user)
	m.metric.Inc("user.login")

	// TODO: add JSON logging
	log.Infof("%d logged in", user.Id)
}
