package user_store_metric_postgres

import (
	"bitbucket.org/epicglue/api/metric/postgres"
	"bitbucket.org/epicglue/api/model"
)

func (m UserMetricPostgres) RemovedUser(user *model.User) {
	// General metrics
	m.metric = metric_postgres.NewPostgresMetric()
	m.metric.Dec("users")
	m.metric.Dec("users.active")

	m.metric.Inc("users.removed")

	// TODO: add JSON logging
	log.Infof("%s removed", user.Id)
}
