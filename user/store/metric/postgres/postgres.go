package user_store_metric_postgres

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/metric"
)

var log = helpers.GetLogger("metric")

type UserMetricPostgres struct {
	metric metric.Metric
}

func NewUserMetricPostgres() *UserMetricPostgres {
	return &UserMetricPostgres{
		metric: nil,
	}
}
