package user_store_metric_postgres

import (
	"bitbucket.org/epicglue/api/metric/postgres"
	"bitbucket.org/epicglue/api/model"
)

func (m UserMetricPostgres) FeedbackAdded(user *model.User) {
	// General metrics
	m.metric = metric_postgres.NewPostgresMetric()
	m.metric.Inc("user.feedback")

	// Per-user metrics
	m.metric = metric_postgres.NewPostgresMetricWithUser(user)
	m.metric.Inc("user.feedback")

	// TODO: add JSON logging
	log.Infof("%s added feedback", user.Id)
}
