package user_store_redis

import (
	"bitbucket.org/epicglue/api/connection/key_value_store"
	"bitbucket.org/epicglue/api/connection/key_value_store/redis"
)

type UserStoreRedis struct {
	Connection key_value_store.KeyValueStore
}

func NewUserStoreRedis() *UserStoreRedis {
	return &UserStoreRedis{
		Connection: redis.NewRedis(),
	}
}
