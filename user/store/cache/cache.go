package user_store_cache

import "bitbucket.org/epicglue/api/model"

type UserStoreCache interface {
	CreateEntriesForUser(*model.User) error
	CleanupAfterUser(*model.User) error
}
