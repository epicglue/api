package user_manager

func (m *DefaultUserManager) StoreFeedback(feedback string) error {
	if err := m.getDB().SaveFeedbackForUser(m.user, feedback); err != nil {
		return err
	}

	m.getMetric().LoggedIn(m.user)

	return nil
}
