package user_manager

import "bitbucket.org/epicglue/api/model"

func (m *DefaultUserManager) DeleteService(service *model.Service, identifier string) error {
	if err := m.getDB().DeleteService(m.user, service, identifier); err != nil {
		return err
	}

	m.getMetric().DisconnectedService(m.user, service, identifier)

	return nil
}
