package user_manager

import (
	"bitbucket.org/epicglue/api/model"
)

func (m *DefaultUserManager) Login(user *model.User) error {
	if err := m.getDB().Login(user); err != nil {
		return err
	}

	m.SetUser(user)

	m.getMetric().LoggedIn(user)

	return nil
}
