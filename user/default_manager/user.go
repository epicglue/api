package user_manager

import (
	"bitbucket.org/epicglue/api/model"
)

func (m *DefaultUserManager) GetUser() *model.User {
	return m.user
}

func (m *DefaultUserManager) SetUser(user *model.User) {
	m.user = user
}
