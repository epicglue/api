package user_manager

func (m *DefaultUserManager) GenerateAndStoreToken() error {
	return m.getDB().GenerateAndStoreToken(m.user)
}
