package user_manager

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/model"
	"bitbucket.org/epicglue/api/user/store/cache/redis"
	"bitbucket.org/epicglue/api/user/store/db/postgres"
	"bitbucket.org/epicglue/api/user/store/index/elasticsearch"
	"bitbucket.org/epicglue/api/user/store/metric/postgres"
	"net/http"
)

var log = helpers.GetLogger("user")

type DefaultUserManager struct {
	user   *model.User
	db     *user_store_postgres.UserStorePostgres
	index  *user_store_elasticsearch.UserStoreElasticsearch
	cache  *user_store_redis.UserStoreRedis
	metric *user_store_metric_postgres.UserMetricPostgres
}

func NewDefaultUserManagerFromRequest(r *http.Request) *DefaultUserManager {
	manager := &DefaultUserManager{}

	if r.Header.Get("User") == "" {
		return manager
	}

	if user := model.LoadUserByUsername(r.Header.Get("User")); user != nil {
		manager.SetUser(user)

		return manager
	}

	log.Panic("User not set")

	return manager
}

func NewDefaultUserManager(userId int64) *DefaultUserManager {
	if user := model.LoadUser(userId); user != nil {
		return NewDefaultUserManagerWithUser(user)
	}

	return nil
}

func NewDefaultUserManagerWithUser(user *model.User) *DefaultUserManager {
	return &DefaultUserManager{
		user: user,
	}
}

func (um *DefaultUserManager) LogFields() map[string]interface{} {
	return um.user.LogFields()
}

func (um *DefaultUserManager) getDB() *user_store_postgres.UserStorePostgres {
	if um.db == nil {
		um.db = user_store_postgres.NewUserStorePostgres()
	}
	return um.db
}

func (m *DefaultUserManager) getIndex() *user_store_elasticsearch.UserStoreElasticsearch {
	if m.index == nil {
		m.index = user_store_elasticsearch.NewUserStoreElasticsearch()
	}
	return m.index
}

func (m *DefaultUserManager) getCache() *user_store_redis.UserStoreRedis {
	if m.cache == nil {
		m.cache = user_store_redis.NewUserStoreRedis()
	}
	return m.cache
}

func (m *DefaultUserManager) getMetric() *user_store_metric_postgres.UserMetricPostgres {
	if m.metric == nil {
		m.metric = user_store_metric_postgres.NewUserMetricPostgres()
	}
	return m.metric
}

func (m *DefaultUserManager) IsTokenActive(token string) bool {
	return m.getDB().IsTokenActive(token)
}
