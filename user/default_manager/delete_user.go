package user_manager

func (m *DefaultUserManager) DeleteUser() error {
	if err := m.getDB().DeleteUser(m.user); err != nil {
		return err
	}

	if err := m.getIndex().DeleteIndexForUser(m.user); err != nil {
		return err
	}

	if err := m.getCache().CleanupAfterUser(m.user); err != nil {
		return err
	}

	m.getMetric().RemovedUser(m.user)

	m.SetUser(nil)

	return nil
}
