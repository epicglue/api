package user_manager

import (
	"bitbucket.org/epicglue/api/model"
	"bitbucket.org/epicglue/api/subscription/default_manager"
)

func (m *DefaultUserManager) AddService(token *model.Token) error {
	if err := m.getDB().AddService(m.user, token); err != nil {
		return err
	}

	subscriptionManager := subscription_manager.NewDefaultSubscriptionManagerWithUser(m.user)
	for _, source := range m.getDB().ListOfAutomaticSourcesForService(token.Service) {
		log.Debugf("Auto subscribing to %s", source.Name)

		if _, err := subscriptionManager.Subscribe(source, nil, ""); err != nil {
			return err
		}
	}

	m.getMetric().ConnectedService(m.user, token)

	return nil
}
