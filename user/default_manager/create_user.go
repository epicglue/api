package user_manager

import "bitbucket.org/epicglue/api/model"

func (m *DefaultUserManager) CreateUser(user *model.User) error {
	if err := m.getDB().CreateUser(user); err != nil {
		return err
	}

	if err := m.getIndex().CreateIndexForUser(user); err != nil {
		return err
	}

	if err := m.getCache().CreateEntriesForUser(user); err != nil {
		return err
	}

	m.SetUser(user)

	m.getMetric().AddedUser(user)

	return nil
}
