package user

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/model"
)

var log = helpers.GetLogger("user")

// Manage anything user-related

type UserManager interface {
	GetUser() *model.User
	SetUser(*model.User)

	CreateUser(*model.User) error
	DeleteUser() error

	AddService(*model.Token) error
	DeleteService(*model.Service, string) error

	Login(*model.User) error
	Logout() error

	IsTokenActive(string) bool
	GenerateAndStoreToken() error

	StoreFeedback(string) error
}
