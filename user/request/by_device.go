package request

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/model"
	"bitbucket.org/epicglue/api/user"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/yezooz/null"
	"net/http"
	"runtime/debug"
)

type ByDeviceRequest struct {
	DeviceId string `json:"device_id"`
}

func NewDeviceRequestFromHttpRequest(r *http.Request) (ByDeviceRequest, error) {
	request := ByDeviceRequest{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&request)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)
	}

	if err := request.validate(); err != nil {
		return request, err
	}

	return request, nil
}

func (request ByDeviceRequest) validate() error {
	if request.DeviceId == "" {
		return request.validationError("DeviceId cannot be null")
	}

	return nil
}

func (request ByDeviceRequest) validationError(msg string) error {
	return errors.New(msg)
}

func (request ByDeviceRequest) Register(manager user.UserManager) (*model.User, error) {
	password := null.StringFrom(helpers.RandomString(32))

	user := model.User{
		Email:    fmt.Sprintf("%s@ios.device", request.DeviceId),
		Password: password,
	}

	if err := manager.CreateUser(&user); err != nil && err.Error() != "Email exists" {
		return nil, err
	}

	if err := manager.Login(&user); err != nil {
		return nil, err
	}

	return &user, nil
}
