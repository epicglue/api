package request

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/model"
	"bitbucket.org/epicglue/api/user"
	"encoding/json"
	"errors"
	"github.com/Sirupsen/logrus"
	"github.com/asaskevich/govalidator"
	"github.com/yezooz/null"
	"net/http"
	"runtime/debug"
)

var log = helpers.GetLogger("user")

type ByEmailRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func NewEmailRequestFromHttpRequest(r *http.Request) (ByEmailRequest, error) {
	request := ByEmailRequest{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&request)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)
	}

	if err := request.validate(); err != nil {
		return request, err
	}

	return request, nil
}

func (request ByEmailRequest) validate() error {
	if request.Email == "" {
		return request.validationError("Email cannot be null")
	}

	if request.Password == "" {
		return request.validationError("Password cannot be null")
	}

	if !govalidator.IsEmail(request.Email) {
		return request.validationError("Email is not valid")
	}

	return nil
}

func (request ByEmailRequest) validationError(msg string) error {
	return errors.New(msg)
}

func (request ByEmailRequest) Login(manager user.UserManager) (*model.User, error) {
	user := model.User{
		Email:    request.Email,
		Password: null.StringFrom(request.Password),
	}

	if err := manager.Login(&user); err != nil {
		return nil, err
	}

	return &user, nil
}

func (request ByEmailRequest) Register(manager user.UserManager) (*model.User, error) {
	user := model.User{
		Email:    request.Email,
		Password: null.StringFrom(request.Password),
	}

	if err := manager.CreateUser(&user); err != nil {
		return nil, err
	}

	// Restore unsalted password
	user.Password = null.StringFrom(request.Password)

	if err := manager.Login(&user); err != nil {
		return nil, err
	}

	return &user, nil
}
