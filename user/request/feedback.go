package request

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/model"
	"bitbucket.org/epicglue/api/user"
	"net/http"
)

type FeedbackRequest struct {
	User     *model.User
	Feedback string `json:"feedback"`
}

func NewFeedbackFromHttpRequest(r *http.Request) *FeedbackRequest {
	feedbackRequest := FeedbackRequest{
		User: model.LoadUser(helpers.UserIdFromHttpHeader(r.Header)),
	}
	helpers.HttpPayloadToObject(r.Body, &feedbackRequest)

	if passed := feedbackRequest.validate(); !passed {
		return nil
	}

	return &feedbackRequest
}

func (request *FeedbackRequest) validate() bool {
	return len(request.Feedback) > 0
}

func (request *FeedbackRequest) Add(manager user.UserManager, feedback string) error {
	manager.SetUser(request.User)
	return manager.StoreFeedback(feedback)
}
