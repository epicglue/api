package request

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/model"
	"bitbucket.org/epicglue/api/user"
	"encoding/json"
	"errors"
	"github.com/Sirupsen/logrus"
	"net/http"
	"runtime/debug"
)

type DisconnectServiceRequest struct {
	User        *model.User
	Service     *model.Service
	ServiceName string `json:"service"`
	Identifier  string `json:"id"`
}

func NewDisconnectServiceFromHttpRequest(r *http.Request) (DisconnectServiceRequest, error) {
	// InRequest, OutRequest types

	// 1. decode
	request := DisconnectServiceRequest{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&request)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return request, err
	}

	// 2. (explicit?) validate
	if err := request.validate(); err != nil {
		return request, err
	}

	// 3. apply user w/error checking
	request.User = model.LoadUser(helpers.UserIdFromHttpHeader(r.Header))

	// 4. apply service w/error checking
	request.Service = model.LoadServiceByShortName(request.ServiceName)

	return request, err
}

func (request DisconnectServiceRequest) validate() error {
	if request.ServiceName == "" {
		return request.validationError("Service cannot be null")
	}

	if request.Identifier == "" {
		return request.validationError("Id cannot be null")
	}

	return nil
}

func (request DisconnectServiceRequest) validationError(msg string) error {
	return errors.New(msg)
}

// Don't pass manager here, it's gross
func (request DisconnectServiceRequest) Disconnect(manager user.UserManager) error {
	if err := manager.DeleteService(request.Service, request.Identifier); err != nil {
		return err
	} else {
		return nil
	}
}
