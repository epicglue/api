package request

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/model"
	"bitbucket.org/epicglue/api/user"
	"encoding/json"
	"errors"
	"github.com/Sirupsen/logrus"
	"github.com/yezooz/null"
	"net/http"
	"runtime/debug"
	"time"
)

type ByServiceRequest struct {
	Username       string    `json:"username"`
	ServiceName    string    `json:"service"`
	ServiceUserId  string    `json:"service_user_id"`
	Token          string    `json:"token"`
	TokenSecret    string    `json:"token_secret,omitempty"`
	TokenRefresh   string    `json:"token_refresh,omitempty"`
	TokenType      string    `json:"token_type,omitempty"`
	ExpirationDate time.Time `json:"expiration_date,omitempty"`
}

func NewRegisterByServiceFromHttpRequest(r *http.Request) (ByServiceRequest, error) {
	request := ByServiceRequest{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&request)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return request, err
	}

	if err := request.validate(); err != nil {
		return request, err
	}

	return request, err
}

func (request ByServiceRequest) validate() error {
	if request.ServiceName == "" {
		return request.validationError("ServiceName cannot be null")
	}

	if request.ServiceUserId == "" {
		return request.validationError("ServiceUserId cannot be null")
	}

	if request.Username == "" {
		return request.validationError("Username cannot be null")
	}

	if request.Token == "" {
		return request.validationError("Token cannot be null")
	}

	return nil
}

func (request ByServiceRequest) validationError(msg string) error {
	return errors.New(msg)
}

func (request ByServiceRequest) Register(manager user.UserManager) (*model.User, error) {
	user := model.User{
		Username: helpers.RandomEmail(),
		Password: null.StringFrom(helpers.RandomString(32)),
	}

	// Create base user
	if err := manager.CreateUser(&user); err != nil {
		log.Errorf("Failed to register by service. %s", err)
		return nil, err
	}

	// Add service to base user
	if err := manager.AddService(request.serviceTokenFromRequest(&user)); err != nil {
		// Rollback
		manager.DeleteUser()

		return nil, err
	}

	// Login to retrieve token
	if err := manager.Login(&user); err != nil {
		return nil, err
	}

	return &user, nil
}

func (request ByServiceRequest) serviceTokenFromRequest(user *model.User) *model.Token {
	return &model.Token{
		Service:      model.LoadServiceByShortName(request.ServiceName),
		Identifier:   request.ServiceUserId,
		FriendlyName: request.Username,
		Token:        request.Token,
		TokenSecret:  null.StringFrom(request.TokenSecret),
		RefreshToken: null.StringFrom(request.TokenRefresh),
		Expiry:       null.TimeFrom(request.ExpirationDate),
	}
}
