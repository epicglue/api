package request

import (
	"bitbucket.org/epicglue/api/helpers"
	"bitbucket.org/epicglue/api/model"
	"bitbucket.org/epicglue/api/user"
	"encoding/json"
	"errors"
	"github.com/Sirupsen/logrus"
	"github.com/yezooz/null"
	"net/http"
	"runtime/debug"
	"time"
)

type ConnectServiceRequest struct {
	User            *model.User
	ServiceName     string    `json:"service"`
	ServiceUsername string    `json:"username"`
	ServiceUserId   string    `json:"user_id"`
	Token           string    `json:"token"`
	TokenSecret     string    `json:"token_secret,omitempty"`
	TokenRefresh    string    `json:"token_refresh,omitempty"`
	TokenType       string    `json:"token_type,omitempty"`
	ExpiryDate      time.Time `json:"expiry_date,omitempty"`
	Scope           string    `json:"scope,omitempty"`
}

func NewConnectServiceFromHttpRequest(r *http.Request) (ConnectServiceRequest, error) {
	request := ConnectServiceRequest{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&request)

	if err != nil {
		log.WithFields(logrus.Fields{
			"stack": string(debug.Stack()),
		}).Error(err)

		return request, err
	}

	if err := request.validate(); err != nil {
		return request, err
	}

	request.User = model.LoadUser(helpers.UserIdFromHttpHeader(r.Header))

	return request, err
}

func (request ConnectServiceRequest) validate() error {
	if request.ServiceName == "" {
		return request.validationError("ServiceName cannot be null")
	}

	if request.ServiceUserId == "" {
		return request.validationError("ServiceUserId cannot be null")
	}

	if request.ServiceUsername == "" {
		return request.validationError("Username cannot be null")
	}

	if request.Token == "" {
		return request.validationError("Token cannot be null")
	}

	return nil
}

func (request ConnectServiceRequest) validationError(msg string) error {
	return errors.New(msg)
}

func (request ConnectServiceRequest) Connect(manager user.UserManager) error {
	if err := manager.AddService(request.serviceTokenFromRequest()); err != nil {
		return err
	} else {
		return nil
	}
}

func (request ConnectServiceRequest) socialProfileFromRequest() *model.ServiceProfile {
	return &model.ServiceProfile{
		User:         request.User,
		Service:      model.LoadServiceByShortName(request.ServiceName),
		Identifier:   request.ServiceUserId,
		FriendlyName: request.ServiceUsername,
	}
}

func (request ConnectServiceRequest) serviceTokenFromRequest() *model.Token {
	return &model.Token{
		Service:      model.LoadServiceByShortName(request.ServiceName),
		Identifier:   request.ServiceUserId,
		FriendlyName: request.ServiceUsername,
		Token:        request.Token,
		TokenSecret:  null.StringFrom(request.TokenSecret),
		RefreshToken: null.StringFrom(request.TokenRefresh),
		Expiry:       null.TimeFrom(request.ExpiryDate),
		Scope:        null.StringFrom(request.Scope),
	}
}
